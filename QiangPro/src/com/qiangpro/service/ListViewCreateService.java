package com.qiangpro.service;

import static com.qiangpro.util.NetworkConnection.isNetworkConnected;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.google.inject.Inject;
import com.qiangpro.application.CustomApplication;
import com.qiangpro.main.R;
import com.qiangpro.tab1.NoticeShowContentActivity;

public class ListViewCreateService {
	private ArrayList<HashMap<String, String>> arrayList;
	private HashMap<String, String> map ;
	private SimpleAdapter listAdapter;
	private ListView listView;
	private Activity activity;
	private CustomApplication customApplication ;
	private int noticeCacheQuantity;
	private String[] listCache;
	
	private static String KEY = "position";
	
	@Inject
	private NetworkingService networkingService;
	@Inject
	private CacheService cacheService; 
	
	private String noticeListKey[] = { "noticeTitle",
			"noticeContent", "noticeTime" };
	
	private int noticeListValue[] = { R.id.notice_title,
			R.id.notice_content, R.id.notice_time };
	
	public ListViewCreateService init(Activity activity,
			ListView listView) {
		this.activity = activity;
		this.listView = listView;
		customApplication = (CustomApplication)activity.getApplication();
		return this;
	}
	
	public void createNoticeList(int endPosition,View view) {
		arrayList = new ArrayList<HashMap<String, String>>();
		noticeCacheQuantity = getCacheQuantityForNotice();
		if (isNetworkConnected(activity)) {
			String[] noticeList = networkingService.init(activity).getNoticeList();
				for (int i = 0; i < endPosition; i++) {
					map = new HashMap<String, String>();
					map.put("noticeTitle", noticeList[i].split("\\|")[0]);
					map.put("noticeContent", noticeList[i].split("\\|")[1]);
					map.put("noticeTime", noticeList[i].split("\\|")[2]);
					arrayList.add(map);
					setCacheQuantityForNotice(endPosition);
					addFootView(view, noticeList);
				}
		} else {
			Toast.makeText(activity, "�������ӶϿ��������������ã�", Toast.LENGTH_SHORT).show();
			if (noticeCacheQuantity > 0) {
			    getCacheForNotice();
				for (int i = 0; i < listCache.length; i++) {
					map = new HashMap<String, String>();
					map.put("noticeTitle", listCache[i].split("\\|")[0]);
					map.put("noticeContent", listCache[i].split("\\|")[1]);
					map.put("noticeTime", listCache[i].split("\\|")[2]);
					arrayList.add(map);
				}
			}
		}
		initNoticeListView();
	}

	private void initNoticeListView() {
		listAdapter = new SimpleAdapter(activity, arrayList,R.layout.list_notice, noticeListKey,noticeListValue);
		listView.setAdapter(listAdapter);
		listView.setOnItemClickListener(new noticeListListener());
	}

	private void getCacheForNotice() {
		listCache = cacheService.getNoticeCache();
	}

	private void addFootView(View view, String[] list) {
		listView.removeFooterView(view);
		if(list.length !=customApplication.getListEndPosition()){
			listView.addFooterView(view,false, false);
		}
	}

	private void setCacheQuantityForNotice(int endPosition) {
		cacheService.setNoticeCacheQuantity(endPosition);
	}

	private int getCacheQuantityForNotice() {
	   return noticeCacheQuantity = cacheService.getNoticeCacheQuantity();
	}
	
	private class noticeListListener implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,	long id) {
			Intent intent = new Intent(activity, NoticeShowContentActivity.class);
			intent.putExtra(KEY,Integer.toString(position));
			activity.startActivity(intent);
		}
	}
	
	public int getListEndPosition(int number) {
		customApplication = (CustomApplication)activity.getApplication();
		int listEndPosition = customApplication.getListEndPosition();
		listEndPosition = listEndPosition + number;
		customApplication.setListEndPosition(listEndPosition);
		return listEndPosition;
	}
}

