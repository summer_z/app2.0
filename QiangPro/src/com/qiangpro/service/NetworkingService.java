package com.qiangpro.service;
import android.app.Activity;

import com.qiangpro.application.CustomApplication;

public class NetworkingService {
	private Activity activity;
	private CustomApplication customApplication;
	
	public NetworkingService init(Activity activity){
		this.activity = activity;
		customApplication = (CustomApplication)this.activity.getApplication();
		return this;
	}
	public String [] getNoticeList(){
		String noticeInfo = customApplication.getNoticeData();
		String [] noticeList = noticeInfo.split("#");
		return swap(noticeList);
	} 
	
	private  String[] swap(String noticeList[]){  
        int len = noticeList.length;  
        for(int i=0;i<len/2;i++){  
            String tmp = noticeList[i];  
            noticeList[i] = noticeList[len-1-i];  
            noticeList[len-1-i] = tmp;  
        }
		return noticeList;  
    }

}
