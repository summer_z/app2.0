package com.qiangpro.service;

import static com.qiangpro.util.NetworkConnection.isNetworkConnected;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.os.Environment;

import com.google.inject.Inject;

public class CacheService {
	@Inject
	private NetworkingService networkingService;
	
	private OutputStream outputStream = null;
	private InputStream input = null;
	
	private static String FOLDER_NAME = Environment.getExternalStorageDirectory()+ "/ToFree/";
	private static String NOTICE_FILE_NAME = Environment.getExternalStorageDirectory()+ "/ToFree/" + "Notice.accept";
	private static String DELIMITER = "Delimiter";
	
	private static String NOTICE_CACHEQUANTITY_FILE_NAME = Environment.getExternalStorageDirectory()+ "/ToFree/" + "NoticeCacheQuantity.accept";
	private static int END = -1;
	private static int NOTICE_ARRAY_SPACE = 10000;
	private static int NOTICE_QUANTITY_ARRAY_SPACE = 1024;
	
	public void saveNoticeCache(int position,Activity activity) {
		String[] noticeList = networkingService.init(activity).getNoticeList();
			if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED) && isNetworkConnected(activity)) {
				File file = new File(FOLDER_NAME);
				if (!file.exists()) {
					file.mkdir();
				}
				File mfile = new File(NOTICE_FILE_NAME);		
				try {
					outputStream = new FileOutputStream(mfile);
					for (int i = 0; i < position; i++) {
						mfile.createNewFile();
						String outData = noticeList[i] + DELIMITER;
						byte mByte[] = outData.getBytes();
						outputStream.write(mByte);
					}
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	}
	
	public String[] getNoticeCache(){
		String[] noticeList = null;
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			File mfile = new File(NOTICE_FILE_NAME);
			try {
				input = new FileInputStream(mfile);
				int len = 0;
				byte mByte[] = new byte[NOTICE_ARRAY_SPACE];
				int temp = 0;
				while((temp = input.read()) != END){
					mByte[len] = (byte)temp;
					len++;
				}
				input.close();
				noticeList = (new String(mByte,0,len)).split(DELIMITER);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return noticeList;
	}
	
	public void setNoticeCacheQuantity(int CacheQuantity){
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED) ) {
			File file = new File(FOLDER_NAME);
			if (!file.exists()) {
				file.mkdir();
			}
			File mfile = new File(NOTICE_CACHEQUANTITY_FILE_NAME);		
			try {
				outputStream = new FileOutputStream(mfile);
				mfile.createNewFile();
				String outData =  Integer.toString(CacheQuantity);
				byte mByte[] = outData.getBytes();
				outputStream.write(mByte);
				
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public int getNoticeCacheQuantity(){
		byte[] inputContent = null;
		int length = 0;
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			File mfile = new File(NOTICE_CACHEQUANTITY_FILE_NAME);
			try {
				input = new FileInputStream(mfile);
				inputContent = new byte[NOTICE_QUANTITY_ARRAY_SPACE];
				int temp = 0;
				while((temp = input.read()) != END){
					inputContent[length] = (byte)temp;
					length++;
				}
				input.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return Integer.parseInt(new String(inputContent,0,length));
	}
}


