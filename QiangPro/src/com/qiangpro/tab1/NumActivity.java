package com.qiangpro.tab1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;

import roboguice.inject.InjectView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.qiangpro.main.R;
import com.qiangpro.util.Constants;
import com.qiangpro.util.HttpUpload;
import com.qiangpro.util.UpdateManager;
import com.qiangpro.util.Constants.ConstantValues;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.media.UMImage;

public class NumActivity extends Activity {
	private TextView namedByTab, num_text;
	private String cityListPy ;
	private String cityListCn ;
	private String lotteryDetails = 
			"2014年01月9日/3/2/1/11111/张一野/1/5/大锤#中锤#中锤#中锤#中锤#中锤|" +
			"2014年01月10日/1/2/3/222/张二野/2/5/中锤#锤#中锤小锤#中锤#中锤#中锤|" +
			"2014年01月10日/1/2/3/222/张三野/2/5/中锤#中锤#中锤小锤#中锤#中锤#中锤|" +
			"2014年01月10日/1/2/3/222/张四野/2/5/锤#中锤#中锤小锤#中锤#中锤#中锤|" +
			"2014年01月10日/1/2/3/222/张五野/2/5/锤#中锤#中锤小锤#中锤#中锤#中锤|" +
			"2014年01月11日/2/2/2/3/张野/3/15/大锤#小锤#中锤#中锤#中锤# ";
	private ListView datalist;
	private String data[][];
	private List<Map<String, String>> list = new ArrayList<Map<String, String>>(); 
	private String url = "http://" + ConstantValues.IP + "/goqiang/android/getOpenWin";
	private List<NameValuePair> params = new ArrayList<NameValuePair>();
	final UMSocialService mController = UMServiceFactory.getUMSocialService("com.umeng.share");
	private ProgressDialog pDialog;
	private Handler handler;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.setContentView(R.layout.tab1_num_activity);
		namedByTab = (TextView)this.findViewById(R.id.namedByTab);
		num_text = (TextView)this.findViewById(R.id.num_text);
		namedByTab.setText("中奖号码");
		this.handler = new NumHandle();
		this.datalist = (ListView)this.findViewById(R.id.num_datalist);
		// 设置分享内容
		mController.setShareContent("土匪TOFREE，一款免费领取商品的抢购软件，http://115.29.45.115/goqiang/resources/xml/QiangPro.apk");
		// 设置分享图片
		mController.setShareMedia(new UMImage(NumActivity.this, R.drawable.ic_launcher));
		pDialog = ProgressDialog.show(NumActivity.this, "ToFree", "请稍等……");
		pDialog.setCancelable(true);
		new NumThread().start();
		
	}

	private class NumHandle extends Handler{
		@Override
		public void handleMessage(Message msg){
			super.handleMessage(msg);
			Bundle bundle = new Bundle();
			bundle = msg.getData();
			String info = bundle.getString("msg");
			switch(msg.what){
				case Constants.GOODSDETAILS_SUCCESS:{
					pDialog.dismiss();
					if (!info.equals("")) {
						lotteryDetails = info;
						num_text.setVisibility(View.GONE);
						init();
					}else {
						num_text.setText("无相关中奖信息。请于每天9点以后查看前一日相关中奖信息。");
					}
					
					break;
				}
				
			}
		}
	}
	
	private void init() {
//		SharedPreferences sp = super.getSharedPreferences(ConstantValues.TAB_NUMBER_FILENAME, Activity.MODE_PRIVATE);
		String lotteryDetailsCount[] = lotteryDetails.split("#");
		int length = lotteryDetailsCount.length;
		String date[] = new String[length];
		String figurea[] = new String[length];
		String figureb[] = new String[length];
		String figurec[] = new String[length];
		String personnum[] = new String[length];
		String name[] = new String[length];
		String num[] = new String[length];
		String value[] = new String[length];
		String persona[] = new String[length];
		String personb[] = new String[length];
		String personc[] = new String[length];
		String persond[] = new String[length];
		String persone[] = new String[length];
		String personf[] = new String[length];
		String person[][] = {persona, personb, personc, persond, persone, personf};
		
		for(int i=0; i<length; i++){
			date[i] = lotteryDetailsCount[i].split("/")[0] ;
			figurea[i] = lotteryDetailsCount[i].split("/")[1] ;
			figureb[i] = lotteryDetailsCount[i].split("/")[2] ;
			figurec[i] = lotteryDetailsCount[i].split("/")[3] ;
			personnum[i] = lotteryDetailsCount[i].split("/")[4] ;
			name[i] = lotteryDetailsCount[i].split("/")[5] ;
			num[i] = lotteryDetailsCount[i].split("/")[6] ;
			value[i] = lotteryDetailsCount[i].split("/")[7] ;
			for (int j = 0; j < lotteryDetailsCount[i].split("/")[8].split("\\|").length; j++) {
				person[j][i] = lotteryDetailsCount[i].split("/")[8].split("\\|")[j] ;
			}
//			persona[i] = lotteryDetailsCount[i].split("/")[8].split("\\|")[0] ;
//			personb[i] = lotteryDetailsCount[i].split("/")[8].split("\\|")[1] ;
//			personc[i] = lotteryDetailsCount[i].split("/")[8].split("\\|")[2] ;
//			persond[i] = lotteryDetailsCount[i].split("/")[8].split("\\|")[3] ;
//			persone[i] = lotteryDetailsCount[i].split("/")[8].split("\\|")[4] ;
//			personf[i] = lotteryDetailsCount[i].split("/")[8].split("\\|")[5] ;
		}	
		data = new String[lotteryDetailsCount.length][14];
		for(int i=0; i<lotteryDetailsCount.length; i++){
			for (int j = 0; j <= i; j++) {
			data[i][0] = date[j] ;
			data[i][1] = figurea[j];
			data[i][2] = figureb[j];
			data[i][3] = figurec[j];
			data[i][4] = personnum[j];
			data[i][5] = name[j];
			data[i][6] = num[j];
			data[i][7] = value[j];
			data[i][8] = persona[j];
			data[i][9] = personb[j];
			data[i][10] = personc[j];
			data[i][11] = persond[j];
			data[i][12] = persone[j];
			data[i][13] = personf[j];
			} 
		}
		for (int x = 0; x < this.data.length; x++) {	
			Map<String, String> map = new HashMap<String, String>();
			map.put("date", this.data[x][0]); 	
			map.put("figurea", this.data[x][1]); 		
			map.put("figureb", this.data[x][2]); 		
			map.put("figurec", this.data[x][3]); 			
			map.put("personnum", this.data[x][4]); 			
			map.put("name", this.data[x][5]); 			
			map.put("num", this.data[x][6]); 			
			map.put("value", this.data[x][7]); 			
			map.put("persona", this.data[x][8]); 			
			map.put("personb", this.data[x][9]); 			
			map.put("personc", this.data[x][10]); 			
			map.put("persond", this.data[x][11]); 			
			map.put("persone", this.data[x][12]); 			
			map.put("personf", this.data[x][13]); 			
			
			this.list.add(map); 			
		}
		MyAdapter myadapter = new MyAdapter(this) ;
		this.datalist.setAdapter(myadapter) ;
	}
	
	private class MyAdapter extends BaseAdapter{
		private LayoutInflater inflate ;
		
		public MyAdapter(Context con){
			inflate = LayoutInflater.from(con) ;
		}
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder holder = null;  
			if (convertView == null) {  
	             holder=new ViewHolder();    
	             //可以理解为从vlist获取view  之后把view返回给ListView  
	             convertView = inflate.inflate(R.layout.tab1_num_data_list, null);  
	             holder.date = (TextView) convertView.findViewById(R.id.tab1_num_data_textview1);
	             holder.figurea = (TextView) convertView.findViewById(R.id.tab1_num_data_num1);
	             holder.figureb = (TextView) convertView.findViewById(R.id.tab1_num_data_num2);
	             holder.figurec = (TextView) convertView.findViewById(R.id.tab1_num_data_num3);
	             holder.personnum = (TextView) convertView.findViewById(R.id.tab1_num_data_textview2);
	             holder.name = (TextView) convertView.findViewById(R.id.tab1_num_data_textview3);
	             holder.num = (TextView) convertView.findViewById(R.id.tab1_num_data_textview5);
	             holder.value = (TextView) convertView.findViewById(R.id.tab1_num_data_textview6);
	             holder.persona = (TextView) convertView.findViewById(R.id.tab1_num_data_textview7);
	             holder.personb = (TextView) convertView.findViewById(R.id.tab1_num_data_textview8);
	             holder.personc = (TextView) convertView.findViewById(R.id.tab1_num_data_textview9);
	             holder.persond = (TextView) convertView.findViewById(R.id.tab1_num_data_textview10);
	             holder.persone = (TextView) convertView.findViewById(R.id.tab1_num_data_textview11);
	             holder.personf = (TextView) convertView.findViewById(R.id.tab1_num_data_textview12);
	             holder.share = (ImageView)convertView.findViewById(R.id.tab1_num_data_share);  
	             convertView.setTag(holder);               
	         }else {               
	             holder = (ViewHolder)convertView.getTag();  
	         }         
			holder.date.setText((String)list.get(position).get("date"));  
			holder.figurea.setText((String)list.get(position).get("figurea"));
			holder.figureb.setText((String)list.get(position).get("figureb"));
			holder.figurec.setText((String)list.get(position).get("figurec"));
			holder.personnum.setText((String)list.get(position).get("personnum"));
			holder.name.setText((String)list.get(position).get("name"));
			holder.num.setText((String)list.get(position).get("num"));
			holder.value.setText((String)list.get(position).get("value"));
			holder.persona.setText((String)list.get(position).get("persona"));
			holder.personb.setText((String)list.get(position).get("personb"));
			holder.personc.setText((String)list.get(position).get("personc"));
			holder.persond.setText((String)list.get(position).get("persond"));
			holder.persone.setText((String)list.get(position).get("persone"));
			holder.personf.setText((String)list.get(position).get("personf"));
			
			holder.share.setTag(position); 
	         //给Button添加单击事件  添加Button之后ListView将失去焦点  需要的直接把Button的焦点去掉  
	        holder.share.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
				mController.openShare(NumActivity.this, false);
				}
	         });  

	         return convertView;  
		}  
	}  
			
	public final class ViewHolder {  
		public TextView date ;
		public TextView figurea ;
		public TextView figureb ;
		public TextView figurec ;
		public TextView personnum ;
		public TextView name ;
		public TextView num ;
		public TextView value ;
		public TextView persona ;
		public TextView personb ;
		public TextView personc ;
		public TextView persond ;
		public TextView persone ;
		public TextView personf ;
		
	    public ImageView share ;
	} 
	
	private class NumThread extends Thread{
		@Override
		public void run(){		
			String info = HttpUpload.post(url, params);
			Bundle bundle = new Bundle();
			Message message = new Message();
			bundle.putString("msg", info);
			message.setData(bundle);
			message.what = Constants.GOODSDETAILS_SUCCESS;
			handler.sendMessage(message);
		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
}
