package com.qiangpro.tab1;

import static com.qiangpro.util.Constants.LIST_QUANTITY;
import static com.qiangpro.util.HttpUpload.post;
import static com.qiangpro.util.NetworkConnection.isNetworkConnected;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.inject.Inject;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.qiangpro.application.CustomApplication;
import com.qiangpro.main.R;
import com.qiangpro.service.CacheService;
import com.qiangpro.service.ListViewCreateService;
import com.qiangpro.service.NetworkingService;
import com.umeng.analytics.MobclickAgent;

public class NoticeActivity extends RoboActivity {
	@InjectView(R.id.notice_list)
	private PullToRefreshListView pullToRefreshListView;
	
	@Inject
	private NetworkingService networkingService;
	@Inject
	private ListViewCreateService listViewCreateService;
	@Inject
	private CacheService cacheService;
	@Inject
    private LayoutInflater inflater;
	@Inject
	private Bundle bundle;
	@InjectView(R.id.namedByTab)
	private TextView namedByTab; 
	private Message message;
	
	private NoticeListHandler noticeListHandler;
	private NoticeListThread noticeListThread;
	private String noticeURL = "http://115.29.45.115/goqiang/android/getAllNotice";
	private List<NameValuePair> params = new ArrayList<NameValuePair>();
	private CustomApplication customApplication;
	private String refreshHeadContent;
	private ProgressDialog pDialog;
	private View listFooter;
	private ListView noticeListView;
	
	private int listEndPosition;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.setContentView(R.layout.tab1_notice_activity);
		customApplication = (CustomApplication)NoticeActivity.this.getApplication();
		pDialog = ProgressDialog.show(NoticeActivity.this, "ToFree", "请稍等……");
		pDialog.setCancelable(true);
		namedByTab.setText("通知消息");
		noticeListThread();
		getListFooter();
		noticeListView = pullToRefreshListView.getRefreshableView();
		super.registerForContextMenu(noticeListView);
		customApplication.setListEndPosition(LIST_QUANTITY);
	}

	private void noticeListThread() {
		noticeListHandler = new NoticeListHandler();
		noticeListThread = new NoticeListThread();
		new Thread(noticeListThread).start();
	}

	private void getListFooter() {
		listFooter = inflater.inflate(R.layout.list_footer, null);
	}
	
	private void initDropDownRefresh() {
		pullToRefreshListView.setOnRefreshListener(new dropDownListener());
	}
	
	private void initLoadMore() {
		pullToRefreshListView.setOnLastItemVisibleListener(new LoadMoreListener());
	}
	
	private class dropDownListener implements OnRefreshListener<ListView> {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			refreshHeadContent = DateUtils.formatDateTime(NoticeActivity.this,
					System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME| DateUtils.FORMAT_SHOW_DATE| DateUtils.FORMAT_ABBREV_ALL);
			refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(refreshHeadContent);
			new PullDownRefreshDataTask().execute();
		}
	}
	
	private class PullDownRefreshDataTask extends AsyncTask<Void, Void, String[]> {
		@Override
		protected String[] doInBackground(Void... params) {
			return null;
		}

		@Override
		protected void onPostExecute(String[] result) {
           if(isNetworkConnected(NoticeActivity.this)){
     		   noticeListThread();
        	   customApplication.setListEndPosition(LIST_QUANTITY);		
           }else{
        	   Toast.makeText(NoticeActivity.this, "网络连接断开，请检查网络设置！", Toast.LENGTH_SHORT).show();
           }
           pullToRefreshListView.onRefreshComplete();
           super.onPostExecute(result);
		}
	}
	
	private void createNoticeListView(){
    	String [] noticeList = networkingService.init(NoticeActivity.this).getNoticeList();
	    	if(noticeList.length < LIST_QUANTITY){
	    		listViewCreateService.init(NoticeActivity.this, noticeListView).createNoticeList(noticeList.length,listFooter);
	    		cacheService.saveNoticeCache(noticeList.length,NoticeActivity.this);
	    		noticeListView.removeFooterView(listFooter);
	    	}else{
	    		listViewCreateService.init(NoticeActivity.this, noticeListView).createNoticeList(LIST_QUANTITY,listFooter);
	    		cacheService.saveNoticeCache(LIST_QUANTITY,NoticeActivity.this);
	    	}
    }
	
	private class LoadMoreListener implements OnLastItemVisibleListener {
		@Override
		public void onLastItemVisible() {
			if(isNetworkConnected(NoticeActivity.this)){
				showLoadMoreList();
			}
		}
	}
	
	private void showLoadMoreList(){
		String[] noticeList = networkingService.init(NoticeActivity.this).getNoticeList();
			int listEndPosition1 = customApplication.getListEndPosition();
			if (noticeList.length > listEndPosition1 + LIST_QUANTITY - 1) {
				listEndPosition = listViewCreateService.init(NoticeActivity.this,noticeListView).getListEndPosition(LIST_QUANTITY);
				listViewCreateService.init(NoticeActivity.this,noticeListView).createNoticeList(listEndPosition,listFooter);
				noticeListView.setSelection(listEndPosition -LIST_QUANTITY-2);
				cacheService.saveNoticeCache(listEndPosition,NoticeActivity.this);	
			} else {
				listEndPosition = listViewCreateService.init(NoticeActivity.this,noticeListView).getListEndPosition(noticeList.length-listEndPosition1);
				listViewCreateService.init(NoticeActivity.this,noticeListView).createNoticeList(listEndPosition,listFooter);
				noticeListView.setSelection(listEndPosition -noticeList.length+listEndPosition1-2);
				cacheService.saveNoticeCache(listEndPosition,NoticeActivity.this);
			}
    }
	
	private class NoticeListThread implements Runnable{
		@Override
		public void run(){
			String a = post(noticeURL, params);
			bundle.putString("notice", a);
			message = new Message();
			message.setData(bundle);
			NoticeActivity.this.noticeListHandler.sendMessage(message);
		}
	}
	
	private class NoticeListHandler extends Handler {
	        @Override
	        public void handleMessage(Message msg) {
	            super.handleMessage(msg);
	            if(isNetworkConnected(NoticeActivity.this)) {
		            Bundle b = msg.getData();
		            String notice = b.getString("notice");
		            customApplication.setNoticeData(notice);
		            pDialog.dismiss();
		            createNoticeListView();
		            initDropDownRefresh();
		    		initLoadMore();
	            } else {
	            	pDialog.dismiss();
	            	createNoticeListView();
		            initDropDownRefresh();
		    		initLoadMore();
	            }
	        }
	    }
	
	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}
	
}
