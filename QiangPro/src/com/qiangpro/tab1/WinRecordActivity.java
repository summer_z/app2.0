package com.qiangpro.tab1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import roboguice.inject.InjectView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.qiangpro.main.R;
import com.qiangpro.util.Constants;
import com.qiangpro.util.Constants.ConstantValues;
import com.qiangpro.util.HttpUpload;
import com.umeng.analytics.MobclickAgent;

public class WinRecordActivity extends Activity {
	private TextView namedByTab; 
	private SharedPreferences sp;
	private TextView tab1_notes_textview;
	private ListView datalist;
	private SimpleAdapter simpleAdapter = null; 
	private String[] notesWin;
	private List<Map<String, String>> list = new ArrayList<Map<String, String>>(); 
	private String data[][];
	private ProgressDialog pDialog;
	private Handler handler;
	private String urlNotesWin = "http://" + ConstantValues.IP + "/goqiang/android/noteswin";
	private SharedPreferences.Editor edit2;
	private List<NameValuePair> param = new ArrayList<NameValuePair>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.setContentView(R.layout.tab1_win_record_activity);
		namedByTab = (TextView)this.findViewById(R.id.namedByTab);
		namedByTab.setText("中奖记录");
		this.handler = new WinRecordHandle();
		tab1_notes_textview = (TextView)super.findViewById(R.id.tab1_notes_textview);
		datalist = (ListView)super.findViewById(R.id.tab1_notes_datalist);
		
		sp = WinRecordActivity.this.getSharedPreferences(Constants.NOTES_WIN_FILENAME, Activity.MODE_PRIVATE);
		edit2 = sp.edit();
//		init();
		pDialog = ProgressDialog.show(WinRecordActivity.this, "ToFree", "请稍等……");
		pDialog.setCancelable(true);
		NotesThread notesThread = new NotesThread();
		notesThread.start();
		
	}

	private void init() {
		String msg = sp.getString("notesWin", "无中奖信息");
		if(msg.equals("")){
			tab1_notes_textview.setText("无中奖信息");
		}else{
			tab1_notes_textview.setVisibility(View.GONE);
			notesWin = msg.split("\\|");
			int index = notesWin.length/4;
			data = new String[index][4];
			for(int i=0; i<index; i++){
				String info[] = new String[4];//一条数据四个属性
				for(int j=0; j<info.length; j++){  
		             data[i][j] = notesWin[j + i*4];  
		         }
			}
			for (int x = 0; x < this.data.length; x++) {	
				Map<String, String> map = new HashMap<String, String>();
				map.put("goods_id", this.data[x][0]); 
				map.put("goods_name", this.data[x][1]); 		
				map.put("goods_value", this.data[x][2]); 		
				map.put("godds_convert_final", this.data[x][3]); 			
				
				this.list.add(map); 			
			}
			this.simpleAdapter = new SimpleAdapter(this, 		
					this.list,			
					R.layout.tab1_win_record_data_list, 		// 要使用的显示模板
					new String[] { "goods_id", "goods_name", "goods_value", "godds_convert_final" }, // 定义显示Key
					new int[] { R.id.notes_data_id, R.id.notes_data_name, R.id.notes_data_value, R.id.notes_data_date });	 // 与模板中的组件匹配
			this.datalist.setAdapter(this.simpleAdapter);
			
		}
	}
	
	private class NotesThread extends Thread{
		@Override
		public void run(){
			param.clear();
			SharedPreferences sp2 = WinRecordActivity.this.getSharedPreferences(Constants.SAVE_NAME_PWD_FILENAME, Activity.MODE_PRIVATE);
			param.add(new BasicNameValuePair("username", sp2.getString("username", "0")));
			param.add(new BasicNameValuePair("password", sp2.getString("password", "0")));
			String infoNotesWin = HttpUpload.post(urlNotesWin, param);
			Bundle b = new Bundle();
			b.putString("msg", infoNotesWin);
			Message msg = new Message();
			msg.setData(b);		
			msg.what = Constants.GOODSDETAILS_SUCCESS;		
			handler.sendMessage(msg);
		}
	}
	
	private class WinRecordHandle extends Handler{
		@Override
		public void handleMessage(Message msg){
			super.handleMessage(msg);
			Bundle bundle = new Bundle();
			bundle = msg.getData();
			String msgStr = bundle.getString("msg");
			switch(msg.what){
				case Constants.GOODSDETAILS_SUCCESS:{
					pDialog.dismiss();
					if(msgStr.equals("请检查你的网络设置")){
						Toast.makeText(WinRecordActivity.this, msgStr, Toast.LENGTH_SHORT).show();
					}else{
						edit2.putString("notesWin", msgStr);
						edit2.commit();
						init();
					}
					
					break;
				}
			}
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}
}
