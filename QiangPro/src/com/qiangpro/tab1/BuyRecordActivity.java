package com.qiangpro.tab1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import roboguice.inject.InjectView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.qiangpro.main.R;
import com.qiangpro.util.Constants;
import com.qiangpro.util.Constants.ConstantValues;
import com.qiangpro.util.HttpUpload;
import com.umeng.analytics.MobclickAgent;

public class BuyRecordActivity extends Activity {
	private TextView namedByTab; 
	private SharedPreferences sp;
	private TextView tab1_buy_record_textview;
	private ListView datalist;
	private SimpleAdapter simpleAdapter = null; 
	private String[] notesWin;
	private List<Map<String, String>> list = new ArrayList<Map<String, String>>(); 
	private String data[][];
	private ProgressDialog pDialog;
	private Handler handler;
	private String urlNotesWin = "http://" + ConstantValues.IP + "/goqiang/android/notesbuy";
	private SharedPreferences.Editor edit2;
	private List<NameValuePair> param = new ArrayList<NameValuePair>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.setContentView(R.layout.tab1_buy_record_activity);
		namedByTab = (TextView)this.findViewById(R.id.namedByTab);
		namedByTab.setText("购买记录");
		this.handler = new BuyRecordHandle();//返回信息
		tab1_buy_record_textview = (TextView)super.findViewById(R.id.tab1_buy_record_textview);
		datalist = (ListView)super.findViewById(R.id.tab1_buy_record_datalist);
		
		sp = BuyRecordActivity.this.getSharedPreferences(Constants.NOTES_BUY_FILENAME, Activity.MODE_PRIVATE);
		edit2 = sp.edit();
		
		pDialog = ProgressDialog.show(BuyRecordActivity.this, "ToFree", "请稍等……");
		pDialog.setCancelable(true);
		NotesThread notesThread = new NotesThread();
		notesThread.start();
		
	}
	
	private void init() {
		String msg = sp.getString("notesBuy", "无购买记录");
		if(msg.equals("")){
			tab1_buy_record_textview.setText("无购买记录");
		}else{
			tab1_buy_record_textview.setVisibility(View.GONE);
			notesWin = msg.split("\\|");
			int index = notesWin.length/5;
			data = new String[index][5];
			for(int i=0; i<index; i++){
				String info[] = new String[5];//一条数据5个值
				for(int j=0; j<info.length; j++){  
		             data[i][j] = notesWin[j + i*5];  
		         }
			}
			for (int x = this.data.length-1; x >= 0; x--) {	
				Map<String, String> map = new HashMap<String, String>();
				String flagString;
				int grade = Integer.parseInt(this.data[x][3]) * Integer.parseInt(this.data[x][4]);
				String gradeString ;
				if(this.data[x][0].equals("1")){
					flagString = "状态：已完成";
					gradeString = "已获积分：" + Integer.toString(grade) + "元";
				}else{
					flagString = "状态：派送中";
					gradeString = "可获积分：" + Integer.toString(grade) + "元";
				}
				
				map.put("buy_record_state", flagString); 
				map.put("buy_record_grade", gradeString); 		
				map.put("buy_record_goodsname", this.data[x][1]); 		
				map.put("buy_record_info", this.data[x][2]); 			
				map.put("buy_record_price", this.data[x][3]); 			
				map.put("buy_record_num", this.data[x][4]); 			
				
				this.list.add(map); 			
			}
			this.simpleAdapter = new SimpleAdapter(this, 		
					this.list,			
					R.layout.tab1_buy_record_data_list, 		// 要使用的显示模板
					new String[] { "buy_record_state", "buy_record_grade", "buy_record_goodsname", 
					"buy_record_info", "buy_record_price", "buy_record_num" }, // 定义显示Key
					new int[] { R.id.buy_record_state, R.id.buy_record_grade, R.id.buy_record_goodsname, 
					R.id.buy_record_info, R.id.buy_record_price, R.id.buy_record_num });	 // 与模板中的组件匹配
			this.datalist.setAdapter(this.simpleAdapter);
			
		}
	}
	
	private class NotesThread extends Thread{
		@Override
		public void run(){
			param.clear();
			SharedPreferences sp2 = BuyRecordActivity.this.getSharedPreferences(Constants.SAVE_NAME_PWD_FILENAME, Activity.MODE_PRIVATE);
			param.add(new BasicNameValuePair("username", sp2.getString("username", "0")));
			String infoNotesWin = HttpUpload.post(urlNotesWin, param);
			Bundle b = new Bundle();
			b.putString("msg", infoNotesWin);
			Message msg = new Message();
			msg.setData(b);		
			msg.what = Constants.GOODSDETAILS_SUCCESS;		
			handler.sendMessage(msg);
		}
	}
	
	private class BuyRecordHandle extends Handler{
		@Override
		public void handleMessage(Message msg){
			super.handleMessage(msg);
			Bundle bundle = new Bundle();
			bundle = msg.getData();
			String msgStr = bundle.getString("msg");
			switch(msg.what){
				case Constants.GOODSDETAILS_SUCCESS:{
					pDialog.dismiss();
					if(msgStr.equals("请检查你的网络设置")){
						Toast.makeText(BuyRecordActivity.this, msgStr, Toast.LENGTH_SHORT).show();
					}else{
						edit2.putString("notesBuy", msgStr);
						edit2.commit();
						init();
					}
					
					break;
				}
			}
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}
}
