package com.qiangpro.tab1;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.qiangpro.adapter.ListViewAdapter;
import com.qiangpro.main.R;
import com.qiangpro.parse.RemoteImageLoader;
import com.qiangpro.util.Constants.ConstantValues;
import com.qiangpro.util.NetworkConnection;
import com.umeng.analytics.MobclickAgent;

public class SectionInformationActivity extends Activity {

	private TextView namedByTab;
	private PullToRefreshListView pullToRefreshListView;
	private ListView fromtab1ActivityList;
	private ListViewAdapter adapter;
	private String refreshHeadContent;
	private static LayoutInflater inflater=null;
	private View listFooter;
	private boolean flag=true;
	private File cacheDir;
	private File cacheFile;
	private RemoteImageLoader remoteImageLoader ;
	private String tab1ImageUrl;
	private Bitmap bitmap ;
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		Intent it = super.getIntent();
		tab1ImageUrl = it.getStringExtra("tab1ImageUrl");
		inflater = (LayoutInflater)SectionInformationActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		listFooter = inflater.inflate(R.layout.list_footer, null);
		super.setContentView(R.layout.tab2_activity);
		initViewId();
		
		setListener();
		fromtab1ActivityList = pullToRefreshListView.getRefreshableView();
		fromtab1ActivityList.addFooterView(listFooter,false, false);
		getUrLArray();
		fromtab1ActivityList.setAdapter(adapter);
		registerForContextMenu(fromtab1ActivityList);
	}
	
	public void initViewId(){
		namedByTab = (TextView)this.findViewById(R.id.namedByTab);
		pullToRefreshListView=(PullToRefreshListView)this.findViewById(R.id.tab2activity_list);
	}
	
	
	public void setListener() {
		pullToRefreshListView.setOnRefreshListener(new dropDownListener());
		pullToRefreshListView.setOnLastItemVisibleListener(new LoadMoreListener());
	}
	
	private class LoadMoreListener implements OnLastItemVisibleListener {
		@Override
		public void onLastItemVisible() {
			if(NetworkConnection.isNetworkConnected(SectionInformationActivity.this)){
				
				if(flag){
					 try {
			                Thread.sleep(1000);
			            } catch (InterruptedException e) {
			            }
					fromtab1ActivityList.removeFooterView(listFooter);
					Toast.makeText(SectionInformationActivity.this, getString(R.string.contentNoMore), Toast.LENGTH_SHORT).show() ;
					flag=false;
				}else{
					fromtab1ActivityList.addFooterView(listFooter,false, false);
					flag=true;
				}
			}else{
			   fromtab1ActivityList.removeFooterView(listFooter);
			   Toast.makeText( SectionInformationActivity.this, getString(R.string.unconnected), Toast.LENGTH_SHORT).show();
			}
		}
	}
	
    private class dropDownListener implements OnRefreshListener<ListView> {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			 if(NetworkConnection.isNetworkConnected(SectionInformationActivity.this)){
			refreshHeadContent = DateUtils.formatDateTime(SectionInformationActivity.this,
					System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME| DateUtils.FORMAT_SHOW_DATE| DateUtils.FORMAT_ABBREV_ALL);
			refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(refreshHeadContent);
			new PullDownRefreshDataTask().execute();
			}else{
	        	  Toast.makeText( SectionInformationActivity.this, getString(R.string.unconnected), Toast.LENGTH_SHORT).show();
	        	  pullToRefreshListView.onRefreshComplete();
			}
		}
	}

	private class PullDownRefreshDataTask extends AsyncTask<Void, Void, String[]> {
		@Override
		protected String[] doInBackground(Void... params) {
			
				  if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
			            cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"LazyList");
				  } else{
			            cacheDir=SectionInformationActivity.this.getApplicationContext().getCacheDir();
			        if(!cacheDir.exists())
			            cacheDir.mkdirs();
			    }
				  remoteImageLoader =new RemoteImageLoader(SectionInformationActivity.this.getApplicationContext());
				  for(int i=0;i<8;i++){
//					  if(tab1ImageUrl.equals("0")){
//						  	bitmap = remoteImageLoader.getHttpBitmap(ConstantValues.tab1activityImage1UrlArray[i]);
//						}else if(tab1ImageUrl.equals("1")){
//							bitmap = remoteImageLoader.getHttpBitmap(ConstantValues.tab1activityImage2UrlArray[i]);
//						}else if(tab1ImageUrl.equals("2")){
//							bitmap = remoteImageLoader.getHttpBitmap(ConstantValues.tab1activityImage3UrlArray[i]);
//						}else if(tab1ImageUrl.equals("3")){
//							bitmap = remoteImageLoader.getHttpBitmap(ConstantValues.tab1activityImage4UrlArray[i]);
//						}else if(tab1ImageUrl.equals("4")){
//							bitmap = remoteImageLoader.getHttpBitmap(ConstantValues.tab1activityImage5UrlArray[i]);
//						}else if(tab1ImageUrl.equals("5")){
//							bitmap = remoteImageLoader.getHttpBitmap(ConstantValues.tab1activityImage6UrlArray[i]);
//						}else if(tab1ImageUrl.equals("6")){
//							bitmap = remoteImageLoader.getHttpBitmap(ConstantValues.tab1activityImage7UrlArray[i]);
//						}else if(tab1ImageUrl.equals("7")){
//							bitmap = remoteImageLoader.getHttpBitmap(ConstantValues.tab1activityImage8UrlArray[i]);
//						}
						
						
						if(bitmap != null){
//							if(tab1ImageUrl.equals("0")){
//								cacheFile = new File(cacheDir,String.valueOf(ConstantValues.tab1activityImage1UrlArray[i].hashCode()));
//							}else if(tab1ImageUrl.equals("1")){
//								cacheFile = new File(cacheDir,String.valueOf(ConstantValues.tab1activityImage2UrlArray[i].hashCode()));
//							}else if(tab1ImageUrl.equals("2")){
//								cacheFile = new File(cacheDir,String.valueOf(ConstantValues.tab1activityImage3UrlArray[i].hashCode()));
//							}else if(tab1ImageUrl.equals("3")){
//								cacheFile = new File(cacheDir,String.valueOf(ConstantValues.tab1activityImage4UrlArray[i].hashCode()));
//							}else if(tab1ImageUrl.equals("4")){
//								cacheFile = new File(cacheDir,String.valueOf(ConstantValues.tab1activityImage5UrlArray[i].hashCode()));
//							}else if(tab1ImageUrl.equals("5")){
//								cacheFile = new File(cacheDir,String.valueOf(ConstantValues.tab1activityImage6UrlArray[i].hashCode()));
//							}else if(tab1ImageUrl.equals("6")){
//								cacheFile = new File(cacheDir,String.valueOf(ConstantValues.tab1activityImage7UrlArray[i].hashCode()));
//							}else if(tab1ImageUrl.equals("7")){
//								cacheFile = new File(cacheDir,String.valueOf(ConstantValues.tab1activityImage8UrlArray[i].hashCode()));
//							}
							try {
								cacheFile.createNewFile();
								OutputStream output = new FileOutputStream(cacheFile);
								bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
								output.flush();
								output.close();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
			return null;
		}

		@Override
		protected void onPostExecute(String[] result) {
		   getUrLArray();
		   fromtab1ActivityList.setAdapter(adapter);
		   Toast.makeText(SectionInformationActivity.this, getString(R.string.updateSuccess), Toast.LENGTH_SHORT).show() ;
           pullToRefreshListView.onRefreshComplete();
           super.onPostExecute(result);
		}
	}
	
	 public void showImageview1Detail(View v){
		 Toast.makeText(SectionInformationActivity.this, "MDan1", Toast.LENGTH_SHORT).show();
	 }
	
 	public void showImageview2Detail(View v){
 		 Toast.makeText(SectionInformationActivity.this, "MDan2", Toast.LENGTH_SHORT).show();
	 }
 	
 	public void showImageview3Detail(View v){
 		 Toast.makeText(SectionInformationActivity.this, "MDan3", Toast.LENGTH_SHORT).show();
	 }
 	
 	public void showImageview4Detail(View v){
 		 Toast.makeText(SectionInformationActivity.this, "MDan4", Toast.LENGTH_SHORT).show();
	 }
	
 	public void showImageview5Detail(View v){
 		 Toast.makeText(SectionInformationActivity.this, "MDan5", Toast.LENGTH_SHORT).show();
	 }
 	
 	public void showImageview6Detail(View v){
 		 Toast.makeText(SectionInformationActivity.this, "MDan6", Toast.LENGTH_SHORT).show();
	 }
 	
 	public void showImageview7Detail(View v){
 		Toast.makeText(SectionInformationActivity.this, "MDan7", Toast.LENGTH_SHORT).show();
	 }
	
	public void showImageview8Detail(View v){
		Toast.makeText(SectionInformationActivity.this, "MDan8", Toast.LENGTH_SHORT).show();
	 }
	
	
	public void getUrLArray(){
//		if(tab1ImageUrl.equals("0")){
//			namedByTab.setText("���һ");
//			adapter=new ListViewAdapter(this, ConstantValues.tab1activityImage1UrlArray);
//		}else if(tab1ImageUrl.equals("1")){
//			namedByTab.setText("����");
//			adapter=new ListViewAdapter(this, ConstantValues.tab1activityImage2UrlArray);
//		}else if(tab1ImageUrl.equals("2")){
//			namedByTab.setText("�����");
//			adapter=new ListViewAdapter(this, ConstantValues.tab1activityImage3UrlArray);	
//		}else if(tab1ImageUrl.equals("3")){
//			namedByTab.setText("�����");
//			adapter=new ListViewAdapter(this, ConstantValues.tab1activityImage4UrlArray);
//		}else if(tab1ImageUrl.equals("4")){
//			namedByTab.setText("�����");
//			adapter=new ListViewAdapter(this, ConstantValues.tab1activityImage5UrlArray);
//		}else if(tab1ImageUrl.equals("5")){
//			namedByTab.setText("�����");
//			adapter=new ListViewAdapter(this, ConstantValues.tab1activityImage6UrlArray);
//		}else if(tab1ImageUrl.equals("6")){
//			namedByTab.setText("�����");
//			adapter=new ListViewAdapter(this, ConstantValues.tab1activityImage7UrlArray);
//		}else if(tab1ImageUrl.equals("7")){
//			namedByTab.setText("����");
//			adapter=new ListViewAdapter(this, ConstantValues.tab1activityImage8UrlArray);
//		}
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
	
	@Override
    public void onDestroy()
    {
        super.onDestroy();
    }
}
