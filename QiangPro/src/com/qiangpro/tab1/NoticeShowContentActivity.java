package com.qiangpro.tab1;

import static com.qiangpro.util.NetworkConnection.isNetworkConnected;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.inject.Inject;
import com.qiangpro.main.R;
import com.qiangpro.service.CacheService;
import com.qiangpro.service.NetworkingService;
import com.umeng.analytics.MobclickAgent;

public class NoticeShowContentActivity extends RoboActivity {
	@InjectView(R.id.title)
	private TextView title;
	
	@InjectView(R.id.date)
	private TextView date;
	
	@InjectView(R.id.content)
	private TextView detailsContent;
	
	@Inject
	private NetworkingService networkingService;
	@Inject
	private CacheService cacheService;
	
	private TextView namedByTab; 
	private String[] showDetails;
	private int position;
	
	private static String KEY = "position";
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.setContentView(R.layout.notice_show_content_activity);
		namedByTab = (TextView)findViewById(R.id.namedByTab);
		namedByTab.setText("����");
		getPreviousData();
		showContent();
	}
	
	private String[] getShowDetails(){
		if(isNetworkConnected(NoticeShowContentActivity.this)) {
			showDetails = networkingService.init(NoticeShowContentActivity.this).getNoticeList();
		} else {
			showDetails = cacheService.getNoticeCache();
		}
		return showDetails;
	}
	
	private void getPreviousData(){
		Intent intent=super.getIntent();
		position=Integer.parseInt(intent.getStringExtra(KEY))-1;
	}
	
	private void showContent(){
		title.setText(getShowDetails()[position].split("\\|")[0]);
		detailsContent.setText(getShowDetails()[position].split("\\|")[1]);
		date.setText(getShowDetails()[position].split("\\|")[2]);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}
}
