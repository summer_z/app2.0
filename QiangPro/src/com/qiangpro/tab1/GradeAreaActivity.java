package com.qiangpro.tab1;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.qiangpro.adapter.GradeAreaListViewAdapter;
import com.qiangpro.adapter.ListViewAdapter;
import com.qiangpro.common.GoodsInformationActivity;
import com.qiangpro.main.R;
import com.qiangpro.parse.RemoteImageLoader;
import com.qiangpro.util.Constants;
import com.qiangpro.util.Constants.ConstantValues;
import com.qiangpro.util.HttpUpload;
import com.qiangpro.util.NetworkConnection;
import com.umeng.analytics.MobclickAgent;
 
public class GradeAreaActivity extends Activity {
	private TextView namedByTab;
	private PullToRefreshListView pullToRefreshListView;
	private ListView tab2ActivityList;
	private GradeAreaListViewAdapter adapter;
	private String refreshHeadContent;
	private static LayoutInflater inflater=null;
	private boolean flag=true;
	private File cacheDir;
	private RemoteImageLoader remoteImageLoader ;
	private int count = 1;
	private String recommendNumURL = "http://115.29.45.115/goqiang/android/getGradeAreaNum";
	private List<NameValuePair> params = new ArrayList<NameValuePair>();
	private Handler recommendNumHandler;
	//缓存图片名字
    private String[] imageName = {"grade1.jpg", "grade2.jpg", "grade3.jpg",  "grade4.jpg",
    		"grade5.jpg", "grade6.jpg", "grade7.jpg", "grade8.jpg"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.setContentView(R.layout.tab2_activity);
		initViewId();
		recommendNumHandler = new RecommendNumHandle();
		namedByTab.setText("积分专区");
		setListener();
		tab2ActivityList = pullToRefreshListView.getRefreshableView();
//		tab2ActivityList.addFooterView(listFooter,false, false);
		adapter=new GradeAreaListViewAdapter(this, ConstantValues.tab1GradeUrls1, imageName);
		tab2ActivityList.setAdapter(adapter);
		registerForContextMenu(tab2ActivityList);
	}
	
	private void initViewId(){
		namedByTab = (TextView)this.findViewById(R.id.namedByTab);
		pullToRefreshListView=(PullToRefreshListView)this.findViewById(R.id.tab2activity_list);
		remoteImageLoader =new RemoteImageLoader(GradeAreaActivity.this.getApplicationContext());
	}
	
	private void setListener() {
		pullToRefreshListView.setMode(com.handmark.pulltorefresh.library.PullToRefreshBase.Mode.BOTH);
		pullToRefreshListView.setOnRefreshListener(new PullStateListener());
		
		pullToRefreshListView.getLoadingLayoutProxy(false, true).setPullLabel("上拉刷新");  
		pullToRefreshListView.getLoadingLayoutProxy(false, true).setRefreshingLabel("正在载入……");  
		pullToRefreshListView.getLoadingLayoutProxy(false, true).setReleaseLabel("放开以刷新");  
	}

    private class PullStateListener implements OnRefreshListener2<ListView>{
    	//下拉刷新
		@Override
		public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			if(NetworkConnection.isNetworkConnected(GradeAreaActivity.this)){
				refreshHeadContent = DateUtils.formatDateTime(GradeAreaActivity.this,
						System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME| DateUtils.FORMAT_SHOW_DATE| DateUtils.FORMAT_ABBREV_ALL);
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(refreshHeadContent);
				new PullDownRefreshDataTask().execute();
			}else{
	        	Toast.makeText( GradeAreaActivity.this, getString(R.string.unconnected), Toast.LENGTH_SHORT).show();
	        	pullToRefreshListView.onRefreshComplete();
			}
		}
		//上拉刷新
		@Override
		public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			if(NetworkConnection.isNetworkConnected(GradeAreaActivity.this)){
				count ++;//用于adapter呈现多少组图片
				new RecommendNumThread().start();
			}else{
	        	Toast.makeText( GradeAreaActivity.this, getString(R.string.unconnected), Toast.LENGTH_SHORT).show();
	        	pullToRefreshListView.onRefreshComplete();
			}
			
		}
    	
    }
    
	private class PullDownRefreshDataTask extends AsyncTask<Void, Void, String[]> {
		@Override
		protected String[] doInBackground(Void... params) {
			//要改！！
			remoteImageLoader.saveGradeImage();
			return null;
		}

		@Override
		protected void onPostExecute(String[] result) {
		    adapter=new GradeAreaListViewAdapter(GradeAreaActivity.this, ConstantValues.tab1GradeUrls1, imageName);
		    tab2ActivityList.setAdapter(adapter);
//			adapter.listViewImageLoader.clearCache();
//		    adapter.notifyDataSetChanged();
		    Toast.makeText(GradeAreaActivity.this, getString(R.string.updateSuccess), Toast.LENGTH_SHORT).show() ;
            pullToRefreshListView.onRefreshComplete();
            count = 1;
            super.onPostExecute(result);
		}
	}
	
	private class RecommendNumHandle extends Handler{
		@Override
		public void handleMessage(Message msg){
			Bundle bundle = new Bundle();
			bundle = msg.getData();
			String info = bundle.getString("info");
			switch (msg.what) {
			case Constants.GOODSDETAILS_SUCCESS:
				if (info.equals("请检查您的网络设置")) {
					Toast.makeText( GradeAreaActivity.this, info, Toast.LENGTH_SHORT).show();
				}else{
					int num = Integer.parseInt(info);
					pullToRefreshListView.onRefreshComplete();
					if (num == count*8) {
						adapter.setCount(count);
						if (count == 2) {
							adapter.setUrlArray(ConstantValues.tab1GradeUrls2);
						}else if(count == 3){
							adapter.setUrlArray(ConstantValues.tab1GradeUrls3);
						}else if (count == 4) {
							adapter.setUrlArray(ConstantValues.tab1GradeUrls4);
						}else if (count == 5) {
							adapter.setUrlArray(ConstantValues.tab1GradeUrls5);
						}
				
						adapter.notifyDataSetChanged();
						Toast.makeText( GradeAreaActivity.this, "刷新成功", Toast.LENGTH_SHORT).show();
					}else {
						Toast.makeText( GradeAreaActivity.this, "没有更多了", Toast.LENGTH_SHORT).show();
					}
				}
				break;
			default:
				break;
			}
		}
	}
	
	private class RecommendNumThread extends Thread{
		@Override
		public void run(){
			String info = HttpUpload.post(recommendNumURL, params);
			Bundle bundle = new Bundle();
			Message message = new Message();
			bundle.putString("info", info);
			message.setData(bundle);
			message.what = Constants.GOODSDETAILS_SUCCESS;
			recommendNumHandler.sendMessage(message);
		}
	}
	
	public void showImageview1Detail(View v) {
		showImageviewDetail("grade", "h1");
	}
	public void showImageview2Detail(View v) {
		showImageviewDetail("grade", "h2");
	}
	public void showImageview3Detail(View v) {
		showImageviewDetail("grade", "h3");
	}
	public void showImageview4Detail(View v) {
		showImageviewDetail("grade", "h4");
	}
	public void showImageview5Detail(View v) {
		showImageviewDetail("grade", "h5");
	}
	public void showImageview6Detail(View v) {
		showImageviewDetail("grade", "h6");
	}
	public void showImageview7Detail(View v) {
		showImageviewDetail("grade", "h7");
	}
	public void showImageview8Detail(View v) {
		showImageviewDetail("grade", "h8");
	}
	public void showImageview9Detail(View v) {
		showImageviewDetail("grade", "h9");
	}
	public void showImageview10Detail(View v) {
		showImageviewDetail("grade", "h10");
	}
	public void showImageview11Detail(View v) {
		showImageviewDetail("grade", "h11");
	}
	public void showImageview12Detail(View v) {
		showImageviewDetail("grade", "h12");
	}
	public void showImageview13Detail(View v) {
		showImageviewDetail("grade", "h13");
	}
	public void showImageview14Detail(View v) {
		showImageviewDetail("grade", "h14");
	}
	public void showImageview15Detail(View v) {
		showImageviewDetail("grade", "h15");
	}
	public void showImageview16Detail(View v) {
		showImageviewDetail("grade", "h16");
	}
	public void showImageview17Detail(View v) {
		showImageviewDetail("grade", "h17");
	}
	public void showImageview18Detail(View v) {
		showImageviewDetail("grade", "h18");
	}
	public void showImageview19Detail(View v) {
		showImageviewDetail("grade", "h19");
	}
	public void showImageview20Detail(View v) {
		showImageviewDetail("grade", "h20");
	}
	public void showImageview21Detail(View v) {
		showImageviewDetail("grade", "h21");
	}
	public void showImageview22Detail(View v) {
		showImageviewDetail("grade", "h22");
	}
	public void showImageview23Detail(View v) {
		showImageviewDetail("grade", "h23");
	}
	public void showImageview24Detail(View v) {
		showImageviewDetail("grade", "h24");
	}
	public void showImageview25Detail(View v) {
		showImageviewDetail("grade", "h25");
	}
	public void showImageview26Detail(View v) {
		showImageviewDetail("grade", "h26");
	}
	public void showImageview27Detail(View v) {
		showImageviewDetail("grade", "h27");
	}
	public void showImageview28Detail(View v) {
		showImageviewDetail("grade", "h28");
	}
	public void showImageview29Detail(View v) {
		showImageviewDetail("grade", "h29");
	}
	public void showImageview30Detail(View v) {
		showImageviewDetail("grade", "h30");
	}
	public void showImageview31Detail(View v) {
		showImageviewDetail("grade", "h31");
	}
	public void showImageview32Detail(View v) {
		showImageviewDetail("grade", "h32");
	}
	public void showImageview33Detail(View v) {
		showImageviewDetail("grade", "h33");
	}
	public void showImageview34Detail(View v) {
		showImageviewDetail("grade", "h34");
	}
	public void showImageview35Detail(View v) {
		showImageviewDetail("grade", "h35");
	}
	public void showImageview36Detail(View v) {
		showImageviewDetail("grade", "h36");
	}
	public void showImageview37Detail(View v) {
		showImageviewDetail("grade", "h37");
	}
	public void showImageview38Detail(View v) {
		showImageviewDetail("grade", "h38");
	}
	public void showImageview39Detail(View v) {
		showImageviewDetail("grade", "h39");
	}
	public void showImageview40Detail(View v) {
		showImageviewDetail("grade", "h40");
	}
	
	
	public void showImageviewDetail(String category, String flag){
		Intent intent= new Intent(GradeAreaActivity.this,GoodsInformationActivity.class);
		intent.putExtra("category", category);
		intent.putExtra("flag", flag);
		intent.putExtra("index", "grade");
		GradeAreaActivity.this.startActivity(intent);
		
	}

  	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
	
	@Override
    public void onDestroy()
    {
		tab2ActivityList.setAdapter(null);
        super.onDestroy();
    }
}
