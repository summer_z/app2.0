package com.qiangpro.tab1;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

import com.qiangpro.main.R;
import com.qiangpro.util.Constants;
import com.qiangpro.util.Constants.ConstantValues;
import com.qiangpro.util.HttpUpload;
import com.umeng.analytics.MobclickAgent;

public class SelectCityActivity extends Activity{
	private String url = "http://" + ConstantValues.IP + "/goqiang/android/getAllCity";
	private GridView gridView;
	private String city ;
	private String cityList ;
	private String cityListPyInfo ;
	private String cityListCnInfo ;
	private String[] cityListArgs ;
	private List<NameValuePair> params=new ArrayList<NameValuePair>();
	private Handler handler;
	private ArrayAdapter<Object> adapter ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_city_activity);
		handler = new Handler(){
			@Override
			public void handleMessage(Message msg){
				super.handleMessage(msg);
				Bundle bundle = new Bundle();
				bundle = msg.getData();
				String msgStr = bundle.getString("msg");
				switch(msg.what){
					case Constants.GETALLCITY_SUCCESS:	
						if(!msgStr.equals("")){
						cityList = city + msgStr ;
						GetAllCityList();
						}else{
							Toast.makeText(SelectCityActivity.this, "服务器异常，请稍候再试！", Toast.LENGTH_SHORT).show();
						}
					}
			}
		};
		GetAllCity getAllCity = new GetAllCity();
		getAllCity.start();
		Intent intent = getIntent();
        city = intent.getStringExtra("city");
        gridView = (GridView) findViewById(R.id.cityList);
		gridView.setOnItemClickListener(new GridView.OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				SelectCityActivity.this.getIntent().putExtra("city", 
						gridView.getItemAtPosition(position).toString()) ;// 返回信息
				SelectCityActivity.this.getIntent().putExtra("cityListPy", cityListPyInfo) ;// 返回信息
				SelectCityActivity.this.getIntent().putExtra("cityListCn", cityListCnInfo) ;// 返回信息
				SelectCityActivity.this.setResult(RESULT_OK, SelectCityActivity.this.getIntent()) ;
				SelectCityActivity.this.finish() ;
			}
			
		});
	}
	private void GetAllCityList() {
		cityListArgs = cityList.split("#");
		String[] cityListPy = new String[cityListArgs.length];
		String[] cityListCn = new String[cityListArgs.length];
		for(int i=0; i<cityListArgs.length; i++){
			cityListPy[i] = cityListArgs[i].split("\\|")[0];
			cityListCn[i] = cityListArgs[i].split("\\|")[1];
		}
		StringBuffer sbPy = new StringBuffer();
		for(int i = 0; i < cityListPy.length; i++){
			sbPy. append(cityListPy[i]+"#");
		}
		cityListPyInfo = sbPy.toString();
		StringBuffer sbCn = new StringBuffer();
		for(int i = 0; i < cityListCn.length; i++){
			sbCn. append(cityListCn[i]+"#");
		}
		cityListCnInfo = sbCn.toString();
		adapter = new ArrayAdapter<Object>(
	            SelectCityActivity.this, 
	            R.layout.city_gridview,cityListCn); 
		gridView.setAdapter(adapter);
	}
	
	private class GetAllCity extends Thread{
		@Override
		public void run(){
			String msgStr = HttpUpload.post(url, params);
			Bundle b = new Bundle();
			b.putString("msg", msgStr);
			Message msg = new Message();
			msg.setData(b);
			msg.what = Constants.GETALLCITY_SUCCESS;
			handler.sendMessage(msg);
		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
}
