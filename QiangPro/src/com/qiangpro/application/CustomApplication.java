package com.qiangpro.application;

import android.app.Application;

public class CustomApplication extends Application {

	private int listEndPosition;
	private static final int CONTENT = 5;
	private static final String INIT = "init";
	private String noticeData;

	@Override
	public void onCreate() {
		super.onCreate();
		setListEndPosition(CONTENT);
		setNoticeData(INIT);
	}
	
	public String getNoticeData() {
		return noticeData;
	}
	public void setNoticeData(String noticeData) {
		this.noticeData = noticeData;
	}
	
	public void setListEndPosition(int listEndPosition) {
		this.listEndPosition = listEndPosition;
	}

	public int getListEndPosition() {
		return listEndPosition;
	}

}
