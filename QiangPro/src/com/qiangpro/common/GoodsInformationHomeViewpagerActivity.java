package com.qiangpro.common;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qiangpro.main.R;
import com.qiangpro.tab3.LoginActivity;
import com.qiangpro.util.AsyncImageLoader;
import com.qiangpro.util.CallbackImpl;
import com.qiangpro.util.Constants;
import com.qiangpro.util.Constants.ConstantValues;
import com.qiangpro.util.DialogManager;
import com.qiangpro.util.HttpUpload;
import com.qiangpro.util.NetworkConnection;
import com.umeng.analytics.MobclickAgent;

public class GoodsInformationHomeViewpagerActivity extends Activity {

	private TextView namedByTab,countdown_textview;//countdown_textview为显示倒计时的组件
	private ViewPager pager;  // 左右滑动组件
	private LinearLayout points;  // 用于存放焦点图标的布局
	private List<View> list;  // 用于保存4张图片布局数据源 
    private ImageView[] views;  // 用于存放导航小圆点
    // 4个布局文件,整体管理,以后要动态加载
    private int[] layouts = {R.layout.tab2_viewpager1, R.layout.tab2_viewpager2,
    		R.layout.tab2_viewpager3,R.layout.tab2_viewpager4};
	private File tab2_viewpagerimage_cache ;//取得缓存图片
	private int flag = 1;
	private int index  = 0;
	private TextView goods_details_textview1, goods_details_textview2, 
		goods_details_textview3, goods_details_textview4;//商品信息：依次是商品名称，价格，数量，详情
	private EditText goods_details_edittext;//输入 的中奖号码
	private ImageView goods_details_imageview2;//提交号码的抢图片
	private List<NameValuePair> param = new ArrayList<NameValuePair>();
	private SharedPreferences sp;
	private String url = "http://" + ConstantValues.IP + "/goqiang/android/goodssubmit";
	private Handler submitHandler;//得到提交结果的handler
	private String nowDate;//提交的时间
	private ProgressDialog pDialog;
	private int currentIndex;
    private boolean isContinue = true;
    private Handler goodsInfoHandler;//得到商品信息
    private Handler mHandler;//改变viewpager(滑动)
    private Timer timer;
    private TimerTask myTimerTask;
    private static boolean isSleep = true;
    private static final int initPositon = 40;
    private static int currentPosition = 0;
    private boolean tag = true;
    private String category;//获得商品版块类别
    private String locationFlag;//获得版块中具体位置
    private String indexString;//判断从哪个activity过来的
    private int day;
    private int hour;
    private int min;
    private int s;
    private Timer countdown = new Timer();
    private String[] urlString = new String[4];
    private String baseUrl = "http://" + ConstantValues.IP + "/goqiang/resources/";
    private String goodsInfoUrl = "http://" + ConstantValues.IP + "/goqiang/android/getGoodsInfo";
    private String urlUserValue = "http://" + ConstantValues.IP + "/goqiang/android/getUserValue";
    private String urlReduceValue = "http://" + ConstantValues.IP + "/goqiang/android/reduceUserValue";
    private List<NameValuePair> paramList = new ArrayList<NameValuePair>();
    private AsyncImageLoader loader = new AsyncImageLoader(GoodsInformationHomeViewpagerActivity.this);
    private LinearLayout linearlayout_tab1_activity;
    
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
    	if(!tag){
    		tag = true;
        	timer = new Timer();
            myTimerTask = new MyTimerTask();
            timer.schedule(myTimerTask, 4000);
    	}
		super.onStart();
	}
	@Override
	public void onStop() {
		tag = false;
		timer.cancel();
		myTimerTask.cancel();
		super.onStop();
	}
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.goodsinfomation_homeviewpager_activity);
		Intent it = super.getIntent();
		category = it.getStringExtra("category");//得到商品类别
		locationFlag = it.getStringExtra("flag");//得到商品标记为，区别具体商品信息
		indexString = it.getStringExtra("index");
//		System.out.println("***** 得到的信息  ****" + category + locationFlag);
		for (int i = 0; i < layouts.length; i++) {
			StringBuffer sBuffer = new StringBuffer();
			sBuffer.append(baseUrl);
			sBuffer.append(category);
			sBuffer.append("//");
			sBuffer.append(locationFlag);
			sBuffer.append("_");
			sBuffer.append(i+1);
			sBuffer.append(".jpg");
			urlString[i] = sBuffer.toString();
		}
		
		init();
		mHandler = new MHandler();
		goodsInfoHandler = new GoodsInfoHandler();
		submitHandler = new SubmitHandler();
		paramList.clear();
		paramList.add(new BasicNameValuePair("category", category));
		paramList.add(new BasicNameValuePair("locationFlag", locationFlag));
		new GoodsDetailsThread().start();//获取商品详情的线程
        //viewpager循环倒计时
        timer = new Timer();
        myTimerTask = new MyTimerTask();
        timer.schedule(myTimerTask, 4000);
		//setListener();
        loadTab2ViewpagerImage();
        this.goods_details_imageview2.setOnClickListener(new OnClickListenerSubmit());
		setPoints();
		pager.setAdapter(new MyPagerAdapter());
		pager.setOnPageChangeListener(new MyPageChangeListener());
		pager.setCurrentItem(initPositon);
		pager.setOnTouchListener(new MyTouchListener());
	}
	
	private class GoodsDetailsThread extends Thread{
		@Override
		public void run(){
			String info = HttpUpload.post(goodsInfoUrl, paramList);
			
			Bundle bundle = new Bundle();
			Message message = new Message();
			bundle.putString("info", info);
			message.setData(bundle);
			message.what = Constants.GOODSDETAILS_SUCCESS;
			goodsInfoHandler.sendMessage(message);
		}
	}
	
	private class GoodsInfoHandler extends Handler{
		@Override
		public void handleMessage(Message msg){
			super.handleMessage(msg);
			Bundle bundle = new Bundle();
			bundle = msg.getData();
			String info = bundle.getString("info");
//			System.out.println("****** info *******" + info);
			switch(msg.what){
				case Constants.GOODSDETAILS_SUCCESS:{
					if(info.equals("服务器错误，请稍后重试")){
						Toast.makeText(GoodsInformationHomeViewpagerActivity.this, info, Toast.LENGTH_SHORT).show();
					}else{
						String[] userInfo = info.split("\\|");
						if(userInfo.length == 8){
							goods_details_textview1.setText(userInfo[0]);
							goods_details_textview2.setText(userInfo[1]);
							goods_details_textview3.setText(userInfo[2]);
							goods_details_textview4.setText(userInfo[3]);
							day = Integer.parseInt(userInfo[4]);
							hour = Integer.parseInt(userInfo[5]);
							min = Integer.parseInt(userInfo[6]);
							s = Integer.parseInt(userInfo[7]);
							String text="离结束还有："+Integer.toString(day)+"天"+Integer.toString(hour)+"时"+
			            			Integer.toString(min)+"分"+Integer.toString(s)+"秒";
			            	if(s==0&&min>0){
			            		s=60;
			            		min=min-1;
			            	}else if(s==0&&min==0&&hour>0){
			            		hour=hour-1;
			            		min=59;
			            		s=60;
			            	}else if(s==0&&min==0&&hour==0&&day>0){
			            		day=day-1;
			            		hour=59;
			            		min=59;
			            		s=60;
			            	}else if(s==0&&min==0&&hour==0&&day==0){
			            		text="抢购未开始";
			            		cancelCountdown();
			            	}
			            	countdown_textview.setText(text); 
			            	if ( !(s==0&&min==0&&hour==0&&day==0) ) {
			            		countdown.schedule(countdowntask, 0, 1000);       // timeTask 
							}
			            	
						}
					}
					break;
				}
			}
		}
	}
	
	private class MHandler extends Handler{
		@Override
		public void handleMessage(Message msg){
            pager.setCurrentItem(currentPosition);
        }
	}
	
	private class SubmitHandler extends Handler{
		@Override
		public void handleMessage(Message msg){
			super.handleMessage(msg);
			Bundle bundle = new Bundle();
			bundle = msg.getData();
			String info = bundle.getString("info");
			switch(msg.what){
				case Constants.GOODSDETAILS_SUCCESS:{
					if(info.equals("提交成功")){
						goods_details_edittext.setText("");
						pDialog.dismiss();
						DialogManager.doDialog(GoodsInformationHomeViewpagerActivity.this);
					}else{
						pDialog.dismiss();
						Toast.makeText(GoodsInformationHomeViewpagerActivity.this, info, Toast.LENGTH_LONG).show();
					}
					break;
				}
				case Constants.POST_SUCCESS:{
					pDialog.dismiss();
					if(info.equals("服务器错误，请稍后重试") || info.equals("用户名或密码错误")){
						Toast.makeText(GoodsInformationHomeViewpagerActivity.this, info, Toast.LENGTH_SHORT).show();
					}else{
						String[] userInfo = info.split("\\|");
						if(userInfo.length == 2){
							if (Integer.parseInt(userInfo[0]) >= 100) {
								param.clear();
								param.add(new BasicNameValuePair("username", sp.getString("username", "0")));
								param.add(new BasicNameValuePair("password", sp.getString("password", "0")));
								new ReduceThread().start();
								DialogManager.doDialogReduceGrade(GoodsInformationHomeViewpagerActivity.this);
							}else {
								Toast.makeText(GoodsInformationHomeViewpagerActivity.this, "您的余额不足1元", Toast.LENGTH_SHORT).show();
							}
						}else {
							Toast.makeText(GoodsInformationHomeViewpagerActivity.this, "服务器错误，请稍后重试", Toast.LENGTH_SHORT).show();
						}
						
					}
					break;
				}
					
			}
		}
	}
	
	private void init(){
		goods_details_textview1 = (TextView)findViewById(R.id.goods_details_textview1);
		goods_details_textview2 = (TextView)findViewById(R.id.goods_details_textview2);
		goods_details_textview3 = (TextView)findViewById(R.id.goods_details_textview3);
		goods_details_textview4 = (TextView)findViewById(R.id.goods_details_textview4);
		goods_details_edittext = (EditText)findViewById(R.id.goods_details_edittext);
		goods_details_imageview2 = (ImageView)findViewById(R.id.goods_details_imageview2);
		namedByTab = (TextView)this.findViewById(R.id.namedByTab);
		namedByTab.setText(getString(R.string.tab2_text));
		countdown_textview= (TextView)this.findViewById(R.id.countdown_textview);
		sp = this.getSharedPreferences(Constants.SAVE_NAME_PWD_FILENAME, Activity.MODE_PRIVATE);
		pager = (ViewPager)this.findViewById(R.id.viewpager_tab2_activity);
		linearlayout_tab1_activity = (LinearLayout)this.findViewById(R.id.linearlayout_tab1_activity);
//		if(indexString.equals("homeviewpager")){
//			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 600);
//			linearlayout_tab1_activity.setLayoutParams(params);
//		}
		points = (LinearLayout)this.findViewById(R.id.pointGroup);
		list = new ArrayList<View>();// 把4张图片对应的布局文件加载回来,放到List中
		//取得缓存的图片
		tab2_viewpagerimage_cache = new File(GoodsInformationHomeViewpagerActivity.this.getFilesDir(), category);
//		System.out.println(">>getFilesDir()>>>" + GoodsInformationActivity.this.getFilesDir());
	}

	private void loadTab2ViewpagerImage() {
		for(int i = 0; i < layouts.length; i++){
			FrameLayout layout = (FrameLayout)getLayoutInflater().inflate(layouts[i], null);
			ImageView image = (ImageView)layout.findViewById(R.id.tab2_viewpager1);
			CallbackImpl callbackImpl = new CallbackImpl(image);
	    	Bitmap cacheImage = loader.loadBitmap(urlString[i], callbackImpl, false, "no");
			if (cacheImage != null) {
				image.setImageBitmap(cacheImage);
			}
//			imageLoader.DisplayImage(urlString[i], image);
//			loadImage(urlString[i], R.id.tab2_viewpager1);
			
			list.add(layout);
		}
	}
	
	private void setPoints() {
		// 根据list的长度决定做多大的导航图片数组
		views = new ImageView[list.size()];
		for(int i=0;i<views.length;i++){
			ImageView iv = new ImageView(this);
			iv.setLayoutParams(new LayoutParams(20,20));
			iv.setPadding(20, 0, 20, 0);
			views[i] = iv;
			if(i==0){
				iv.setBackgroundResource(R.drawable.page_indicator_focused);
			}else{
				iv.setBackgroundResource(R.drawable.page_indicator);
			}
			points.addView(views[i]);	// 很重要
		}
	}
	
	private class MyPagerAdapter extends PagerAdapter{
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
//				return list.size();
			return Integer.MAX_VALUE;//用于无线循环滑动
		}
		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == arg1;
		}
		@Override
		public Object instantiateItem(View container, int position) {
		
			position = position % list.size();  //循环的关键！求余数！(本来不用求余数)
			( (ViewPager)container ).addView(list.get(position));
			return list.get(position);
		}
		@Override
		public void destroyItem(View container, int position, Object object) {
			( (ViewPager)container ).removeView(list.get(position % list.size()));
		}
	}
	
	private class MyPageChangeListener implements OnPageChangeListener{
		@Override
		public void onPageSelected(int arg0) {
			currentPosition = arg0;//必须放在最前面
			arg0 = arg0 % list.size();
			for(int i=0;i<views.length;i++){
				if(i==arg0){
					views[i].setBackgroundResource(R.drawable.page_indicator_focused);
				}else{
					views[i].setBackgroundResource(R.drawable.page_indicator);
				}
			}
		}
		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub
			
		}
	}
	
	private class MyTouchListener implements OnTouchListener{
        @Override
        public boolean onTouch(View v, MotionEvent event){
            switch (event.getAction()){
	            case MotionEvent.ACTION_DOWN:
	            case MotionEvent.ACTION_MOVE:
	                isContinue = false;                
	                break;
	            case MotionEvent.ACTION_UP:
	            default:
	                isContinue = true;
	                isSleep = true;
                break;
            }
            return false;
        }
	}
	
	private void sleep(long time){
        try{
           Thread.sleep(time); 
        } catch (Exception e){
            e.printStackTrace();
        }
	}
	
	private class MyTimerTask extends TimerTask{
		public void run(){
            while (tag){
                if (isContinue){
                    currentPosition ++;
                    mHandler.sendEmptyMessage(0);
//                    System.out.println(">>>>>> currentPosition " + currentPosition);
                    sleep(3000);
                }
            }
        }
	}
	
	final Handler handler = new Handler(){  
		@Override  
        public void handleMessage(Message msg){  
            switch (msg.what) {  
            case 1:  
            	String text="离结束还有："+Integer.toString(day)+"天"+Integer.toString(hour)+"时"+
            			Integer.toString(min)+"分"+Integer.toString(s)+"秒";
            	if(s==0&&min>0){
            		s=60;
            		min=min-1;
            	}else if(s==0&&min==0&&hour>0){
            		hour=hour-1;
            		min=59;
            		s=60;
            	}else if(s==0&&min==0&&hour==0&&day>0){
            		day=day-1;
            		hour=59;
            		min=59;
            		s=60;
            	}else if(s==0&&min==0&&hour==0&&day==0){
            		text="抢购未开始";
            		cancelCountdown();
            	}
            	countdown_textview.setText(text);  
            }  
        }  
	};  
	  
    TimerTask countdowntask = new TimerTask() {  //抢购倒计时的TimerTask
        @Override  
        public void run() {  
            s--;  
            Message message = new Message();  
            message.what = 1;  
            handler.sendMessage(message);  
        }  
    };  
	
	public void cancelCountdown(){
		countdown.cancel();  
		countdowntask.cancel();
        countdown_textview.setVisibility(View.VISIBLE);  
	}
	
	public void qiang(View v){
		//TODO Connect net work 还有0天0时0分-1秒结束
		if("抢购未开始".equals(countdown_textview.getText())){
			Toast.makeText(GoodsInformationHomeViewpagerActivity.this, "亲，时间已到，换个商品抢吧……", Toast.LENGTH_LONG).show();
		}else{
			Toast.makeText(GoodsInformationHomeViewpagerActivity.this, "已提交", Toast.LENGTH_SHORT).show();
		}
	}
	
	private class OnClickListenerSubmit implements OnClickListener{
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			if("抢购未开始".equals(countdown_textview.getText())){
				Toast.makeText(GoodsInformationHomeViewpagerActivity.this, "亲，时间已到，换个商品抢吧……", Toast.LENGTH_LONG).show();
			}else{
				java.util.Date date = new java.util.Date();
	//			java.util.Date yesDate = new Date(date.getTime()- 1 * 24 * 60 * 60 * 1000);//昨天的日期
				String pat = "yyyy-MM-dd";
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pat);
				nowDate = simpleDateFormat.format(date);
				boolean isCon = NetworkConnection.isNetworkConnected(GoodsInformationHomeViewpagerActivity.this) ;
				boolean login_success = sp.getBoolean("login_success", false);
				if(isCon){
					if(login_success){
						String editText = goods_details_edittext.getText().toString();
						if(editText.length() != 3){
							Toast.makeText(GoodsInformationHomeViewpagerActivity.this, "亲，只能是3位数字哦……", Toast.LENGTH_LONG).show();
						}else{
							pDialog = ProgressDialog.show(GoodsInformationHomeViewpagerActivity.this, "ToFree", "请稍等……");
							if (indexString.equals("free")) {
								String username = sp.getString("username", "客户端用户名错误");
								initParam(editText, username);
								new FreeGoodsThread().start();//交给服务器处理
							}else if(indexString.equals("grade") | indexString.equals("homeviewpager")){
//									Toast.makeText(GoodsInformationActivity.this, "这是积分专区的", Toast.LENGTH_LONG).show() ;
								param.clear();
								param.add(new BasicNameValuePair("username", sp.getString("username", "0")));
								param.add(new BasicNameValuePair("password", sp.getString("password", "0")));
								new UserValueGradeThread().start();
							}
						}
						
					}else{
						Toast.makeText(GoodsInformationHomeViewpagerActivity.this, "请先登录", Toast.LENGTH_LONG).show();
						Intent it = new Intent(GoodsInformationHomeViewpagerActivity.this, LoginActivity.class);
						GoodsInformationHomeViewpagerActivity.this.startActivity(it);
					}
				}else{
					Toast.makeText(GoodsInformationHomeViewpagerActivity.this, R.string.unconnected, Toast.LENGTH_LONG).show() ;
				}
				
			}
		}

		private void initParam(String editText, String username) {
			param.clear();
			param.add(new BasicNameValuePair("goods_name", goods_details_textview1.getText().toString()));
			param.add(new BasicNameValuePair("goods_value", goods_details_textview2.getText().toString()));
			param.add(new BasicNameValuePair("goods_num", goods_details_textview3.getText().toString()));
			param.add(new BasicNameValuePair("goods_date_final", countdown_textview.getText().toString()));
			param.add(new BasicNameValuePair("goods_date_submit", nowDate));
			param.add(new BasicNameValuePair("goods_username", username));
			param.add(new BasicNameValuePair("goods_figure", editText));
		}
    	
    }
	private class FreeGoodsThread extends Thread{
		@Override
		public void run(){
			String info = HttpUpload.post(url, param);
			Bundle bundle = new Bundle();
			bundle.putString("info", info);
			Message message = new Message();
			message.setData(bundle);
			message.what = Constants.GOODSDETAILS_SUCCESS;
			submitHandler.sendMessage(message);
		}
	}
	
	private class UserValueGradeThread extends Thread{
		@Override
		public void run(){
			String info = HttpUpload.post(urlUserValue, param);
			Bundle bundle = new Bundle();
			bundle.putString("info", info);
			Message message = new Message();
			message.setData(bundle);
			message.what = Constants.POST_SUCCESS;
			submitHandler.sendMessage(message);
		}
	}
	
	private class ReduceThread extends Thread{
		@Override
		public void run(){
			HttpUpload.postWithoutReturn(urlReduceValue, param);
//			Bundle bundle = new Bundle();
//			bundle.putString("info", info);
//			Message message = new Message();
//			message.setData(bundle);
//			message.what = Constants.POST_SUCCESS;
//			submitHandler.sendMessage(message);
		}
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
	
	@Override
    public void onDestroy(){
//		imageLoader.clearCache();
        super.onDestroy();
    }
}
