package com.qiangpro.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.qiangpro.main.R;
import com.qiangpro.tab3.LoginActivity;
import com.qiangpro.util.Constants;
import com.qiangpro.util.Constants.ConstantValues;
import com.qiangpro.util.HttpUpload;
import com.umeng.analytics.MobclickAgent;

public class ModifyUserInfoActivity extends Activity {
	private String url = "http://" + ConstantValues.IP + "/goqiang/android/updateuserinfo";
	private EditText modify_edittext_ago, modify_edittext;
	private ImageView modify_button;
	private String index, username, password, tel, address;
	private List<NameValuePair> params;
	private Handler handler;
	private SharedPreferences sp;
	private SharedPreferences.Editor edit;
	private String editText_ago, editText;
	private ProgressDialog pDialog;
	private TextView namedByTab;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.setContentView(R.layout.modify_userinfo_activity);
		this.handler  = new Handler(){
			@Override
			public void handleMessage(Message msg){
				super.handleMessage(msg);
				Bundle bundle = new Bundle();
				bundle = msg.getData();
				String msgStr = bundle.getString("msg");
				switch(msg.what){
				case Constants.MODIFY_SUCCESS:
					pDialog.dismiss();
					if(msgStr.equals("修改成功")){
						if(index.equals("1")){
							edit.putString("username", editText);
						}else if(index.equals("2")){
							edit.putString("password", editText);
						}else if(index.equals("3")){
							edit.putString("tel", editText);
						}else if(index.equals("4")){
							edit.putString("address", editText);
						}
						edit.commit();
						ModifyUserInfoActivity.this.finish();
					}
					
					Toast.makeText(ModifyUserInfoActivity.this, msgStr, Toast.LENGTH_SHORT).show();
				}
				
			}
		};
		
		this.modify_edittext_ago = (EditText)this.findViewById(R.id.modify_edittext_ago);
		this.modify_edittext = (EditText)this.findViewById(R.id.modify_edittext);
		this.modify_button = (ImageView)this.findViewById(R.id.modify_button);
		this.namedByTab = (TextView)this.findViewById(R.id.namedByTab);
		namedByTab.setText("修改");
		
		Intent it = super.getIntent();
		index = it.getStringExtra("index");
		
		sp = this.getSharedPreferences(Constants.SAVE_NAME_PWD_FILENAME, Activity.MODE_PRIVATE);
		edit = sp.edit();
		
		username = sp.getString("username", "1");
		password = sp.getString("password", "1");
		tel = sp.getString("tel", "12345678900");
		address = sp.getString("address", "1");
		
		if(index.equals("1")){
			modify_edittext_ago.setText(username);
			modify_edittext_ago.setEnabled(false);
		}else if(index.equals("2")){
			
		}else if(index.equals("3")){
			modify_edittext_ago.setText(tel);
			modify_edittext_ago.setEnabled(false);
		}else if(index.equals("4")){
			modify_edittext_ago.setText(address);
			modify_edittext_ago.setEnabled(false);
		}
		
		params = new ArrayList<NameValuePair>();
		
		this.modify_button.setOnClickListener(new OnClickListenerButton());
		
	}
	private class OnClickListenerButton implements OnClickListener{

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			editText_ago = modify_edittext_ago.getText().toString().trim();
			editText = modify_edittext.getText().toString().trim();
			if (editText.equals("")) {
				Toast.makeText(ModifyUserInfoActivity.this, "请正确填写内容", Toast.LENGTH_LONG).show();
			}else {
				if(index.equals("2")){
					if(editText_ago.equals(password)){
						initParam();
					}else{
						Toast.makeText(ModifyUserInfoActivity.this, "原始密码不正确", Toast.LENGTH_LONG).show();
					}
				}else{
					initParam();
				}
			}
			
		}

		private void initParam() {
			pDialog = ProgressDialog.show(ModifyUserInfoActivity.this, "ToFree", "请稍等……");
			params.clear();
			params.add(new BasicNameValuePair("index", index));
			params.add(new BasicNameValuePair("username", username));
			params.add(new BasicNameValuePair("tel", tel));
			params.add(new BasicNameValuePair("edit", editText));
			
			new Thread(
				new Runnable(){
					@Override
					public void run() {
						// TODO Auto-generated method stub
						String msgStr = HttpUpload.post(url, params);
						Bundle b = new Bundle();
						b.putString("msg", msgStr);
						Message msg = new Message();
						msg.setData(b);
						msg.what = Constants.MODIFY_SUCCESS;
						handler.sendMessage(msg);
						
					}
				}
			).start();
		}
    	
    }
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}

}
