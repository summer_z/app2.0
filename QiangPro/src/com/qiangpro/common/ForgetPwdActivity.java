package com.qiangpro.common;

import java.util.ArrayList;

import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.InjectView;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.inject.Inject;
import com.qiangpro.main.R;
import com.qiangpro.tab3.LoginActivity;
import com.qiangpro.util.Constants.ConstantValues;
import com.qiangpro.util.Constants;
import com.qiangpro.util.DialogManager;
import com.qiangpro.util.HttpUpload;
import com.qiangpro.util.NetworkConnection;
import com.qiangpro.validator.RegularValidator;
import static com.qiangpro.util.ViewTextUtil.viewToString;

public class ForgetPwdActivity extends RoboFragmentActivity {
	private ImageView forget_pwd_submit;
	private String  url =  "http://" + ConstantValues.IP + "/goqiang/android/usersforget";
	private String info;//接受服务器返回的信息
	private List<NameValuePair> params;
	private static int MIN_LENGTH = 1;
	private static int MAX_LENGTH = 16;
	private ProgressDialog pDialog;
	private ForgetHandler forgetHandler;
	@Inject
	private RegularValidator regularValidator;
	
	@InjectView(R.id.forget_username)
	private EditText forget_username;
	@InjectView(R.id.forget_tel)
	private EditText forget_tel;
	@InjectView(R.id.namedByTab)
	private TextView namedByTab; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.setContentView(R.layout.forget_pwd_activity);
		this.forget_pwd_submit = (ImageView)this.findViewById(R.id.forget_pwd_submit);
		this.forget_pwd_submit.setOnClickListener(new OnClickListenerSubmit());
		namedByTab.setText("找回密码");
		
		forgetHandler = new ForgetHandler();
	}

	
	private class OnClickListenerSubmit implements OnClickListener{

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			//String name = forget_username.getText().toString().trim();
			//String tel = forget_tel.getText().toString().trim();
			if(NetworkConnection.isNetworkConnected(ForgetPwdActivity.this)){
				pDialog = ProgressDialog.show(ForgetPwdActivity.this, "ToFree", "请稍等……");
				pDialog.setCancelable(true);
				doForget(viewToString(forget_username), viewToString(forget_tel));
				
			}else{
				Toast.makeText(ForgetPwdActivity.this, "请检查网络设置", Toast.LENGTH_SHORT).show();
			}
		}
    }
	
	private void doForget(String userName, String tel) {
		try {
			params = new ArrayList<NameValuePair>();
			params.clear();
			params.add(new BasicNameValuePair("name", viewToString(forget_username)));
			params.add(new BasicNameValuePair("tel", viewToString(forget_tel)));
			new ForgetThread().start();
		} catch (Exception e) {
			Toast.makeText(ForgetPwdActivity.this, e.getMessage(), Toast.LENGTH_SHORT)
					.show();
		}
	}
	
	private class ForgetHandler extends Handler{
		@Override
		public void handleMessage(Message msg){
			super.handleMessage(msg);
			Bundle bundle = new Bundle();
			bundle = msg.getData();
			String msgString = bundle.getString("msg"); 
			pDialog.dismiss();
			switch (msg.what) {
			case Constants.LOGIN_SUCCESS:
				if (msgString.equals("验证成功")) {
					forget_username.setText("");
					forget_tel.setText("");
					DialogManager.doDialogForget(ForgetPwdActivity.this);
				}else {
					Toast.makeText(ForgetPwdActivity.this, "验证失败", Toast.LENGTH_LONG).show();
				}
				break;

			default:
				break;
			}
		}
	}
	
	private class ForgetThread extends Thread{
		@Override
		public void run(){
			String infoString = HttpUpload.post(url, params);
			Bundle bundle = new Bundle();
			Message message = new Message();
			bundle.putString("msg", infoString);
			message.setData(bundle);
			message.what = Constants.LOGIN_SUCCESS;
			forgetHandler.sendMessage(message);
		}
	}
	
}
