package com.qiangpro.validator;

import android.content.Context;


import com.google.inject.Inject;
import com.qiangpro.main.R;

import java.security.InvalidParameterException;
import java.util.Locale;

public class StringValidator {

	private String illegalChar = "^[\u4e00-\u9fa5\\w]+";
	private String blankChar = "^\\S*$";
	private String IllegalCharWithoutCommaAndPercent = "[%.\u4e00-\u9fa5a-zA-Z0-9_]*";
	private String NotChineseChar = "^[a-zA-Z0-9_]+" ;
    private Context context;

    @Inject
    public StringValidator(Context context) {
        this.context = context;
    }

	public void validateLength(int minLength, int maxLength, String input)
			throws InvalidParameterException {
		if (minLength < 0 || maxLength <= 0 || maxLength < minLength) {
			throw new InvalidParameterException(context.getString(R.string.error_input_length));
		}

		if (input == null || input.length() < minLength
				|| input.length() > maxLength) {
			throw new InvalidParameterException(context.getString(R.string.error_input_length));
		}
	}
	
	public void validateRgisterUsernameLength(int minLength, int maxLength, String input)
			throws InvalidParameterException {
		if (minLength < 0 || maxLength <= 0 || maxLength < minLength) {
			throw new InvalidParameterException("用户名长度为1-16位字符");
		}

		if (input == null || input.length() < minLength
				|| input.length() > maxLength) {
			throw new InvalidParameterException("用户名长度为1-16位字符");
		}
	}
	
	public void validateRgisterPwdLength(int minLength, int maxLength, String input)
			throws InvalidParameterException {
		if (minLength < 0 || maxLength <= 0 || maxLength < minLength) {
			throw new InvalidParameterException("密码为6-20位字符");
		}

		if (input == null || input.length() < minLength
				|| input.length() > maxLength) {
			throw new InvalidParameterException("密码长度为6-20位字符");
		}
	}
	
	
	public void validateOrignUsernameLength(int minLength, int maxLength, String input)
			throws InvalidParameterException {
		if (minLength < 0 || maxLength <= 0 || maxLength < minLength) {
			throw new InvalidParameterException("请输入原始的正确用户名");
		}

		if (input == null || input.length() < minLength
				|| input.length() > maxLength) {
			throw new InvalidParameterException("请输入原始的正确用户名");
		}
	}
	
	public void validateIllegalChar(String input)
			throws InvalidParameterException {
		if (!input.matches(illegalChar))
			throw new InvalidParameterException(context.getString(R.string.error_illegal_char));
	}

	public void validateIllegalCharWithoutCommaAndPercent(String input)
			throws InvalidParameterException {
		if (input == null || !input.matches(IllegalCharWithoutCommaAndPercent))
			throw new InvalidParameterException(context.getString(R.string.error_illegal_char));
	}

	public void validateBlank(String input) throws InvalidParameterException {
		if (input == null || !input.matches(blankChar))
			throw new InvalidParameterException(context.getString(R.string.error_input_include_blank));
	}
	
	public void validateEqual(String input1,String input2) throws InvalidParameterException {
		if (input1 == null ||input2==null){
			throw new InvalidParameterException("确认密码不能为空!");
			}
		if (!input1.equals(input2) ){
			throw new InvalidParameterException("两次密码输入不一致!");
		}
	}
	
	
	public void validateChinese(String input) throws InvalidParameterException {
		if (input == null) {
			throw new InvalidParameterException(context.getString(R.string.error_blank_input));
		}
		if (!input.matches(NotChineseChar))
			throw new InvalidParameterException(context.getString(R.string.error_input_include_chinese));
	}

	public void validatePhoneNumber(String input) throws InvalidParameterException {
		if (input == null) {
			throw new InvalidParameterException(context.getString(R.string.error_blank_input));
		}
		if (!input.matches("^(13\\d{9})$||^(15[0,1,2,3,4,5,6,7,8,9]\\d{8})$||^(18[0,1,2,3,5,6,7,8,9]\\d{8})$"))
			throw new InvalidParameterException("请输入正确的手机号");
	}
	
	
	public void validateSQLInject(String input)
			throws InvalidParameterException {
		if (input == null) {
			throw new InvalidParameterException(context.getString(R.string.error_blank_input));
		}
		String inputTransformation = input.toLowerCase(Locale.ENGLISH);
		String SQLInjectChar = "use<>create<>show<>desc<>alter<>drop<>'<>and<>exec<>insert<>select<>delete<>update<>count<>*<>%<>chr<>mid<>master<>truncate<>char<>declare<>;<>or<>+<>,";
		String[] SQLInjectChars = SQLInjectChar.split("<>");
		for (int i = 0; i < SQLInjectChars.length; i++) {
			if (inputTransformation.indexOf(SQLInjectChars[i]) >= 0) {
				throw new InvalidParameterException(context.getString(R.string.error_illegal_input));
			}
		}

	}
}
