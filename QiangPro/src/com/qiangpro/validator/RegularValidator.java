package com.qiangpro.validator;

import java.security.InvalidParameterException;


import com.google.inject.Inject;

public class RegularValidator {

	@Inject
	private StringValidator stringValidator;

	private String input;

	public RegularValidator with(String input) {
		this.input = input;
		return this;
	}

	public RegularValidator checkLength(int min, int max)
			throws InvalidParameterException {
		stringValidator.validateLength(min, max, input);
		return this;
	}
	
	public RegularValidator checkRgisterUsernameLength(int min, int max)
			throws InvalidParameterException {
		stringValidator.validateRgisterUsernameLength(min, max, input);
		return this;
	}
	
	public RegularValidator checkRgisterPwdLength(int min, int max)
			throws InvalidParameterException {
		stringValidator.validateRgisterPwdLength(min, max, input);
		return this;
	}
	
	
	
	public RegularValidator checkOrignUsernameLength(int min, int max)
			throws InvalidParameterException {
		stringValidator.validateOrignUsernameLength(min, max, input);
		return this;
	}
	
	public RegularValidator checkIllegalChar() {
		stringValidator.validateIllegalChar(input);
		return this;
	}

	public RegularValidator checkBlankChar() {
		stringValidator.validateBlank(input);
		return this;
	}

	public RegularValidator checkSQLInject() {
		stringValidator.validateSQLInject(input);
		return this;
	}

	public RegularValidator checkIllegalCharWithoutCommaAndPercent() {
		stringValidator.validateIllegalCharWithoutCommaAndPercent(input);
		return this;
	}

	public RegularValidator checkCheseInput() {
		stringValidator.validateChinese(input);
		return this;
	}

	public RegularValidator checkPhoneNumber() {
		stringValidator.validatePhoneNumber(input);
		return this;
	}

	public RegularValidator checkEqual(String input1,String input2) {
		stringValidator.validateEqual(input1,input2);
		return this; 
	}
	public void setStringValidator(StringValidator stringValidator) {
		this.stringValidator = stringValidator;
	}
}
