package com.qiangpro.parse;
 
import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.qiangpro.util.Constants.ConstantValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class RemoteImageLoader {
	private Context context ;
	private String url = "http://" + ConstantValues.IP + "/goqiang/resources";
	
	public RemoteImageLoader(Context con){
		context = con;
	}       
	//保存首页上方的滑动图片
//	public void saveHomeViewpagerImage() {
//		File cacheDir = new File(context.getFilesDir(), "home_viewpagerimage_cache");
//		
//		if(!cacheDir.exists()){
//			cacheDir.mkdirs();
//		}
//		for(int i=0;i<5;i++){
//			Bitmap bitmap = getHttpBitmap(this.url + "/homeviewpager/homeviewpager" + (i+1) + ".jpg");
//			if(bitmap != null){
//				File cacheFile = new File(cacheDir, "home_viewpagerimage_item" + (i+1) + ".jpg");
//				try {
//					cacheFile.createNewFile();
//					OutputStream output = new FileOutputStream(cacheFile);
//					bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
//					output.flush();
//					output.close();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}
//	}
	//保存首页下方的方块图片
//	public void saveHomeBlockImage() {
//		File cacheDir = new File(context.getFilesDir(), "home_blockimage_cache");
//		if(!cacheDir.exists()){
//			cacheDir.mkdirs();
//		}
//		for(int i=0;i<8;i++){
//			Bitmap bitmap = getHttpBitmap(this.url + "/homeblock/homeblock" + (i+1) + ".jpg");
//			if(bitmap != null){
//				File cacheFile = new File(cacheDir, "home_blockimage_item" + (i+1) + ".jpg");
//				try {
//					cacheFile.createNewFile();
//					OutputStream output = new FileOutputStream(cacheFile);
//					bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
//					output.flush();
//					output.close();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}
//	}
	//保存分类下方的长方形图片
//	public void saveCategoryBlockImage() {
//		File cacheDir = new File(context.getFilesDir(), "category_blockimage_cache");
//		if(!cacheDir.exists()){
//			cacheDir.mkdirs();
//		}
//		for(int i=0;i<8;i++){
//			Bitmap bitmap = getHttpBitmap(this.url + "/categoryblock/categoryblock" + (i+1) + ".jpg");
//			if(bitmap != null){
//				File cacheFile = new File(cacheDir, "category_blockimage_item" + (i+1) + ".jpg");
//				try {
//					cacheFile.createNewFile();
//					OutputStream output = new FileOutputStream(cacheFile);
//					bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
//					output.flush();
//					output.close();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}
//	}
	
	//保存分类下方的长方形图片
//	public void saveGoodsInfoViewpagerImage(String Url) {
//		File cacheDir = new File(context.getFilesDir(), Url);
//		if(!cacheDir.exists()){
//			cacheDir.mkdirs();
//		}
//		for(int i=0;i<4;i++){
//			Bitmap bitmap = getHttpBitmap(Url);
//			if(bitmap != null){
//				File cacheFile = new File(cacheDir, "goodsInfoViewpager_cache" + (i+1) + ".jpg");
//				try {
//					cacheFile.createNewFile();
//					OutputStream output = new FileOutputStream(cacheFile);
//					bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
//					output.flush();
//					output.close();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}
//	}
	
//	public void saveCampusBlockImage() {
//		File cacheDir = new File(context.getFilesDir(), "campus_blockimage_cache");
//		if(!cacheDir.exists()){
//			cacheDir.mkdirs();
//		}
//		for(int i=0;i<8;i++){
//			Bitmap bitmap = getHttpBitmap(this.url + "/campusblock/campusblock" + (i+1) + ".jpg");
//			if(bitmap != null){
//				File cacheFile = new File(cacheDir, "campus_blockimage_item" + (i+1) + ".jpg");
//				try {
//					cacheFile.createNewFile();
//					OutputStream output = new FileOutputStream(cacheFile);
//					bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
//					output.flush();
//					output.close();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}
//	}
	
	public void saveRecommendImage() {
		File cacheDir = new File(context.getFilesDir(), "cache");
		if(!cacheDir.exists()){
			cacheDir.mkdirs();
		}
		for(int i=0;i<8;i++){
			Bitmap bitmap = getHttpBitmap(this.url + "/recommend/h" + (i+1) + "_1.jpg");
			if(bitmap != null){
				File cacheFile = new File(cacheDir, "recommend" + (i+1) + ".jpg");
				try {
					cacheFile.createNewFile();
					OutputStream output = new FileOutputStream(cacheFile);
					bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
					output.flush();
					output.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public void saveFreeImage() {
		File cacheDir = new File(context.getFilesDir(), "cache");
		if(!cacheDir.exists()){
			cacheDir.mkdirs();
		}
		for(int i=0;i<8;i++){
			Bitmap bitmap = getHttpBitmap(this.url + "/free/h" + (i+1) + "_1.jpg");
			if(bitmap != null){
				File cacheFile = new File(cacheDir, "free" + (i+1) + ".jpg");
				try {
					cacheFile.createNewFile();
					OutputStream output = new FileOutputStream(cacheFile);
					bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
					output.flush();
					output.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public void saveGradeImage() {
		File cacheDir = new File(context.getFilesDir(), "cache");
		if(!cacheDir.exists()){
			cacheDir.mkdirs();
		}
		for(int i=0;i<8;i++){
			Bitmap bitmap = getHttpBitmap(this.url + "/grade/h" + (i+1) + "_1.jpg");
			if(bitmap != null){
				File cacheFile = new File(cacheDir, "grade" + (i+1) + ".jpg");
				try {
					cacheFile.createNewFile();
					OutputStream output = new FileOutputStream(cacheFile);
					bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
					output.flush();
					output.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public void saveStartActivityImage() {
		File cacheDir = new File(context.getFilesDir(), "cache");
		if(!cacheDir.exists()){
			cacheDir.mkdirs();
		}
		Bitmap bitmap = getHttpBitmap(this.url + "/start/h1.jpg");
		if(bitmap != null){
			File cacheFile = new File(cacheDir, "start.jpg");
			try {
				cacheFile.createNewFile();
				OutputStream output = new FileOutputStream(cacheFile);
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
				output.flush();
				output.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public Bitmap getHttpBitmap(String url) {
		URL myFileURL;
        Bitmap bitmap = null;
		try {
			myFileURL = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) myFileURL.openConnection();
			conn.setConnectTimeout(5000);
			conn.setReadTimeout(5000) ;
			conn.setDoInput(true);
			conn.connect();
			InputStream is = conn.getInputStream();
			bitmap = BitmapFactory.decodeStream(is);
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bitmap;
	}
}
