package com.qiangpro.parse;

import java.io.InputStream;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ParseRemoteXml {   
	public static HashMap<String, String> parseVersionXml(InputStream inStream) throws Exception { 
        HashMap<String, String> hashMap = new HashMap<String, String>(); 
        // 实例化一个文档构建器工厂 
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
        // 通过文档构建器工厂获取一个文档构建器 
        DocumentBuilder builder = factory.newDocumentBuilder(); 
        // 通过文档通过文档构建器构建一个文档实例 
        Document document = builder.parse(inStream); 
        //获取XML文件根节点 
        Element root = document.getDocumentElement(); 
        //获得所有子节点 
        NodeList childNodes = root.getChildNodes(); 
        
        for (int j = 0; j < childNodes.getLength(); j++) 
        { 
            //遍历子节点 
            Node childNode = (Node) childNodes.item(j); 
            if (childNode.getNodeType() == Node.ELEMENT_NODE) 
            { 
                Element childElement = (Element) childNode;  
                if ("version".equals(childElement.getNodeName())) {  
                    hashMap.put("version",childElement.getFirstChild().getNodeValue()); 
                }else if (("name".equals(childElement.getNodeName()))) { 
                    hashMap.put("name",childElement.getFirstChild().getNodeValue()); 
                }else if (("url".equals(childElement.getNodeName()))) { 
                    hashMap.put("url",childElement.getFirstChild().getNodeValue()); 
                } 
            } 
        } 
        return hashMap; 
    }
	
	public static HashMap<String, String> parseGoodsXml(InputStream inStream) throws Exception { 
        HashMap<String, String> hashMap = new HashMap<String, String>(); 
        // 实例化一个文档构建器工厂 
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
        // 通过文档构建器工厂获取一个文档构建器 
        DocumentBuilder builder = factory.newDocumentBuilder(); 
        // 通过文档通过文档构建器构建一个文档实例 
        Document document = builder.parse(inStream); 
        //获取XML文件根节点 
        Element root = document.getDocumentElement(); 
        //获得所有子节点 
        NodeList childNodes = root.getChildNodes(); 
        
        for (int j = 0; j < childNodes.getLength(); j++) 
        { 
            //遍历子节点 
            Node childNode = (Node) childNodes.item(j); 
            if (childNode.getNodeType() == Node.ELEMENT_NODE) 
            { 
                Element childElement = (Element) childNode; 
                if ("title1".equals(childElement.getNodeName())){  
                    hashMap.put("title1",childElement.getFirstChild().getNodeValue()); 
                } 
                else if (("title2".equals(childElement.getNodeName()))) { 
                    hashMap.put("title2",childElement.getFirstChild().getNodeValue()); 
                } 
                else if (("title3".equals(childElement.getNodeName()))) { 
                    hashMap.put("title3",childElement.getFirstChild().getNodeValue()); 
                } 
                else if (("title4".equals(childElement.getNodeName()))) { 
                    hashMap.put("title4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("title5".equals(childElement.getNodeName()))) { 
                    hashMap.put("title5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("title6".equals(childElement.getNodeName()))) { 
                    hashMap.put("title6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("title7".equals(childElement.getNodeName()))) { 
                    hashMap.put("title7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("title8".equals(childElement.getNodeName()))) { 
                    hashMap.put("title8",childElement.getFirstChild().getNodeValue()); 
                }
                
                else if (("price1".equals(childElement.getNodeName()))) { 
                    hashMap.put("price1",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("price2".equals(childElement.getNodeName()))) { 
                    hashMap.put("price2",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("price3".equals(childElement.getNodeName()))) { 
                    hashMap.put("price3",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("price4".equals(childElement.getNodeName()))) { 
                	hashMap.put("price4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("price5".equals(childElement.getNodeName()))) { 
                	hashMap.put("price5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("price6".equals(childElement.getNodeName()))) { 
                	hashMap.put("price6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("price7".equals(childElement.getNodeName()))) { 
                	hashMap.put("price7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("price8".equals(childElement.getNodeName()))) { 
                	hashMap.put("price8",childElement.getFirstChild().getNodeValue()); 
                }
                
                else if (("num1".equals(childElement.getNodeName()))) { 
                    hashMap.put("num1",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("num2".equals(childElement.getNodeName()))) { 
                    hashMap.put("num2",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("num3".equals(childElement.getNodeName()))) { 
                    hashMap.put("num3",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("num4".equals(childElement.getNodeName()))) { 
                	hashMap.put("num4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("num5".equals(childElement.getNodeName()))) { 
                	hashMap.put("num5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("num6".equals(childElement.getNodeName()))) { 
                	hashMap.put("num6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("num7".equals(childElement.getNodeName()))) { 
                	hashMap.put("num7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("num8".equals(childElement.getNodeName()))) { 
                	hashMap.put("num8",childElement.getFirstChild().getNodeValue()); 
                }
                
                else if (("person1".equals(childElement.getNodeName()))) { 
                    hashMap.put("person1",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("person2".equals(childElement.getNodeName()))) { 
                    hashMap.put("person2",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("person3".equals(childElement.getNodeName()))) { 
                    hashMap.put("person3",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("person4".equals(childElement.getNodeName()))) { 
                	hashMap.put("person4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("person5".equals(childElement.getNodeName()))) { 
                	hashMap.put("person5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("person6".equals(childElement.getNodeName()))) { 
                	hashMap.put("person6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("person7".equals(childElement.getNodeName()))) { 
                	hashMap.put("person7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("person8".equals(childElement.getNodeName()))) { 
                	hashMap.put("person8",childElement.getFirstChild().getNodeValue()); 
                }
                
                else if (("date1".equals(childElement.getNodeName()))) { 
                    hashMap.put("date1",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("date2".equals(childElement.getNodeName()))) { 
                    hashMap.put("date2",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("date3".equals(childElement.getNodeName()))) { 
                    hashMap.put("date3",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("date4".equals(childElement.getNodeName()))) { 
                	hashMap.put("date4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("date5".equals(childElement.getNodeName()))) { 
                	hashMap.put("date5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("date6".equals(childElement.getNodeName()))) { 
                	hashMap.put("date6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("date7".equals(childElement.getNodeName()))) { 
                	hashMap.put("date7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("date8".equals(childElement.getNodeName()))) { 
                	hashMap.put("date8",childElement.getFirstChild().getNodeValue()); 
                }
            } 
        } 
        return hashMap; 
    } 
	
	public static HashMap<String, String> parseNumberXml(InputStream inStream) throws Exception { 
        HashMap<String, String> hashMap = new HashMap<String, String>(); 
        // 实例化一个文档构建器工厂 
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
        // 通过文档构建器工厂获取一个文档构建器 
        DocumentBuilder builder = factory.newDocumentBuilder(); 
        // 通过文档通过文档构建器构建一个文档实例 
        Document document = builder.parse(inStream); 
        //获取XML文件根节点 
        Element root = document.getDocumentElement(); 
        //获得所有子节点 
        NodeList childNodes = root.getChildNodes(); 
        
        for (int j = 0; j < childNodes.getLength(); j++) 
        { 
            //遍历子节点 
            Node childNode = (Node) childNodes.item(j); 
            if (childNode.getNodeType() == Node.ELEMENT_NODE) 
            { 
                Element childElement = (Element) childNode; 
                if ("date1".equals(childElement.getNodeName())){  
                    hashMap.put("date1",childElement.getFirstChild().getNodeValue()); 
                } 
                else if (("date2".equals(childElement.getNodeName()))) { 
                    hashMap.put("date2",childElement.getFirstChild().getNodeValue()); 
                } 
                else if (("date3".equals(childElement.getNodeName()))) { 
                    hashMap.put("date3",childElement.getFirstChild().getNodeValue()); 
                } 
                else if (("date4".equals(childElement.getNodeName()))) { 
                    hashMap.put("date4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("date5".equals(childElement.getNodeName()))) { 
                    hashMap.put("date5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("date6".equals(childElement.getNodeName()))) { 
                    hashMap.put("date6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("date7".equals(childElement.getNodeName()))) { 
                    hashMap.put("date7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("date8".equals(childElement.getNodeName()))) { 
                    hashMap.put("date8",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("date9".equals(childElement.getNodeName()))) { 
                    hashMap.put("date9",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("date10".equals(childElement.getNodeName()))) { 
                	hashMap.put("date10",childElement.getFirstChild().getNodeValue()); 
                }
                
                else if (("figurea1".equals(childElement.getNodeName()))) { 
                    hashMap.put("figurea1",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurea2".equals(childElement.getNodeName()))) { 
                    hashMap.put("figurea2",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurea3".equals(childElement.getNodeName()))) { 
                    hashMap.put("figurea3",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurea4".equals(childElement.getNodeName()))) { 
                	hashMap.put("figurea4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurea5".equals(childElement.getNodeName()))) { 
                	hashMap.put("figurea5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurea6".equals(childElement.getNodeName()))) { 
                	hashMap.put("figurea6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurea7".equals(childElement.getNodeName()))) { 
                	hashMap.put("figurea7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurea8".equals(childElement.getNodeName()))) { 
                	hashMap.put("figurea8",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurea9".equals(childElement.getNodeName()))) { 
                	hashMap.put("figurea9",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurea10".equals(childElement.getNodeName()))) { 
                	hashMap.put("figurea10",childElement.getFirstChild().getNodeValue()); 
                }
                
                else if (("figureb1".equals(childElement.getNodeName()))) { 
                    hashMap.put("figureb1",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figureb2".equals(childElement.getNodeName()))) { 
                    hashMap.put("figureb2",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figureb3".equals(childElement.getNodeName()))) { 
                    hashMap.put("figureb3",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figureb4".equals(childElement.getNodeName()))) { 
                	hashMap.put("figureb4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figureb5".equals(childElement.getNodeName()))) { 
                	hashMap.put("figureb5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figureb6".equals(childElement.getNodeName()))) { 
                	hashMap.put("figureb6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figureb7".equals(childElement.getNodeName()))) { 
                	hashMap.put("figureb7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figureb8".equals(childElement.getNodeName()))) { 
                	hashMap.put("figureb8",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figureb9".equals(childElement.getNodeName()))) { 
                	hashMap.put("figureb9",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figureb10".equals(childElement.getNodeName()))) { 
                	hashMap.put("figureb10",childElement.getFirstChild().getNodeValue()); 
                }
                
                else if (("figurec1".equals(childElement.getNodeName()))) { 
                    hashMap.put("figurec1",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurec2".equals(childElement.getNodeName()))) { 
                    hashMap.put("figurec2",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurec3".equals(childElement.getNodeName()))) { 
                    hashMap.put("figurec3",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurec4".equals(childElement.getNodeName()))) { 
                	hashMap.put("figurec4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurec5".equals(childElement.getNodeName()))) { 
                	hashMap.put("figurec5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurec6".equals(childElement.getNodeName()))) { 
                	hashMap.put("figurec6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurec7".equals(childElement.getNodeName()))) { 
                	hashMap.put("figurec7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurec8".equals(childElement.getNodeName()))) { 
                	hashMap.put("figurec8",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurec9".equals(childElement.getNodeName()))) { 
                	hashMap.put("figurec9",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("figurec10".equals(childElement.getNodeName()))) { 
                	hashMap.put("figurec10",childElement.getFirstChild().getNodeValue()); 
                }
                
                else if (("personnum1".equals(childElement.getNodeName()))) { 
                    hashMap.put("personnum1",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personnum2".equals(childElement.getNodeName()))) { 
                    hashMap.put("personnum2",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personnum3".equals(childElement.getNodeName()))) { 
                    hashMap.put("personnum3",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personnum4".equals(childElement.getNodeName()))) { 
                	hashMap.put("personnum4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personnum5".equals(childElement.getNodeName()))) { 
                	hashMap.put("personnum5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personnum6".equals(childElement.getNodeName()))) { 
                	hashMap.put("personnum6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personnum7".equals(childElement.getNodeName()))) { 
                	hashMap.put("personnum7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personnum8".equals(childElement.getNodeName()))) { 
                	hashMap.put("personnum8",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personnum9".equals(childElement.getNodeName()))) { 
                	hashMap.put("personnum9",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personnum10".equals(childElement.getNodeName()))) { 
                	hashMap.put("personnum10",childElement.getFirstChild().getNodeValue()); 
                }
                
                else if (("name1".equals(childElement.getNodeName()))) { 
                    hashMap.put("name1",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("name2".equals(childElement.getNodeName()))) { 
                    hashMap.put("name2",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("name3".equals(childElement.getNodeName()))) { 
                    hashMap.put("name3",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("name4".equals(childElement.getNodeName()))) { 
                	hashMap.put("name4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("name5".equals(childElement.getNodeName()))) { 
                	hashMap.put("name5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("name6".equals(childElement.getNodeName()))) { 
                	hashMap.put("name6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("name7".equals(childElement.getNodeName()))) { 
                	hashMap.put("name7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("name8".equals(childElement.getNodeName()))) { 
                	hashMap.put("name8",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("name9".equals(childElement.getNodeName()))) { 
                	hashMap.put("name9",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("name10".equals(childElement.getNodeName()))) { 
                	hashMap.put("name10",childElement.getFirstChild().getNodeValue()); 
                }
                
                else if (("num1".equals(childElement.getNodeName()))) { 
                    hashMap.put("num1",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("num2".equals(childElement.getNodeName()))) { 
                    hashMap.put("num2",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("num3".equals(childElement.getNodeName()))) { 
                    hashMap.put("num3",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("num4".equals(childElement.getNodeName()))) { 
                	hashMap.put("num4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("num5".equals(childElement.getNodeName()))) { 
                	hashMap.put("num5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("num6".equals(childElement.getNodeName()))) { 
                	hashMap.put("num6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("num7".equals(childElement.getNodeName()))) { 
                	hashMap.put("num7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("num8".equals(childElement.getNodeName()))) { 
                	hashMap.put("num8",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("num9".equals(childElement.getNodeName()))) { 
                	hashMap.put("num9",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("num10".equals(childElement.getNodeName()))) { 
                	hashMap.put("num10",childElement.getFirstChild().getNodeValue()); 
                }
                
                else if (("value1".equals(childElement.getNodeName()))) { 
                    hashMap.put("value1",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("value2".equals(childElement.getNodeName()))) { 
                    hashMap.put("value2",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("value3".equals(childElement.getNodeName()))) { 
                    hashMap.put("value3",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("value4".equals(childElement.getNodeName()))) { 
                	hashMap.put("value4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("value5".equals(childElement.getNodeName()))) { 
                	hashMap.put("value5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("value6".equals(childElement.getNodeName()))) { 
                	hashMap.put("value6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("value7".equals(childElement.getNodeName()))) { 
                	hashMap.put("value7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("value8".equals(childElement.getNodeName()))) { 
                	hashMap.put("value8",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("value9".equals(childElement.getNodeName()))) { 
                	hashMap.put("value9",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("value10".equals(childElement.getNodeName()))) { 
                	hashMap.put("value10",childElement.getFirstChild().getNodeValue()); 
                }
                
                else if (("persona1".equals(childElement.getNodeName()))) { 
                    hashMap.put("persona1",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persona2".equals(childElement.getNodeName()))) { 
                    hashMap.put("persona2",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persona3".equals(childElement.getNodeName()))) { 
                    hashMap.put("persona3",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persona4".equals(childElement.getNodeName()))) { 
                	hashMap.put("persona4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persona5".equals(childElement.getNodeName()))) { 
                	hashMap.put("persona5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persona6".equals(childElement.getNodeName()))) { 
                	hashMap.put("persona6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persona7".equals(childElement.getNodeName()))) { 
                	hashMap.put("persona7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persona8".equals(childElement.getNodeName()))) { 
                	hashMap.put("persona8",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persona9".equals(childElement.getNodeName()))) { 
                	hashMap.put("persona9",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persona10".equals(childElement.getNodeName()))) { 
                	hashMap.put("persona10",childElement.getFirstChild().getNodeValue()); 
                }
                
                else if (("personb1".equals(childElement.getNodeName()))) { 
                    hashMap.put("personb1",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personb2".equals(childElement.getNodeName()))) { 
                    hashMap.put("personb2",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personb3".equals(childElement.getNodeName()))) { 
                    hashMap.put("personb3",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personb4".equals(childElement.getNodeName()))) { 
                	hashMap.put("personb4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personb5".equals(childElement.getNodeName()))) { 
                	hashMap.put("personb5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personb6".equals(childElement.getNodeName()))) { 
                	hashMap.put("personb6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personb7".equals(childElement.getNodeName()))) { 
                	hashMap.put("personb7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personb8".equals(childElement.getNodeName()))) { 
                	hashMap.put("personb8",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personb9".equals(childElement.getNodeName()))) { 
                	hashMap.put("personb9",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personb10".equals(childElement.getNodeName()))) { 
                	hashMap.put("personb10",childElement.getFirstChild().getNodeValue()); 
                }
                
                else if (("personc1".equals(childElement.getNodeName()))) { 
                    hashMap.put("personc1",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personc2".equals(childElement.getNodeName()))) { 
                    hashMap.put("personc2",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personc3".equals(childElement.getNodeName()))) { 
                    hashMap.put("personc3",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personc4".equals(childElement.getNodeName()))) { 
                	hashMap.put("personc4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personc5".equals(childElement.getNodeName()))) { 
                	hashMap.put("personc5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personc6".equals(childElement.getNodeName()))) { 
                	hashMap.put("personc6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personc7".equals(childElement.getNodeName()))) { 
                	hashMap.put("personc7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personc8".equals(childElement.getNodeName()))) { 
                	hashMap.put("personc8",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personc9".equals(childElement.getNodeName()))) { 
                	hashMap.put("personc9",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personc10".equals(childElement.getNodeName()))) { 
                	hashMap.put("personc10",childElement.getFirstChild().getNodeValue()); 
                }
                
                else if (("persond1".equals(childElement.getNodeName()))) { 
                    hashMap.put("persond1",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persond2".equals(childElement.getNodeName()))) { 
                    hashMap.put("persond2",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persond3".equals(childElement.getNodeName()))) { 
                    hashMap.put("persond3",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persond4".equals(childElement.getNodeName()))) { 
                	hashMap.put("persond4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persond5".equals(childElement.getNodeName()))) { 
                	hashMap.put("persond5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persond6".equals(childElement.getNodeName()))) { 
                	hashMap.put("persond6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persond7".equals(childElement.getNodeName()))) { 
                	hashMap.put("persond7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persond8".equals(childElement.getNodeName()))) { 
                	hashMap.put("persond8",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persond9".equals(childElement.getNodeName()))) { 
                	hashMap.put("persond9",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persond10".equals(childElement.getNodeName()))) { 
                	hashMap.put("persond10",childElement.getFirstChild().getNodeValue()); 
                }
                
                else if (("persone1".equals(childElement.getNodeName()))) { 
                    hashMap.put("persone1",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persone2".equals(childElement.getNodeName()))) { 
                    hashMap.put("persone2",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persone3".equals(childElement.getNodeName()))) { 
                    hashMap.put("persone3",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persone4".equals(childElement.getNodeName()))) { 
                	hashMap.put("persone4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persone5".equals(childElement.getNodeName()))) { 
                	hashMap.put("persone5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persone6".equals(childElement.getNodeName()))) { 
                	hashMap.put("persone6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persone7".equals(childElement.getNodeName()))) { 
                	hashMap.put("persone7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persone8".equals(childElement.getNodeName()))) { 
                	hashMap.put("persone8",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persone9".equals(childElement.getNodeName()))) { 
                	hashMap.put("persone9",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("persone10".equals(childElement.getNodeName()))) { 
                	hashMap.put("persone10",childElement.getFirstChild().getNodeValue()); 
                }
                
                else if (("personf1".equals(childElement.getNodeName()))) { 
                    hashMap.put("personf1",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personf2".equals(childElement.getNodeName()))) { 
                    hashMap.put("personf2",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personf3".equals(childElement.getNodeName()))) { 
                    hashMap.put("personf3",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personf4".equals(childElement.getNodeName()))) { 
                	hashMap.put("personf4",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personf5".equals(childElement.getNodeName()))) { 
                	hashMap.put("personf5",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personf6".equals(childElement.getNodeName()))) { 
                	hashMap.put("personf6",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personf7".equals(childElement.getNodeName()))) { 
                	hashMap.put("personf7",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personf8".equals(childElement.getNodeName()))) { 
                	hashMap.put("personf8",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personf9".equals(childElement.getNodeName()))) { 
                	hashMap.put("personf9",childElement.getFirstChild().getNodeValue()); 
                }
                else if (("personf10".equals(childElement.getNodeName()))) { 
                	hashMap.put("personf10",childElement.getFirstChild().getNodeValue()); 
                }
                
            } 
        } 
        return hashMap; 
    } 
	
	public static HashMap<String, String> parseNoticeXml(InputStream inStream) throws Exception { 
        HashMap<String, String> hashMap = new HashMap<String, String>(); 
        // 实例化一个文档构建器工厂 
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
        // 通过文档构建器工厂获取一个文档构建器 
        DocumentBuilder builder = factory.newDocumentBuilder(); 
        // 通过文档通过文档构建器构建一个文档实例 
        Document document = builder.parse(inStream); 
        //获取XML文件根节点 
        Element root = document.getDocumentElement(); 
        //获得所有子节点 
        NodeList childNodes = root.getChildNodes(); 
        
        for (int j = 0; j < childNodes.getLength(); j++) 
        { 
            //遍历子节点 
            Node childNode = (Node) childNodes.item(j); 
            if (childNode.getNodeType() == Node.ELEMENT_NODE) 
            { 
                Element childElement = (Element) childNode;  
                if ("title".equals(childElement.getNodeName())) {  
                    hashMap.put("title",childElement.getFirstChild().getNodeValue()); 
                }else if (("content".equals(childElement.getNodeName()))) { 
                    hashMap.put("content",childElement.getFirstChild().getNodeValue()); 
                }else if (("date".equals(childElement.getNodeName()))) { 
                    hashMap.put("date",childElement.getFirstChild().getNodeValue()); 
                } 
            } 
        } 
        return hashMap; 
    }
}
