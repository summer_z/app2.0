package com.qiangpro.util;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.widget.Toast;

import com.qiangpro.main.R;
import com.qiangpro.parse.ParseRemoteXml;
import com.qiangpro.util.Constants.ConstantValues;

public class UpdateManager {
	private Context context;
	private String mark = "success";
	private String path = "http://" + ConstantValues.IP + "/goqiang/resources/xml/version.xml";
	private String path1 = "http://" + ConstantValues.IP + "/goqiang/resources/xml/goods_tab1_top.xml";
	private String path2 = "http://" + ConstantValues.IP + "/goqiang/resources/xml/goods_tab1_block.xml";
	private String path3 = "http://" + ConstantValues.IP + "/goqiang/resources/xml/goods_tab2.xml";
	private String path4 = "http://" + ConstantValues.IP + "/goqiang/resources/xml/goods_tab1_campus.xml";
	private String pathNum = "http://" + ConstantValues.IP + "/goqiang/resources/xml/tab1_number.xml";
	private String pathNotice = "http://" + ConstantValues.IP + "/goqiang/resources/xml/tab1_notice.xml";
	private String xmlPath[] = {path1, path2, path3, path4};
	private String fileName[] = {ConstantValues.GOODS_TAB1_TOP_FILENAME,
			ConstantValues.GOODS_TAB1_BLOCK_FILENAME, ConstantValues.GOODS_TAB2_FILENAME, ConstantValues.GOODS_CAMPUS_FILENAME};
	private int localVersion = 0;
	private int serviceVersion = 0;
	
	public UpdateManager(Context context) { 
        this.context = context; 
    }
	
	public void checkUpdate(boolean tg){
        if (tg) { 
            // 显示下载提示对话框 
        	DownloadManager downloadManager = new DownloadManager(context);
        	downloadManager.showNoticeDialog();
        } else { 
            Toast.makeText(context, R.string.update_no, Toast.LENGTH_LONG).show(); 
        } 
    }
	
	public void saveRemoteVersionMessage(){
		localVersion = getLocalVersion(context);
		try{  
            URL url = new URL(path);  
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();  
            conn.setReadTimeout(5*1000);  
            conn.setRequestMethod("GET");  
            InputStream inStream = conn.getInputStream();  
            //使用DOM解析
            HashMap<String, String> hashMap = new HashMap<String, String>();
            hashMap = ParseRemoteXml.parseVersionXml(inStream);
            serviceVersion = Integer.parseInt(hashMap.get("version")) ;
            String name = hashMap.get("name") ;
            String url2 = hashMap.get("url") ;
            boolean target = false;
        	if(serviceVersion > localVersion){
        		//有新版本，重新设置标志位
        		target = true ;
        	}
    		SharedPreferences share = context.getSharedPreferences(ConstantValues.VERSION_FILENAME,
    				Activity.MODE_PRIVATE);	// 指定操作的文件名称
    		SharedPreferences.Editor edit = share.edit(); 	// 编辑文件
    		edit.putBoolean("version", target);			// 保存整型
    		edit.putString("name", name) ;
    		edit.putString("url", url2) ;
    		edit.commit() ;	
        } catch (Exception e)  {  
            e.printStackTrace();  
        }
	}
	
	private int getLocalVersion(Context context) 
    { 
        int localVersion = 0; 
        try{ 
            // 获取软件版本号，对应AndroidManifest.xml下android:versionCode 
        	localVersion = context.getPackageManager().getPackageInfo(ConstantValues.PACKAGE_NAME, 0).versionCode; 
        } catch (NameNotFoundException e) { 
            e.printStackTrace(); 
        } 
        return localVersion; 
    }
	
	public void saveRemoteGoodsInfo(){
		for(int i=0; i<xmlPath.length; i++){
			try{  
	            URL url = new URL(xmlPath[i]);  
	            HttpURLConnection conn = (HttpURLConnection)url.openConnection();  
	            conn.setReadTimeout(5*1000);  
	            conn.setRequestMethod("GET");  
	            InputStream inStream = conn.getInputStream();  
	            //使用DOM解析
	            HashMap<String, String> hashMap = new HashMap<String, String>();
	            hashMap = ParseRemoteXml.parseGoodsXml(inStream);
	            
	            String title[] = {"title1", "title2", "title3", "title4", "title5"
	            		, "title6", "title7", "title8"};
	            String price[] = {"price1", "price2", "price3", "price4", "price5"
	            		, "price6", "price7", "price8"};
	            String num[] = {"num1", "num2", "num3", "num4", "num5"
	            		, "num6", "num7", "num8"};
	            String person[] = {"person1", "person2", "person3", "person4", "person5"
	            		, "person6", "person7", "person8"};
	            String date[] = {"date1", "date2", "date3", "date4", "date5"
	            		, "date6", "date7", "date8"};
	    		SharedPreferences share = context.getSharedPreferences(fileName[i], Activity.MODE_PRIVATE);	// 指定操作的文件名称
	    		SharedPreferences.Editor edit = share.edit(); 	// 编辑文件
	    		for(int j=0; j<title.length; j++){
	    			edit.putString(title[j], hashMap.get(title[j])) ;
		    		edit.putString(price[j], hashMap.get(price[j])) ;
		    		edit.putString(num[j], hashMap.get(num[j])) ;
		    		edit.putString(person[j], hashMap.get(person[j])) ;
		    		edit.putString(date[j], hashMap.get(date[j])) ;
	            }
	    		edit.commit() ;	
	        } catch (Exception e)  {  
	            e.printStackTrace();  
	        }
		}
	}
	
	public void saveRemoteNumberInfo(){
		try{  
            URL url = new URL(pathNum);  
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();  
            conn.setReadTimeout(5*1000);  
            conn.setRequestMethod("GET");  
            InputStream inStream = conn.getInputStream();  
            //使用DOM解析
            HashMap<String, String> hashMap = new HashMap<String, String>();
            hashMap = ParseRemoteXml.parseNumberXml(inStream);
            
            String date[] = {"date1", "date2", "date3", "date4", "date5"
            		, "date6", "date7", "date8", "date9", "date10"};
            String figurea[] = {"figurea1", "figurea2", "figurea3", "figurea4", "figurea5"
            		, "figurea6", "figurea7", "figurea8", "figurea9", "figurea10"};
            String figureb[] = {"figureb1", "figureb2", "figureb3", "figureb4", "figureb5"
            		, "figureb6", "figureb7", "figureb8", "figureb9", "figureb10"};
            String figurec[] = {"figurec1", "figurec2", "figurec3", "figurec4", "figurec5"
            		, "figurec6", "figurec7", "figurec8", "figurec9", "figurec10"};
            String personnum[] = {"personnum1", "personnum2", "personnum3", "personnum4", "personnum5"
            		, "personnum6", "personnum7", "personnum8", "personnum9", "personnum10"};
            String name[] = {"name1", "name2", "name3", "name4", "name5"
            		, "name6", "name7", "name8", "name9", "name10"};
            String num[] = {"num1", "num2", "num3", "num4", "num5"
            		, "num6", "num7", "num8", "num9", "num10"};
            String value[] = {"value1", "value2", "value3", "value4", "value5"
            		, "value6", "value7", "value8", "value9", "value10"};
            String persona[] = {"persona1", "persona2", "persona3", "persona4", "persona5"
            		, "persona6", "persona7", "persona8", "persona9", "persona10"};
            String personb[] = {"personb1", "personb2", "personb3", "personb4", "personb5"
            		, "personb6", "personb7", "personb8", "personb9", "personb10"};
            String personc[] = {"personc1", "personc2", "personc3", "personc4", "personc5"
            		, "personc6", "personc7", "personc8", "personc9", "personc10"};
            String persond[] = {"persond1", "persond2", "persond3", "persond4", "persond5"
            		, "persond6", "persond7", "persond8", "persond9", "persond10"};
            String persone[] = {"persone1", "persone2", "persone3", "persone4", "persone5"
            		, "persone6", "persone7", "persone8", "persone9", "persone10"};
            String personf[] = {"personf1", "personf2", "personf3", "personf4", "personf5"
            		, "personf6", "personf7", "personf8", "personf9", "personf10"};
            
    		SharedPreferences share = context.getSharedPreferences(ConstantValues.TAB_NUMBER_FILENAME, Activity.MODE_PRIVATE);	// 指定操作的文件名称
    		SharedPreferences.Editor edit = share.edit(); 	// 编辑文件
    		for(int j=0; j<date.length; j++){
    			edit.putString(date[j], hashMap.get(date[j])) ;
	    		edit.putString(figurea[j], hashMap.get(figurea[j])) ;
	    		edit.putString(figureb[j], hashMap.get(figureb[j])) ;
	    		edit.putString(figurec[j], hashMap.get(figurec[j])) ;
	    		edit.putString(personnum[j], hashMap.get(personnum[j])) ;
	    		edit.putString(name[j], hashMap.get(name[j])) ;
	    		edit.putString(num[j], hashMap.get(num[j])) ;
	    		edit.putString(value[j], hashMap.get(value[j])) ;
	    		edit.putString(persona[j], hashMap.get(persona[j])) ;
	    		edit.putString(personb[j], hashMap.get(personb[j])) ;
	    		edit.putString(personc[j], hashMap.get(personc[j])) ;
	    		edit.putString(persond[j], hashMap.get(persond[j])) ;
	    		edit.putString(persone[j], hashMap.get(persone[j])) ;
	    		edit.putString(personf[j], hashMap.get(personf[j])) ;
            }
    		edit.commit() ;	
        } catch (Exception e)  {  
            e.printStackTrace();  
        }
	}
	
	public String saveRemoteNoticeMessage(){
		try{  
            URL url = new URL(pathNotice);  
            HttpURLConnection conn = (HttpURLConnection)url.openConnection(); 
            conn.setConnectTimeout(2500);
            conn.setReadTimeout(2500);  
            conn.setRequestMethod("GET");  
            InputStream inStream = conn.getInputStream();  
            //使用DOM解析
            HashMap<String, String> hashMap = new HashMap<String, String>();
            hashMap = ParseRemoteXml.parseNoticeXml(inStream);
            String title = hashMap.get("title") ;
            String content = hashMap.get("content") ;
            String date = hashMap.get("date") ;
            
    		SharedPreferences share = context.getSharedPreferences(Constants.NOTICE_FILENAME,
    				Activity.MODE_PRIVATE);	
    		SharedPreferences.Editor edit = share.edit(); 	
    		edit.putString("title", title);			
    		edit.putString("content", content) ;
    		edit.putString("date", date) ;
    		edit.commit() ;	
        } catch (Exception e)  {  
        	mark = "error" ;
        }
		return mark;
	}
	
}
