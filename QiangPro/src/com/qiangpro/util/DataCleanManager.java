package com.qiangpro.util;

import java.io.File;

import android.content.Context;

public class DataCleanManager {

	public static void cleanCache(Context context){
		deleteFilesByDirectory(context.getCacheDir());
	}
	
	private static void deleteFilesByDirectory(File directory) {
	    if (directory != null && directory.exists() && directory.isDirectory()) {
	        for (File item : directory.listFiles()) {
	            item.delete();
	        }
	    }
	}
	
}
