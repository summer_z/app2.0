package com.qiangpro.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.qiangpro.main.R;

public class DialogManager {
	
	public static void doExit(final Activity activity){
		final Dialog dialog = new AlertDialog.Builder(activity).create();
		dialog.show() ;	// 显示对话框
		dialog.setContentView(R.layout.dialog_exit);
		Button exit_button_yes, exit_button_no;
		exit_button_no = (Button)dialog.findViewById(R.id.exit_button_no);
		exit_button_yes = (Button)dialog.findViewById(R.id.exit_button_yes);
		exit_button_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		exit_button_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				activity.finish() ;	
			}
		});
	}
	
	public static void doDialog(final Activity activity){
		final Dialog dialog = new AlertDialog.Builder(activity).create();
		dialog.show() ;	// 显示对话框
		dialog.setContentView(R.layout.dialog_prompt);
		Button prompt_button_yes;
		TextView prompt_dialog_title, prompt_dialog_content; 
		prompt_button_yes = (Button)dialog.findViewById(R.id.prompt_button_yes);
		prompt_dialog_title = (TextView)dialog.findViewById(R.id.prompt_dialog_title);
		prompt_dialog_content = (TextView)dialog.findViewById(R.id.prompt_dialog_content);
		prompt_dialog_title.setText("哈哈，又当了一次土匪！");
		prompt_dialog_content.setText("号码已提交,敬请等待,已获得0.01元……");
		prompt_button_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
	}
	
	public static void doDialogReduceGrade(final Activity activity){
		final Dialog dialog = new AlertDialog.Builder(activity).create();
		dialog.show() ;	// 显示对话框
		dialog.setContentView(R.layout.dialog_prompt);
		Button prompt_button_yes;
		TextView prompt_dialog_title, prompt_dialog_content; 
		prompt_button_yes = (Button)dialog.findViewById(R.id.prompt_button_yes);
		prompt_dialog_title = (TextView)dialog.findViewById(R.id.prompt_dialog_title);
		prompt_dialog_content = (TextView)dialog.findViewById(R.id.prompt_dialog_content);
		prompt_dialog_title.setText("哈哈，又当了一次土匪！");
		prompt_dialog_content.setText("号码已提交,敬请等待,已扣除1元……");
		prompt_button_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
	}

	public static void doDialogForget(final Activity activity){
		final Dialog dialog = new AlertDialog.Builder(activity).create();
		dialog.show() ;	// 显示对话框
		dialog.setContentView(R.layout.dialog_prompt);
		Button prompt_button_yes;
		TextView prompt_dialog_title, prompt_dialog_content; 
		prompt_button_yes = (Button)dialog.findViewById(R.id.prompt_button_yes);
		prompt_dialog_title = (TextView)dialog.findViewById(R.id.prompt_dialog_title);
		prompt_dialog_content = (TextView)dialog.findViewById(R.id.prompt_dialog_content);
		prompt_dialog_title.setText("验证成功");
		prompt_dialog_content.setText("初始密码：123456");
		prompt_button_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
	}
	
	public static void doDialogOrder(final Activity activity){
		final Dialog dialog = new AlertDialog.Builder(activity).create();
		dialog.show() ;	// 显示对话框
		dialog.setContentView(R.layout.dialog_prompt);
		Button prompt_button_yes;
		TextView prompt_dialog_title, prompt_dialog_content; 
		prompt_button_yes = (Button)dialog.findViewById(R.id.prompt_button_yes);
		prompt_dialog_title = (TextView)dialog.findViewById(R.id.prompt_dialog_title);
		prompt_dialog_content = (TextView)dialog.findViewById(R.id.prompt_dialog_content);
		prompt_dialog_title.setText("TOFREE");
		prompt_dialog_content.setText("订单已提交，快递员正在拼命送货……");
		prompt_button_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
	}
	
}
