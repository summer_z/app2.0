package com.qiangpro.util;

import com.qiangpro.main.R;

import com.qiangpro.main.TabActivity1;
import com.qiangpro.main.TabActivity2;
import com.qiangpro.main.TabActivity3;
import com.qiangpro.main.TabActivity4;


public class Constants {
	public static final int LOGIN_SUCCESS = 0;//登录成功
	public static final int REGISTER_SUCCESS = 3;
	public static final int GOODSDETAILS_SUCCESS = 4;
	public static final int MODIFY_SUCCESS = 5;
	public static final int POST_SUCCESS = 5;
	public static final int GETALLCITY_SUCCESS = 0;
	public static final int GETALLCITY_FAILED = 1;
	public static final int LIST_QUANTITY = 5;
	public static final String SAVE_NAME_PWD_FILENAME = "usernamepwd";//保存用户名和密码
	public static final String REGISTER_NUM_FILENAME = "register_num";//保存注册的次数
	public static final String SUBMIT_NUM_FILENAME = "submit_num";//保存提交的次数
	public static final String NOTES_WIN_FILENAME = "notes_win";//保存中奖记录
	public static final String NOTES_BUY_FILENAME = "notes_buy";//保存购买记录
	public static final String NOTICE_FILENAME = "notice";//保存公告信息
	public static final class ConstantValues{
		//Tab选项卡的图标
		public static int tabImageViewArray[] = {
			R.drawable.tab1_backgroud,
			R.drawable.tab2_backgroud,
			R.drawable.tab3_backgroud,
			R.drawable.tab4_backgroud
		};
		//Tab选项卡的文字
		public static String tabTextViewArray[] = {
			"首页", "推荐", "我的", "设置"
		};
		//Tab对应的每个Activity
		public static Class tabClassArray[] = {
			TabActivity1.class,
			TabActivity2.class,
			TabActivity3.class,
			TabActivity4.class
		};
		//首页viewpager图片url地址
		public static String tab1activityImageUrlArray[] = {
			"http://115.29.45.115/goqiang/resources/homeviewpager/h1_1.jpg",
			"http://115.29.45.115/goqiang/resources/homeviewpager/h2_1.jpg", 
			"http://115.29.45.115/goqiang/resources/homeviewpager/h3_1.jpg", 
			"http://115.29.45.115/goqiang/resources/homeviewpager/h4_1.jpg",
			"http://115.29.45.115/goqiang/resources/homeviewpager/h5_1.jpg"
		};
		//推荐版块图片url地址
		public static String tab2activityImageUrlArray[] = {
			"http://115.29.45.115/goqiang/resources/recommend/h1_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h2_1.png", 
			"http://115.29.45.115/goqiang/resources/recommend/h3_1.png", 
			"http://115.29.45.115/goqiang/resources/recommend/h4_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h5_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h6_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h7_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h8_1.png"
		};
		//推荐版块图片url地址
		public static String recommendUrl2[] = {
			"http://115.29.45.115/goqiang/resources/recommend/h9_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h10_1.png", 
			"http://115.29.45.115/goqiang/resources/recommend/h11_1.png", 
			"http://115.29.45.115/goqiang/resources/recommend/h12_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h13_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h14_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h15_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h16_1.png"
		};
		//推荐版块图片url地址
		public static String recommendUrl3[] = {
			"http://115.29.45.115/goqiang/resources/recommend/h17_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h18_1.png", 
			"http://115.29.45.115/goqiang/resources/recommend/h19_1.png", 
			"http://115.29.45.115/goqiang/resources/recommend/h20_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h21_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h22_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h23_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h24_1.png"
		};
		//推荐版块图片url地址
		public static String recommendUrl4[] = {
			"http://115.29.45.115/goqiang/resources/recommend/h25_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h26_1.png", 
			"http://115.29.45.115/goqiang/resources/recommend/h27_1.png", 
			"http://115.29.45.115/goqiang/resources/recommend/h28_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h29_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h30_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h31_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h32_1.png"
		};
		//推荐版块图片url地址
		public static String recommendUrl5[] = {
			"http://115.29.45.115/goqiang/resources/recommend/h33_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h34_1.png", 
			"http://115.29.45.115/goqiang/resources/recommend/h35_1.png", 
			"http://115.29.45.115/goqiang/resources/recommend/h36_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h37_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h38_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h39_1.png",
			"http://115.29.45.115/goqiang/resources/recommend/h40_1.png"
		};
		//免费专区版块图片url地址
		public static String tab1FreeUrls1[] = {
			"http://115.29.45.115/goqiang/resources/free/h1_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h2_1.jpg", 
			"http://115.29.45.115/goqiang/resources/free/h3_1.jpg", 
			"http://115.29.45.115/goqiang/resources/free/h4_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h5_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h6_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h7_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h8_1.jpg"
		};
		//免费专区版块图片url地址
		public static String tab1FreeUrls2[] = {
			"http://115.29.45.115/goqiang/resources/free/h9_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h10_1.jpg", 
			"http://115.29.45.115/goqiang/resources/free/h11_1.jpg", 
			"http://115.29.45.115/goqiang/resources/free/h12_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h13_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h14_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h15_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h16_1.jpg"
		};
		//免费专区版块图片url地址
		public static String tab1FreeUrls3[] = {
			"http://115.29.45.115/goqiang/resources/free/h17_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h18_1.jpg", 
			"http://115.29.45.115/goqiang/resources/free/h19_1.jpg", 
			"http://115.29.45.115/goqiang/resources/free/h20_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h21_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h22_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h23_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h24_1.jpg"
		};
		//免费专区版块图片url地址
		public static String tab1FreeUrls4[] = {
			"http://115.29.45.115/goqiang/resources/free/h25_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h26_1.jpg", 
			"http://115.29.45.115/goqiang/resources/free/h27_1.jpg", 
			"http://115.29.45.115/goqiang/resources/free/h28_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h29_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h30_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h31_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h32_1.jpg"
		};
		//免费专区版块图片url地址
		public static String tab1FreeUrls5[] = {
			"http://115.29.45.115/goqiang/resources/free/h33_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h34_1.jpg", 
			"http://115.29.45.115/goqiang/resources/free/h35_1.jpg", 
			"http://115.29.45.115/goqiang/resources/free/h36_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h37_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h38_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h39_1.jpg",
			"http://115.29.45.115/goqiang/resources/free/h40_1.jpg"
		};
		//积分专区版块图片url地址
		public static String tab1GradeUrls1[] = {
			"http://115.29.45.115/goqiang/resources/grade/h1_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h2_1.jpg", 
			"http://115.29.45.115/goqiang/resources/grade/h3_1.jpg", 
			"http://115.29.45.115/goqiang/resources/grade/h4_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h5_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h6_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h7_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h8_1.jpg"
		};
		//积分专区版块图片url地址
		public static String tab1GradeUrls2[] = {
			"http://115.29.45.115/goqiang/resources/grade/h9_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h10_1.jpg", 
			"http://115.29.45.115/goqiang/resources/grade/h11_1.jpg", 
			"http://115.29.45.115/goqiang/resources/grade/h12_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h13_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h14_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h15_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h16_1.jpg"
		};
		//积分专区版块图片url地址
		public static String tab1GradeUrls3[] = {
			"http://115.29.45.115/goqiang/resources/grade/h17_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h18_1.jpg", 
			"http://115.29.45.115/goqiang/resources/grade/h19_1.jpg", 
			"http://115.29.45.115/goqiang/resources/grade/h20_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h21_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h22_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h23_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h24_1.jpg"
		};
		//积分专区版块图片url地址
		public static String tab1GradeUrls4[] = {
			"http://115.29.45.115/goqiang/resources/grade/h25_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h26_1.jpg", 
			"http://115.29.45.115/goqiang/resources/grade/h27_1.jpg", 
			"http://115.29.45.115/goqiang/resources/grade/h28_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h29_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h30_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h31_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h32_1.jpg"
		};
		//积分专区版块图片url地址
		public static String tab1GradeUrls5[] = {
			"http://115.29.45.115/goqiang/resources/grade/h33_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h34_1.jpg", 
			"http://115.29.45.115/goqiang/resources/grade/h35_1.jpg", 
			"http://115.29.45.115/goqiang/resources/grade/h36_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h37_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h38_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h39_1.jpg",
			"http://115.29.45.115/goqiang/resources/grade/h40_1.jpg"
		};
		
		public static String CHECK_FIRST_BUILD_FILENAME = "checkfirst";//存储是否是第一次加载的文件名
		public static String IP = "115.29.45.115";
		public static String PACKAGE_NAME = "com.qiangpro.main";
		public static String VERSION_FILENAME = "version";
		public static String GOODS_TAB1_TOP_FILENAME = "goods_tab1_top";
		public static String GOODS_TAB1_BLOCK_FILENAME = "goods_tab1_block";
		public static String GOODS_TAB2_FILENAME = "goods_tab2";
		public static String GOODS_CAMPUS_FILENAME = "goods_campus";
		public static String TAB_NUMBER_FILENAME = "tab_number";
		public static int DOWNLOAD = 1;//正在下载更新
		public static int DOWNLOAD_FINISH = 2;//下载完成
	}
	
	
}