package com.qiangpro.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.SoftReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;

//该类的主要作用是实现图片的异步加载
public class AsyncImageLoader {
	private  Context context;
	//图片缓存对象
	//键是图片的URL，值是一个SoftReference对象，该对象指向一个Bitmap对象
	private Map<String, SoftReference<Bitmap>> imageCache = 
		new HashMap<String, SoftReference<Bitmap>>();
	
	public AsyncImageLoader(Context context){
		this.context = context;
	}
	//实现图片的异步加载
	public Bitmap loadBitmap(final String imageUrl, final ImageCallback callback,
			final boolean isSave, final String imageName){
		//查询缓存，查看当前需要下载的图片是否已经存在于缓存当中
		if(imageCache.containsKey(imageUrl)){
			SoftReference<Bitmap> softReference=imageCache.get(imageUrl);
			if(softReference.get() != null){
				return softReference.get();
			}
		}
		
		final Handler handler=new Handler(){
			@Override
			public void handleMessage(Message msg) {
				callback.imageLoaded((Bitmap) msg.obj);
			}
		};
		//新开辟一个线程，该线程用于进行图片的下载
		new Thread(){
			public void run() {
				Bitmap bitmap = getHttpBitmap(imageUrl);
				if (bitmap != null) {
					//是否需要保存图片到本地
					if (isSave) {
						File cacheDir = new File(context.getFilesDir(), "cache");
						if(!cacheDir.exists()){
							cacheDir.mkdirs();
						}
						File cacheFile = new File(cacheDir, imageName);
						try {
							cacheFile.createNewFile();
							OutputStream output = new FileOutputStream(cacheFile);
							bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
							output.flush();
							output.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					imageCache.put(imageUrl, new SoftReference<Bitmap>(bitmap));
					Message message = handler.obtainMessage(0, bitmap);
					handler.sendMessage(message);
				}
			};
		}.start();
		return null;
	}
	//该方法用于根据图片的URL，从网络上下载图片
//	protected Bitmap loadImageFromUrl(String imageUrl) {
//		try {
//			//根据图片的URL，下载图片，并生成一个Drawable对象
////			return Drawable.createFromStream(new URL(imageUrl).openStream(), "src");
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
//	}
	
	public Bitmap getHttpBitmap(String url) {
		URL myFileURL;
        Bitmap bitmap = null;
		try {
			myFileURL = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) myFileURL.openConnection();
			conn.setConnectTimeout(5000);
			conn.setReadTimeout(5000) ;
			conn.setDoInput(true);
			conn.connect();
			InputStream is = conn.getInputStream();
			bitmap = BitmapFactory.decodeStream(is);
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bitmap;
	}
	//回调接口
	public interface ImageCallback{
		public void imageLoaded(Bitmap imageBitmap);
	}
}
