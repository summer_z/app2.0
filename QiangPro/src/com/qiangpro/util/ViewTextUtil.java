package com.qiangpro.util;

import android.widget.EditText;
import android.widget.TextView;

public class ViewTextUtil {

    public static String viewToString(EditText editText) {
        return editText.getText().toString().trim();
    }
    
    public static String textToString(TextView textView) {
        return textView.getText().toString().trim();
    }
    
}
