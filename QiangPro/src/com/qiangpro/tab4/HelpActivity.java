package com.qiangpro.tab4;

import java.util.ArrayList;
import java.util.List;

import com.qiangpro.main.GuideActivity;
import com.qiangpro.main.MainActivity;
import com.qiangpro.main.R;
import com.umeng.analytics.MobclickAgent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class HelpActivity extends Activity {
	private ViewPager pager;  // 左右滑动组件
//	private LinearLayout points;  // 用于存放焦点图标的布局
	private List<View> list;  // 用于保存4张图片布局数据源 
//    private ImageView[] views;  // 用于存放导航小圆点
    // 4个布局文件,整体管理
    private int[] ids = {R.layout.guideitem01, R.layout.guideitem02,
    		R.layout.guideitem03, R.layout.guideitem04,R.layout.guideitem05};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.guide_activity);
		pager = (ViewPager)this.findViewById(R.id.imagePages);
//		points = (LinearLayout)this.findViewById(R.id.pointGroup);
		list = new ArrayList<View>();
		// 滑动图片的集合，这里设置成了固定加载，当然也可动态加载。
		int[] slideImages = {
				R.drawable.guide_activity01,
				R.drawable.guide_activity02,
				R.drawable.guide_activity03,
				R.drawable.guide_activity04,
				R.drawable.guide_activity05
		};
		for(int i = 0; i < slideImages.length; i++){
			FrameLayout layout = (FrameLayout)getLayoutInflater().inflate(ids[i], null);
			ImageView image = (ImageView)layout.findViewById(R.id.imageView1);
			// 很重要
			image.setImageResource(slideImages[i]);
			list.add(layout);
		}
		// 根据list的长度决定做多大的导航图片数组
//		views = new ImageView[list.size()];
		pager.setAdapter(new MyPagerAdapter());
		pager.setOnPageChangeListener(new OnPageChangeListenerImpl() );
		
	}
	
	private class OnPageChangeListenerImpl implements OnPageChangeListener{
    	@Override
		public void onPageSelected(int arg0) {
			
		}
		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub
		}
		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub
		}
    }

    private class MyPagerAdapter extends PagerAdapter{
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}
		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == arg1;
		}
		@Override
		public Object instantiateItem(View container, int position) {
			( (ViewPager)container ).addView(list.get(position));
			return list.get(position);
		}
		@Override
		public void destroyItem(View container, int position, Object object) {
			// TODO Auto-generated method stub
			( (ViewPager)container ).removeView(list.get(position));
		}
	}
    public void button(View view){
		
		finish() ;
	}
    @Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
}
