package com.qiangpro.tab4;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.qiangpro.main.R;
import com.qiangpro.tab3.LoginActivity;
import com.qiangpro.util.Constants;
import com.qiangpro.util.Constants.ConstantValues;
import com.qiangpro.util.HttpUpload;
import com.qiangpro.util.NetworkConnection;
import com.umeng.analytics.MobclickAgent;

public class SuggestionActivity extends Activity {	
	private String url = "http://"  + ConstantValues.IP + "/goqiang/android/suggestion";
	private EditText suggestion_edittext;
	private ImageView suggestion_button;
	private List<NameValuePair> list = new ArrayList<NameValuePair>();
	private TextView namedByTab;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.setContentView(R.layout.suggestion_activity);
		
		this.namedByTab = (TextView)this.findViewById(R.id.namedByTab);
		this.namedByTab.setText("意见");
		this.suggestion_edittext = (EditText)super.findViewById(R.id.suggestion_edittext);
		this.suggestion_button = (ImageView)super.findViewById(R.id.suggestion_button);
		
		this.suggestion_button.setOnClickListener(new OnClickListenerButton());
		
	}
	
	private class OnClickListenerButton implements OnClickListener{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			SharedPreferences sp = SuggestionActivity.this.getSharedPreferences(Constants.SAVE_NAME_PWD_FILENAME, Activity.MODE_PRIVATE);
			boolean login_success = sp.getBoolean("login_success", false);
			if(login_success){
				if(NetworkConnection.isNetworkConnected(SuggestionActivity.this)){
					String content = suggestion_edittext.getText().toString().trim();
					String username = sp.getString("username", "没取到");
					list.add(new BasicNameValuePair("content", content));
					list.add(new BasicNameValuePair("username", username));
					new Thread(
						new Runnable(){
							@Override
							public void run() {
								// TODO Auto-generated method stub
								HttpUpload.postWithoutReturn(url, list);
							}
						}
					).start();
					Toast.makeText(SuggestionActivity.this, R.string.suggestion_activity_hint, Toast.LENGTH_LONG).show();
					SuggestionActivity.this.finish();
				}else{
					Toast.makeText(SuggestionActivity.this, R.string.unconnected, Toast.LENGTH_SHORT).show();
				}
			}else{
				Toast.makeText(SuggestionActivity.this, R.string.tab3_activity_login_first, Toast.LENGTH_SHORT).show();
			}
			
		}
		
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}

}
