package com.qiangpro.tab2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.qiangpro.main.R;
import com.qiangpro.tab3.LoginActivity;
import com.qiangpro.util.AsyncImageLoader;
import com.qiangpro.util.CallbackImpl;
import com.qiangpro.util.Constants;
import com.qiangpro.util.Constants.ConstantValues;
import com.qiangpro.util.DialogManager;
import com.qiangpro.util.HttpUpload;
import com.qiangpro.util.NetworkConnection;
import com.qiangpro.util.ViewTextUtil;
import com.umeng.analytics.MobclickAgent;

public class RecommendGoodsActivity extends Activity {

	private TextView namedByTab;
	private ViewPager pager;  // 左右滑动组件
	private LinearLayout points;  // 用于存放焦点图标的布局
	private List<View> list;  // 用于保存4张图片布局数据源 
    private ImageView[] views;  // 用于存放导航小圆点
    // 4个布局文件,整体管理,以后要动态加载
    private int[] layouts = {R.layout.tab2_viewpager1, R.layout.tab2_viewpager2,
    		R.layout.tab2_viewpager3,R.layout.tab2_viewpager4};
	private List<NameValuePair> param = new ArrayList<NameValuePair>();
	private SharedPreferences sp;
	private String url = "http://" + ConstantValues.IP + "/goqiang/android/ordersubmit";
	private Handler submitHandler;//得到提交结果的handler
	private String nowDate;//提交的时间
	private ProgressDialog pDialog;
	private int currentIndex;
    private boolean isContinue = true;
    private Handler goodsInfoHandler;//得到商品信息
    private Handler mHandler;//改变viewpager(滑动)
    private Timer timer;
    private TimerTask myTimerTask;
    private static boolean isSleep = true;
    private static final int initPositon = 40;
    private static int currentPosition = 0;
    private boolean tag = true;
    private String category;//获得商品版块类别
    private String locationFlag;//获得版块中具体位置
    private Timer countdown = new Timer();
    private String[] urlString = new String[4];
    private String baseUrl = "http://115.29.45.115/goqiang/resources/";
    private String goodsInfoUrl = "http://115.29.45.115/goqiang/android/getGoodsInfo";
    private List<NameValuePair> paramList = new ArrayList<NameValuePair>();
    private AsyncImageLoader loader = new AsyncImageLoader(RecommendGoodsActivity.this);
    private Spinner spinnerPrice, spinnerNum;
    private ArrayAdapter<CharSequence> arrayAdapterPrice, arrayAdapterNum;
    private List<CharSequence> listPrice, listNum;
    private TextView title, recommendgoods_details;//详情界面
    private EditText tel, address;
    private SharedPreferences sharedPreferences;
    private Map<String, String> priceMap = new HashMap<String, String>();
    private Map<String, String> infoMap = new HashMap<String, String>();
    private Dialog buyDialog;
    private TextView order_grade, order_title, order_info, order_price, order_num,
    			order_username, order_tel, order_address;
    private RelativeLayout layout_all;
    
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
    	if(!tag){
    		tag = true;
        	timer = new Timer();
            myTimerTask = new MyTimerTask();
            timer.schedule(myTimerTask, 4000);
    	}
		super.onStart();
	}
	@Override
	public void onStop() {
		tag = false;
		timer.cancel();
		myTimerTask.cancel();
		super.onStop();
	}
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.recommendgoods_activity);
		Intent it = super.getIntent();
		category = it.getStringExtra("category");//得到商品类别
		locationFlag = it.getStringExtra("flag");//得到商品标记为，区别具体商品信息
//		System.out.println("***** 得到的信息  ****" + category + locationFlag);
		for (int i = 0; i < layouts.length; i++) {
			StringBuffer sBuffer = new StringBuffer();
			sBuffer.append(baseUrl);
			sBuffer.append(category);
			sBuffer.append("//");
			sBuffer.append(locationFlag);
			sBuffer.append("_");
			sBuffer.append(i+1);
			sBuffer.append(".jpg");
			urlString[i] = sBuffer.toString();
		}
		
		init();
		mHandler = new MHandler();
		goodsInfoHandler = new GoodsInfoHandler();
		submitHandler = new SubmitHandler();
		paramList.clear();
		paramList.add(new BasicNameValuePair("category", category));
		paramList.add(new BasicNameValuePair("locationFlag", locationFlag));
		new GoodsDetailsThread().start();//获取商品详情的线程
        //viewpager循环倒计时
        timer = new Timer();
        myTimerTask = new MyTimerTask();
        timer.schedule(myTimerTask, 4000);
		//setListener();
        loadTab2ViewpagerImage();
		setPoints();
		pager.setAdapter(new MyPagerAdapter());
		pager.setOnPageChangeListener(new MyPageChangeListener());
		pager.setCurrentItem(initPositon);
		pager.setOnTouchListener(new MyTouchListener());
	}

	private void init(){
		layout_all = (RelativeLayout)findViewById(R.id.layout_all);
		title = (TextView)findViewById(R.id.title);
		recommendgoods_details = (TextView)findViewById(R.id.recommendgoods_details);
		tel = (EditText)findViewById(R.id.tel);
		address = (EditText)findViewById(R.id.address);
		namedByTab = (TextView)this.findViewById(R.id.namedByTab);
		namedByTab.setText(getString(R.string.tab2_text));
		spinnerPrice = (Spinner)findViewById(R.id.price);
		spinnerNum = (Spinner)findViewById(R.id.num);
		sharedPreferences = getSharedPreferences(Constants.SAVE_NAME_PWD_FILENAME, Activity.MODE_PRIVATE);
		if (sharedPreferences.getBoolean("login_success", false)) {
			tel.setText(sharedPreferences.getString("tel", "联系电话"));
			address.setText(sharedPreferences.getString("address", "收货地址"));
		}
		
		listPrice = new ArrayList<CharSequence>();
		listNum = new ArrayList<CharSequence>();
		listNum.add("数量");
		for (int i = 1; i < 100; i++) {
			listNum.add(Integer.toString(i));
		}
		arrayAdapterNum = new ArrayAdapter<CharSequence>(this, R.layout.recommend_spinner_item
				, listNum);
		arrayAdapterNum.setDropDownViewResource(R.layout.recommend_drop_down_item);
		spinnerNum.setAdapter(arrayAdapterNum);

//		price.setPopupBackgroundDrawable(new ColorDrawable(0x00FFFFFF));
		sp = this.getSharedPreferences(Constants.SAVE_NAME_PWD_FILENAME, Activity.MODE_PRIVATE);
		pager = (ViewPager)this.findViewById(R.id.viewpager_tab2_activity);
		points = (LinearLayout)this.findViewById(R.id.pointGroup);
		list = new ArrayList<View>();// 把4张图片对应的布局文件加载回来,放到List中
		//取得缓存的图片
	}
	
	private class GoodsDetailsThread extends Thread{
		@Override
		public void run(){
			String info = HttpUpload.post(goodsInfoUrl, paramList);
			
			Bundle bundle = new Bundle();
			Message message = new Message();
			bundle.putString("info", info);
			message.setData(bundle);
			message.what = Constants.GOODSDETAILS_SUCCESS;
			goodsInfoHandler.sendMessage(message);
		}
	}
	
	private class GoodsInfoHandler extends Handler{
		@Override
		public void handleMessage(Message msg){
			super.handleMessage(msg);
			Bundle bundle = new Bundle();
			bundle = msg.getData();
			String info = bundle.getString("info");
//			System.out.println("****** info *******" + info);
			switch(msg.what){
				case Constants.GOODSDETAILS_SUCCESS:{
					if(info.equals("服务器错误，请稍后重试")){
						Toast.makeText(RecommendGoodsActivity.this, info, Toast.LENGTH_SHORT).show();
					}else{
						String[] userInfo = info.split("\\|");
						if(userInfo.length == 8){
			            	title.setText(userInfo[0].trim());
			            	recommendgoods_details.setText(userInfo[3].trim());
			            	String[] goodsInfo = userInfo[1].split("#");
			            	String[] goodsPrice = userInfo[2].split("#");
			            	for (int i = 0; i < goodsInfo.length; i++) {
			            		infoMap.put(goodsInfo[i].trim() + goodsPrice[i].trim() + "元", goodsInfo[i].trim());
			            		priceMap.put(goodsInfo[i].trim() + goodsPrice[i].trim() + "元", goodsPrice[i]);
								listPrice.add(goodsInfo[i].trim() + goodsPrice[i].trim() + "元");
							}
			            	arrayAdapterPrice = new ArrayAdapter<CharSequence>(RecommendGoodsActivity.this, R.layout.recommend_spinner_item
			        				, listPrice);
			            	arrayAdapterPrice.setDropDownViewResource(R.layout.recommend_drop_down_item);
			        		spinnerPrice.setAdapter(arrayAdapterPrice);
						}
					}
					break;
				}
			}
		}
	}
	
	private class MHandler extends Handler{
		@Override
		public void handleMessage(Message msg){
            pager.setCurrentItem(currentPosition);
        }
	}

	private class SubmitThread extends Thread{
		@Override
		public void run(){
			String info = HttpUpload.post(url, param);
			Bundle bundle = new Bundle();
			bundle.putString("info", info);
			Message message = new Message();
			message.setData(bundle);
			message.what = Constants.GOODSDETAILS_SUCCESS;
			submitHandler.sendMessage(message);
		}
	}
	
	private class SubmitHandler extends Handler{
		@Override
		public void handleMessage(Message msg){
			super.handleMessage(msg);
			Bundle bundle = new Bundle();
			bundle = msg.getData();
			String info = bundle.getString("info");
			switch(msg.what){
				case Constants.GOODSDETAILS_SUCCESS:{
					if(info.equals("提交成功")){
						pDialog.dismiss();
						DialogManager.doDialogOrder(RecommendGoodsActivity.this);
					}else{
						pDialog.dismiss();
						Toast.makeText(RecommendGoodsActivity.this, "服务器错误", Toast.LENGTH_LONG).show();
					}
					break;
				}
			}
		}
	}
	

	private void loadTab2ViewpagerImage() {
		for(int i = 0; i < layouts.length; i++){
			FrameLayout layout = (FrameLayout)getLayoutInflater().inflate(layouts[i], null);
			ImageView image = (ImageView)layout.findViewById(R.id.tab2_viewpager1);
			CallbackImpl callbackImpl = new CallbackImpl(image);
	    	Bitmap cacheImage = loader.loadBitmap(urlString[i], callbackImpl, false, "no");
			if (cacheImage != null) {
				image.setImageBitmap(cacheImage);
			}
//			imageLoader.DisplayImage(urlString[i], image);
//			loadImage(urlString[i], R.id.tab2_viewpager1);
			
			list.add(layout);
		}
	}
	
	private void setPoints() {
		// 根据list的长度决定做多大的导航图片数组
		views = new ImageView[list.size()];
		for(int i=0;i<views.length;i++){
			ImageView iv = new ImageView(this);
			iv.setLayoutParams(new LayoutParams(20,20));
			iv.setPadding(20, 0, 20, 0);
			views[i] = iv;
			if(i==0){
				iv.setBackgroundResource(R.drawable.page_indicator_focused);
			}else{
				iv.setBackgroundResource(R.drawable.page_indicator);
			}
			points.addView(views[i]);	// 很重要
		}
	}
	
	private class MyPagerAdapter extends PagerAdapter{
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
//				return list.size();
			return Integer.MAX_VALUE;//用于无线循环滑动
		}
		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == arg1;
		}
		@Override
		public Object instantiateItem(View container, int position) {
		
			position = position % list.size();  //循环的关键！求余数！(本来不用求余数)
			( (ViewPager)container ).addView(list.get(position));
			return list.get(position);
		}
		@Override
		public void destroyItem(View container, int position, Object object) {
			( (ViewPager)container ).removeView(list.get(position % list.size()));
		}
	}
	
	private class MyPageChangeListener implements OnPageChangeListener{
		@Override
		public void onPageSelected(int arg0) {
			currentPosition = arg0;//必须放在最前面
			arg0 = arg0 % list.size();
			for(int i=0;i<views.length;i++){
				if(i==arg0){
					views[i].setBackgroundResource(R.drawable.page_indicator_focused);
				}else{
					views[i].setBackgroundResource(R.drawable.page_indicator);
				}
			}
		}
		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub
			
		}
	}
	
	private class MyTouchListener implements OnTouchListener{
        @Override
        public boolean onTouch(View v, MotionEvent event){
            switch (event.getAction()){
	            case MotionEvent.ACTION_DOWN:
	            case MotionEvent.ACTION_MOVE:
	                isContinue = false;                
	                break;
	            case MotionEvent.ACTION_UP:
	            default:
	                isContinue = true;
	                isSleep = true;
                break;
            }
            return false;
        }
	}
	
	private void sleep(long time){
        try{
           Thread.sleep(time); 
        } catch (Exception e){
            e.printStackTrace();
        }
	}
	
	private class MyTimerTask extends TimerTask{
		public void run(){
            while (tag){
                if (isContinue){
                    currentPosition ++;
                    mHandler.sendEmptyMessage(0);
//                    System.out.println(">>>>>> currentPosition " + currentPosition);
                    sleep(3000);
                }
            }
        }
	}
	
	public void buy(View view){
		if (NetworkConnection.isNetworkConnected(RecommendGoodsActivity.this)) {
			if (sharedPreferences.getBoolean("login_success", false)) {
				
				String numSelected = spinnerNum.getSelectedItem().toString();
				String telEditText = tel.getText().toString();
				String addressEditText = address.getText().toString();
				if (numSelected.equals("数量")) {
//					System.out.println(">>> listPrice >>>" + listPrice);
					Toast.makeText(RecommendGoodsActivity.this, "请选择购买数量", Toast.LENGTH_SHORT).show();
				}else if (listPrice == null || listPrice.equals("") || listPrice.size() == 0) {
					Toast.makeText(RecommendGoodsActivity.this, "请选择购买类型", Toast.LENGTH_SHORT).show();
				}else if ( !(telEditText.length() == 11) ) {
					Toast.makeText(RecommendGoodsActivity.this, "联系电话格式不正确", Toast.LENGTH_SHORT).show();
				}else if (addressEditText.equals("")) {
					Toast.makeText(RecommendGoodsActivity.this, "收货地址不能为空", Toast.LENGTH_SHORT).show();
				}else{
					buyDialog = new Dialog(RecommendGoodsActivity.this, R.style.BuyDialog);
					buyDialog.setContentView(R.layout.dialog_buy);
					buyDialog.show();
					order_grade = (TextView)buyDialog.findViewById(R.id.order_grade);
					order_title = (TextView)buyDialog.findViewById(R.id.order_title);
					order_info = (TextView)buyDialog.findViewById(R.id.order_info);
					order_price = (TextView)buyDialog.findViewById(R.id.order_price);
					order_num = (TextView)buyDialog.findViewById(R.id.order_num);
					order_username = (TextView)buyDialog.findViewById(R.id.order_username);
					order_tel = (TextView)buyDialog.findViewById(R.id.order_tel);
					order_address = (TextView)buyDialog.findViewById(R.id.order_address);
					Button yesButton = (Button)buyDialog.findViewById(R.id.order_butten_yes);
					Button noButton = (Button)buyDialog.findViewById(R.id.order_butten_no);
					
					String key = spinnerPrice.getSelectedItem().toString();//之前Map里面的key
					int numInt = Integer.parseInt(numSelected);
					int gradeInt = Integer.parseInt(priceMap.get(key));
					int total = numInt * gradeInt;
					order_grade.setText("可获积分：" + total + "元");
					order_title.setText(RecommendGoodsActivity.this.title.getText().toString());
					order_info.setText(infoMap.get(key));
					order_price.setText(priceMap.get(key));
					order_num.setText(numSelected);
					order_username.setText(sharedPreferences.getString("username", "0"));
					order_tel.setText(telEditText);
					order_address.setText(addressEditText);
					
					yesButton.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							buyDialog.dismiss();
							pDialog = ProgressDialog.show(RecommendGoodsActivity.this, 
									"ToFree", "请稍等……");
							param.clear();
							param.add(new BasicNameValuePair("username", ViewTextUtil.textToString(order_username)));
							param.add(new BasicNameValuePair("tel", ViewTextUtil.textToString(order_tel)));
							param.add(new BasicNameValuePair("address", ViewTextUtil.textToString(order_address)));
							param.add(new BasicNameValuePair("goodsname", ViewTextUtil.textToString(order_title)));
							param.add(new BasicNameValuePair("info", ViewTextUtil.textToString(order_info)));
							param.add(new BasicNameValuePair("price", ViewTextUtil.textToString(order_price)));
							param.add(new BasicNameValuePair("num", ViewTextUtil.textToString(order_num)));
							new SubmitThread().start();
						}
					});
					
					noButton.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							buyDialog.dismiss();
						}
					});
				}
			}else {
				Toast.makeText(RecommendGoodsActivity.this, "请先登录", Toast.LENGTH_SHORT).show();
				Intent it = new Intent(RecommendGoodsActivity.this, LoginActivity.class);
				startActivity(it);
			}
		}else {
			Toast.makeText(RecommendGoodsActivity.this, R.string.unconnected, Toast.LENGTH_SHORT).show();	
		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
		if (sharedPreferences.getBoolean("login_success", false)) {
			tel.setText(sharedPreferences.getString("tel", "联系电话"));
			address.setText(sharedPreferences.getString("address", "收货地址"));
		}
	}
	
	@Override
    public void onDestroy(){
//		imageLoader.clearCache();
        super.onDestroy();
    }
}
