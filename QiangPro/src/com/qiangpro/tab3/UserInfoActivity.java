package com.qiangpro.tab3;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qiangpro.common.ModifyUserInfoActivity;
import com.qiangpro.main.R;
import com.qiangpro.util.Constants;
import com.qiangpro.util.HttpUpload;
import com.qiangpro.util.Constants.ConstantValues;
import com.umeng.analytics.MobclickAgent;

public class UserInfoActivity extends Activity {
	private TextView userinfo_activity_username, userinfo_activity_grade, 
				userinfo_activity_tel, userinfo_activity_address;
	private LinearLayout linearlayout3_userinfo_activity, linearlayout4_userinfo_activity,
		linearlayout5_userinfo_activity, linearlayout6_userinfo_activity, linearlayout7_userinfo_activity;
	private List<NameValuePair> param = new ArrayList<NameValuePair>();
	private ProgressDialog pDialog;
	private Handler userInfoHandler;
	private String urlLogin = "http://" + ConstantValues.IP + "/goqiang/android/login";
	private SharedPreferences sp;
	private SharedPreferences.Editor edit2;
	private TextView namedByTab;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.setContentView(R.layout.user_info_activity);
		
		this.userInfoHandler = new UserInfoHandler();
		
		namedByTab =(TextView)findViewById(R.id.namedByTab);
		namedByTab.setText("会员");
		this.userinfo_activity_username = (TextView)this.findViewById(R.id.userinfo_activity_username);
		this.userinfo_activity_grade = (TextView)this.findViewById(R.id.userinfo_activity_grade);
		this.userinfo_activity_tel = (TextView)this.findViewById(R.id.userinfo_activity_tel);
		this.linearlayout3_userinfo_activity = (LinearLayout)this.findViewById(R.id.linearlayout3_userinfo_activity);
		this.linearlayout4_userinfo_activity = (LinearLayout)this.findViewById(R.id.linearlayout4_userinfo_activity);
		this.linearlayout5_userinfo_activity = (LinearLayout)this.findViewById(R.id.linearlayout5_userinfo_activity);
		this.linearlayout6_userinfo_activity = (LinearLayout)this.findViewById(R.id.linearlayout6_userinfo_activity);
		this.linearlayout7_userinfo_activity = (LinearLayout)this.findViewById(R.id.linearlayout7_userinfo_activity);
		
		sp = this.getSharedPreferences(Constants.SAVE_NAME_PWD_FILENAME, Activity.MODE_PRIVATE);
		edit2 = sp.edit();
		
		pDialog = ProgressDialog.show(UserInfoActivity.this, "ToFree", "请稍等……");
		pDialog.setCancelable(true);
		UserInfoThread userInfoThread = new UserInfoThread();
		userInfoThread.start();
		
		init();
		
		linearlayout3_userinfo_activity.setOnClickListener(new OnClickListenerLinear("1"));
		linearlayout5_userinfo_activity.setOnClickListener(new OnClickListenerLinear("2"));
		linearlayout6_userinfo_activity.setOnClickListener(new OnClickListenerLinear("3"));
		linearlayout7_userinfo_activity.setOnClickListener(new OnClickListenerLinear("4"));
	}

	private void init() {
		String username = sp.getString("username", "0");
		String grade = sp.getString("grade", "0");
		String tel = sp.getString("tel", "0");
		String address = sp.getString("address", "0");
		userinfo_activity_username.setText(username);
		userinfo_activity_grade.setText(grade);
		userinfo_activity_tel.setText(tel);
		
	}
	
	private class UserInfoHandler extends Handler{
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			Bundle bundle = new Bundle();
			bundle = msg.getData();
			String msgStr = bundle.getString("msg");
			switch(msg.what){
				case Constants.GOODSDETAILS_SUCCESS:{
					pDialog.dismiss();
					if(msgStr.equals("服务器错误，请稍后重试")){
						Toast.makeText(UserInfoActivity.this, msgStr, Toast.LENGTH_SHORT).show();
					}else{
						String[] userInfo = msgStr.split("\\|");
						if(userInfo.length == 4){
							edit2.putString("username", userInfo[0]);
							edit2.putString("grade", userInfo[1]);
							edit2.putString("tel", userInfo[2]);
							edit2.putString("address", userInfo[3]);
							edit2.commit();
						}
						init();
					}
					break;
				}
			}
		}
	}
	
	private class UserInfoThread extends Thread{
		@Override
		public void run(){
			param.clear();
			param.add(new BasicNameValuePair("username", sp.getString("username", "0")));
			param.add(new BasicNameValuePair("password", sp.getString("password", "0")));
			String infoLogin = HttpUpload.post(urlLogin, param);
			Bundle b = new Bundle();
			b.putString("msg", infoLogin);
			Message msg = new Message();
			msg.setData(b);
			msg.what = Constants.GOODSDETAILS_SUCCESS;
			userInfoHandler.sendMessage(msg);
		}
	}
	
	private class OnClickListenerLinear implements OnClickListener{
		String index;
		public OnClickListenerLinear(String i){
			index = i;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

				Intent it = new Intent(UserInfoActivity.this, ModifyUserInfoActivity.class);
				it.putExtra("index", index);
				UserInfoActivity.this.startActivity(it);
		}
	}

	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
		
		SharedPreferences sp = this.getSharedPreferences(Constants.SAVE_NAME_PWD_FILENAME, Activity.MODE_PRIVATE);
		String username = sp.getString("username", "0");
		String grade = sp.getString("grade", "0");
		String tel = sp.getString("tel", "0");
		String address = sp.getString("address", "0");
		
		userinfo_activity_username.setText(username);
		userinfo_activity_grade.setText(grade);
		userinfo_activity_tel.setText(tel);
	}
}
