package com.qiangpro.tab3;

import static com.qiangpro.util.ViewTextUtil.viewToString;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.InjectView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.inject.Inject;
import com.qiangpro.common.UserProtocolActivity;
import com.qiangpro.main.R;
import com.qiangpro.util.Constants;
import com.qiangpro.util.Constants.ConstantValues;
import com.qiangpro.util.HttpUpload;
import com.qiangpro.util.NetworkConnection;
import com.qiangpro.validator.RegularValidator;
import com.umeng.analytics.MobclickAgent;

public class RegisterActivity extends RoboFragmentActivity {
	private TextView register_accept;
	private String username, password, password_confirm, tel, address;
	private String url = "http://" + ConstantValues.IP + "/goqiang/android/register";
	private List<NameValuePair> params=new ArrayList<NameValuePair>();
	private Handler handler;
	private SharedPreferences sp;
	private int num;
	private ProgressDialog pDialog;
	private static int UMIN_LENGTH = 1;
	private static int UMAX_LENGTH = 16;
	private static int PMIN_LENGTH = 6;
	private static int PMAX_LENGTH = 20;
	
	@InjectView(R.id.register_username)
	private EditText register_username;
	@InjectView(R.id.register_password)
	private EditText register_password;
	@InjectView(R.id.register_password_confirm)
	private EditText register_password_confirm;
	@InjectView(R.id.register_tel)
	private EditText register_tel;
	@InjectView(R.id.register_address)
	private EditText register_address;
	@InjectView(R.id.register_button)
	private ImageView register_button;
	@InjectView(R.id.errormessage1)
	private TextView errormessage1;
	@InjectView(R.id.errormessage2)
	private TextView errormessage2;
	@InjectView(R.id.errormessage3)
	private TextView errormessage3;
	@InjectView(R.id.errormessage4)
	private TextView errormessage4;
	@InjectView(R.id.errormessage5)
	private TextView errormessage5;
	@InjectView(R.id.namedByTab)
	private TextView namedByTab; 
	@Inject
	private RegularValidator regularValidator;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub 
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.setContentView(R.layout.register_activity);
		this.register_accept = (TextView)this.findViewById(R.id.register_accept);
		namedByTab.setText("ע��");
		sp = RegisterActivity.this.getSharedPreferences(Constants.REGISTER_NUM_FILENAME, Activity.MODE_PRIVATE);
		num = sp.getInt("number", 0);
		this.handler = new Handler(){
			@Override
			public void handleMessage(Message msg){
				super.handleMessage(msg);
				Bundle bundle = new Bundle();
				bundle = msg.getData();
				String info = bundle.getString("info");
				switch(msg.what){
					case Constants.REGISTER_SUCCESS:{
						if(info.equals("ע��ɹ�")){
							pDialog.dismiss();
							Toast.makeText(RegisterActivity.this, info, Toast.LENGTH_LONG).show();
							SharedPreferences.Editor edit = sp.edit();
							edit.putInt("number", num+1);
							edit.commit();
							RegisterActivity.this.finish();
						}else{
							pDialog.dismiss();
							Toast.makeText(RegisterActivity.this, info, Toast.LENGTH_LONG).show();
						}
						break;
					}
				}
			}
		};
		
		this.register_button.setOnClickListener(new OnClickListenerButton());
		this.register_accept.setOnClickListener(new OnClickListenerAccept());
	}
	
	private class OnClickListenerAccept implements OnClickListener{

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			Intent it = new Intent(RegisterActivity.this, UserProtocolActivity.class);
			RegisterActivity.this.startActivity(it);
		}
    	
    }
	
	
	private class OnClickListenerButton implements OnClickListener{

		@Override
		public void onClick(View arg0) {
			
			if(num >= 2){
				Toast.makeText(RegisterActivity.this, R.string.rigister_activity_num_out, Toast.LENGTH_LONG).show();
			}else{
				if(NetworkConnection.isNetworkConnected(RegisterActivity.this)){
					doRegister(viewToString(register_username),viewToString(register_password),
							viewToString(register_password_confirm),viewToString(register_tel),
							viewToString(register_address));
				}else{
					Toast.makeText(RegisterActivity.this, R.string.unconnected, Toast.LENGTH_SHORT).show();
				}	
			}
		}
	}
	
	private class RegisterThread extends Thread{
		@Override
		public void run(){
			String info = HttpUpload.post(url, params);
//			System.out.println(">>>info>>" + info);
			Bundle bundle = new Bundle();
			bundle.putString("info", info);
			Message message = new Message();
			message.setData(bundle);
			message.what = Constants.REGISTER_SUCCESS;
			handler.sendMessage(message);
		}
	}
	
	private void doRegister(String userName, String pwd,String confirmpwd,String photonumber,String adress) {
		try {
			regularValidator.with(userName).checkBlankChar().checkIllegalChar().checkSQLInject()
						.checkRgisterUsernameLength(UMIN_LENGTH, UMAX_LENGTH);
			 errormessage1.setText("");
		} catch (Exception e1) {
			 errormessage1.setText(e1.getMessage());
		}
		try{
			regularValidator.with(pwd).checkBlankChar().checkIllegalChar().checkSQLInject()
						.checkCheseInput().checkRgisterPwdLength(PMIN_LENGTH, PMAX_LENGTH);
			 errormessage2.setText("");
		} catch (Exception e2) {
			 errormessage2.setText(e2.getMessage());
		}
		try{
			regularValidator.with(confirmpwd).checkEqual(pwd, confirmpwd);
			 errormessage3.setText("");
		} catch (Exception e3) {
			 errormessage3.setText(e3.getMessage());
		}
		try{
			regularValidator.with(photonumber).checkBlankChar().checkPhoneNumber();
			 errormessage4.setText("");
		} catch (Exception e4) {
			 errormessage4.setText(e4.getMessage());
		}
		try{
			regularValidator.with(adress).checkBlankChar().checkIllegalChar().checkSQLInject();
			 errormessage5.setText("");
		} catch (Exception e5) {
			 errormessage5.setText(e5.getMessage());
		}
			
		if(IsBlank(errormessage1)&&IsBlank(errormessage2)&&IsBlank(errormessage3)&&IsBlank(errormessage4)
				&&IsBlank(errormessage5)){
			pDialog = ProgressDialog.show(RegisterActivity.this, "ToFree", "���Եȡ���");
			params.clear();
			params.add(new BasicNameValuePair("name", viewToString(register_username)));
			params.add(new BasicNameValuePair("pwd", viewToString(register_password)));
			params.add(new BasicNameValuePair("tel", viewToString(register_tel)));
			params.add(new BasicNameValuePair("address",viewToString(register_address)));
			new RegisterThread().start();
		}else{
			Toast.makeText(RegisterActivity.this, "����ȷ��д��ע����Ϣ", Toast.LENGTH_SHORT).show();
		}
	}
	
	public boolean IsBlank(TextView text){
		if(text.getText()==null||"".equals(text.getText())){
			return true;
		}else{
			return false;
		}
	}
	
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
}
