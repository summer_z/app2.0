package com.qiangpro.tab3;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.InjectView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.inject.Inject;
import com.qiangpro.common.ForgetPwdActivity;
import com.qiangpro.main.R;
import com.qiangpro.main.TabActivity3;
import com.qiangpro.util.Constants;
import com.qiangpro.util.Constants.ConstantValues;
import com.qiangpro.util.HttpUpload;
import com.qiangpro.util.NetworkConnection;
import com.qiangpro.validator.RegularValidator;
import com.umeng.analytics.MobclickAgent;
import static com.qiangpro.util.ViewTextUtil.viewToString;

public class LoginActivity extends RoboFragmentActivity{
	private CheckBox login_checkbox_auto;
	private TextView login_forget_pwd, login_rigister;
	private ImageView login_button;
	private String url = "http://" + ConstantValues.IP + "/goqiang/android/login";
	private List<NameValuePair> params=new ArrayList<NameValuePair>();
	private Handler loginHandler;
	private String[] userInfo;
	private ProgressDialog pDialog;
	private static int MIN_LENGTH = 1;
	private static int MAX_LENGTH = 16;
	private static int UMIN_LENGTH = 6;
	private static int PMAX_LENGTH = 20;
	@Inject
	private RegularValidator regularValidator;
	
	@InjectView(R.id.login_username)
	private EditText login_username;
	
	@InjectView(R.id.login_password)
	private EditText login_password;
	
	@InjectView(R.id.namedByTab)
	private TextView namedByTab; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.setContentView(R.layout.login_activity);
		this.login_checkbox_auto = (CheckBox)this.findViewById(R.id.login_checkbox_auto);
		this.login_forget_pwd= (TextView)this.findViewById(R.id.login_forget_pwd);
		this.login_rigister= (TextView)this.findViewById(R.id.login_rigister);
		this.login_button = (ImageView)this.findViewById(R.id.login_button);
		namedByTab.setText("登录");
		loginHandler = new LoginHandle();
		
		this.login_button.setOnClickListener(new OnClickListenerButton());
		this.login_forget_pwd.setOnClickListener(new OnClickListenerForget());
		this.login_rigister.setOnClickListener(new OnClickListenerRigister());
	}

	private class OnClickListenerForget implements OnClickListener{

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			Intent it = new Intent(LoginActivity.this, ForgetPwdActivity.class);
			LoginActivity.this.startActivity(it);
		}
    	
    }
	
	private class OnClickListenerRigister implements OnClickListener{

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			Intent it = new Intent(LoginActivity.this, RegisterActivity.class);
			LoginActivity.this.startActivity(it);
		}
    	
    }
	
	private class OnClickListenerButton implements OnClickListener{

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			if(NetworkConnection.isNetworkConnected(LoginActivity.this)){
				doLogin(viewToString(login_username), viewToString(login_password));
			}else{
				Toast.makeText(LoginActivity.this, R.string.unconnected, Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	private void doLogin(String userName, String pwd) {
		try {
			regularValidator.with(userName).checkLength(MIN_LENGTH, MAX_LENGTH)
					.checkIllegalChar().checkBlankChar().checkSQLInject();
			regularValidator.with(pwd).checkLength(UMIN_LENGTH, PMAX_LENGTH)
					.checkIllegalChar().checkSQLInject().checkCheseInput();
			pDialog = ProgressDialog.show(LoginActivity.this, "ToFree", "请稍等……");
			params.clear();
//			System.out.println(">>>name password>>>>" + userName + pwd);
			params.add(new BasicNameValuePair("username", userName));				
			params.add(new BasicNameValuePair("password", pwd));	
			new LoginThread().start();
		} catch (Exception e) {
			Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_SHORT)
					.show();
		}
	}
	
	private class LoginHandle extends Handler{
		@Override
		public void handleMessage(Message msg){
			super.handleMessage(msg);
			Bundle bundle = new Bundle();
			bundle = msg.getData();
			String msgStr = bundle.getString("msg");//返回的信息，包括name,grade,tel,address
			switch(msg.what){
				case Constants.LOGIN_SUCCESS:	
					pDialog.dismiss();
					if(!msgStr.equals("用户名或密码错误")){
						if(msgStr.equals("服务器错误，请稍后重试")){
							Toast.makeText(LoginActivity.this, msgStr, Toast.LENGTH_SHORT).show();
						}else{
							userInfo = msgStr.split("\\|");
							if(userInfo.length == 4){
								saveUserInfo(); //保存用户信息到本地
							}
							//LoginActivity.this.startActivity(intent);
							LoginActivity.this.getIntent().putExtra("retmsg", "1") ;// 返回信息
							LoginActivity.this.setResult(RESULT_OK, LoginActivity.this.getIntent()) ;
							LoginActivity.this.finish() ;		// 结束Intent
							Toast.makeText(LoginActivity.this, "登录成功", Toast.LENGTH_SHORT).show();
						}
					}else{
						Toast.makeText(LoginActivity.this, msgStr, Toast.LENGTH_SHORT).show();
					}
				break;
			}
		}

		private void saveUserInfo() {
			SharedPreferences sp = LoginActivity.this.getSharedPreferences(Constants.SAVE_NAME_PWD_FILENAME, Activity.MODE_PRIVATE);
			SharedPreferences.Editor edit = sp.edit();
			edit.putString("username", userInfo[0]);
			edit.putString("grade", userInfo[1]);
			edit.putString("tel", userInfo[2]);
			edit.putString("address", userInfo[3]);
			edit.putString("password", viewToString(login_password));
			edit.putBoolean("login_success", true);//标记登录成功
			if(login_checkbox_auto.isChecked()){
				edit.putBoolean("auto_login", true);//标记自动登录
			}else{
				edit.putBoolean("auto_login", false);
			}
			edit.commit();
		}
	}
	
	private class LoginThread extends Thread{
		@Override
		public void run(){
			String msgStr = HttpUpload.post(url, params);
			Bundle b = new Bundle();
			b.putString("msg", msgStr);
			Message msg = new Message();
			msg.setData(b);
			msg.what = Constants.LOGIN_SUCCESS;
			loginHandler.sendMessage(msg);
		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
}
