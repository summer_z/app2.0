package com.qiangpro.adapter;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.qiangpro.common.GoodsInformationActivity;
import com.qiangpro.main.R;
import com.qiangpro.util.AsyncImageLoader;
import com.qiangpro.util.CallbackImpl;

public class FreeAreaListViewAdapter extends BaseAdapter {
    
    private Activity activity;
    private String[] urlArray;
    private String[] imageName;
    private static LayoutInflater inflater=null;
    private AsyncImageLoader loader;
    private int count = 1;
    
    public FreeAreaListViewAdapter(Activity a, String[] d, String[] imaStrings) {
        activity = a;
        urlArray = d;
        imageName = imaStrings;
        loader = new AsyncImageLoader(a);
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setCount(int c){
    	count = c;
    }
    
    public void setUrlArray(String[] s){
    	urlArray = s;
    }
    
    public int getCount() {
        return 1;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }
    
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null){
            vi = inflater.inflate(R.layout.tab2activity_listdemo, null);
        }
        ImageView tab2_imageview1=(ImageView)vi.findViewById(R.id.tab2_imageview1);
        ImageView tab2_imageview2=(ImageView)vi.findViewById(R.id.tab2_imageview2);
        ImageView tab2_imageview3=(ImageView)vi.findViewById(R.id.tab2_imageview3);
        ImageView tab2_imageview4=(ImageView)vi.findViewById(R.id.tab2_imageview4);
        ImageView tab2_imageview5=(ImageView)vi.findViewById(R.id.tab2_imageview5);
        ImageView tab2_imageview6=(ImageView)vi.findViewById(R.id.tab2_imageview6);
        ImageView tab2_imageview7=(ImageView)vi.findViewById(R.id.tab2_imageview7);
        ImageView tab2_imageview8=(ImageView)vi.findViewById(R.id.tab2_imageview8);
        
        ImageView tab2_imageview9=(ImageView)vi.findViewById(R.id.tab2_imageview9);
        ImageView tab2_imageview10=(ImageView)vi.findViewById(R.id.tab2_imageview10);
        ImageView tab2_imageview11=(ImageView)vi.findViewById(R.id.tab2_imageview11);
        ImageView tab2_imageview12=(ImageView)vi.findViewById(R.id.tab2_imageview12);
        ImageView tab2_imageview13=(ImageView)vi.findViewById(R.id.tab2_imageview13);
        ImageView tab2_imageview14=(ImageView)vi.findViewById(R.id.tab2_imageview14);
        ImageView tab2_imageview15=(ImageView)vi.findViewById(R.id.tab2_imageview15);
        ImageView tab2_imageview16=(ImageView)vi.findViewById(R.id.tab2_imageview16);
        
        ImageView tab2_imageview17=(ImageView)vi.findViewById(R.id.tab2_imageview17);
        ImageView tab2_imageview18=(ImageView)vi.findViewById(R.id.tab2_imageview18);
        ImageView tab2_imageview19=(ImageView)vi.findViewById(R.id.tab2_imageview19);
        ImageView tab2_imageview20=(ImageView)vi.findViewById(R.id.tab2_imageview20);
        ImageView tab2_imageview21=(ImageView)vi.findViewById(R.id.tab2_imageview21);
        ImageView tab2_imageview22=(ImageView)vi.findViewById(R.id.tab2_imageview22);
        ImageView tab2_imageview23=(ImageView)vi.findViewById(R.id.tab2_imageview23);
        ImageView tab2_imageview24=(ImageView)vi.findViewById(R.id.tab2_imageview24);
        
        ImageView tab2_imageview25=(ImageView)vi.findViewById(R.id.tab2_imageview25);
        ImageView tab2_imageview26=(ImageView)vi.findViewById(R.id.tab2_imageview26);
        ImageView tab2_imageview27=(ImageView)vi.findViewById(R.id.tab2_imageview27);
        ImageView tab2_imageview28=(ImageView)vi.findViewById(R.id.tab2_imageview28);
        ImageView tab2_imageview29=(ImageView)vi.findViewById(R.id.tab2_imageview29);
        ImageView tab2_imageview30=(ImageView)vi.findViewById(R.id.tab2_imageview30);
        ImageView tab2_imageview31=(ImageView)vi.findViewById(R.id.tab2_imageview31);
        ImageView tab2_imageview32=(ImageView)vi.findViewById(R.id.tab2_imageview32);
        
        ImageView tab2_imageview33=(ImageView)vi.findViewById(R.id.tab2_imageview33);
        ImageView tab2_imageview34=(ImageView)vi.findViewById(R.id.tab2_imageview34);
        ImageView tab2_imageview35=(ImageView)vi.findViewById(R.id.tab2_imageview35);
        ImageView tab2_imageview36=(ImageView)vi.findViewById(R.id.tab2_imageview36);
        ImageView tab2_imageview37=(ImageView)vi.findViewById(R.id.tab2_imageview37);
        ImageView tab2_imageview38=(ImageView)vi.findViewById(R.id.tab2_imageview38);
        ImageView tab2_imageview39=(ImageView)vi.findViewById(R.id.tab2_imageview39);
        ImageView tab2_imageview40=(ImageView)vi.findViewById(R.id.tab2_imageview40);
        //隐藏图片
        ImageView tab2_image9 = (ImageView)vi.findViewById(R.id.tab2_m9);
        ImageView tab2_image10 = (ImageView)vi.findViewById(R.id.tab2_m10);
        ImageView tab2_image11 = (ImageView)vi.findViewById(R.id.tab2_m11);
        ImageView tab2_image12 = (ImageView)vi.findViewById(R.id.tab2_m12);
        ImageView tab2_image13 = (ImageView)vi.findViewById(R.id.tab2_m13);
        ImageView tab2_image14 = (ImageView)vi.findViewById(R.id.tab2_m14);
        ImageView tab2_image15 = (ImageView)vi.findViewById(R.id.tab2_m15);
        ImageView tab2_image16 = (ImageView)vi.findViewById(R.id.tab2_m16);
        
        ImageView tab2_image17 = (ImageView)vi.findViewById(R.id.tab2_m17);
        ImageView tab2_image18 = (ImageView)vi.findViewById(R.id.tab2_m18);
        ImageView tab2_image19 = (ImageView)vi.findViewById(R.id.tab2_m19);
        ImageView tab2_image20 = (ImageView)vi.findViewById(R.id.tab2_m20);
        ImageView tab2_image21 = (ImageView)vi.findViewById(R.id.tab2_m21);
        ImageView tab2_image22 = (ImageView)vi.findViewById(R.id.tab2_m22);
        ImageView tab2_image23 = (ImageView)vi.findViewById(R.id.tab2_m23);
        ImageView tab2_image24 = (ImageView)vi.findViewById(R.id.tab2_m24);
        
        ImageView tab2_image25 = (ImageView)vi.findViewById(R.id.tab2_m25);
        ImageView tab2_image26 = (ImageView)vi.findViewById(R.id.tab2_m26);
        ImageView tab2_image27 = (ImageView)vi.findViewById(R.id.tab2_m27);
        ImageView tab2_image28 = (ImageView)vi.findViewById(R.id.tab2_m28);
        ImageView tab2_image29 = (ImageView)vi.findViewById(R.id.tab2_m29);
        ImageView tab2_image30 = (ImageView)vi.findViewById(R.id.tab2_m30);
        ImageView tab2_image31 = (ImageView)vi.findViewById(R.id.tab2_m31);
        ImageView tab2_image32 = (ImageView)vi.findViewById(R.id.tab2_m32);
        
        ImageView tab2_image33 = (ImageView)vi.findViewById(R.id.tab2_m33);
        ImageView tab2_image34 = (ImageView)vi.findViewById(R.id.tab2_m34);
        ImageView tab2_image35 = (ImageView)vi.findViewById(R.id.tab2_m35);
        ImageView tab2_image36 = (ImageView)vi.findViewById(R.id.tab2_m36);
        ImageView tab2_image37 = (ImageView)vi.findViewById(R.id.tab2_m37);
        ImageView tab2_image38 = (ImageView)vi.findViewById(R.id.tab2_m38);
        ImageView tab2_image39 = (ImageView)vi.findViewById(R.id.tab2_m39);
        ImageView tab2_image40 = (ImageView)vi.findViewById(R.id.tab2_m40);
        //中间隐藏图片组
        ImageView[] m2 = {tab2_image9, tab2_image10, tab2_image11, tab2_image12, tab2_image13, 
        		tab2_image14, tab2_image15, tab2_image16 };
        ImageView[] m3 = {tab2_image17, tab2_image18, tab2_image19, tab2_image20, tab2_image21, 
        		tab2_image22, tab2_image23, tab2_image24 };
        ImageView[] m4 = {tab2_image25, tab2_image26, tab2_image27, tab2_image28, tab2_image29, 
        		tab2_image30, tab2_image31, tab2_image32 };
        ImageView[] m5 = {tab2_image33, tab2_image34, tab2_image35, tab2_image36, tab2_image37, 
        		tab2_image38, tab2_image39, tab2_image40 };
        //显示的图片组
        ImageView[] imageViews1 = {tab2_imageview1, tab2_imageview2, tab2_imageview3, tab2_imageview4, 
        		tab2_imageview5, tab2_imageview6, tab2_imageview7, tab2_imageview8};
        ImageView[] imageViews2 = {tab2_imageview9, tab2_imageview10, tab2_imageview11, tab2_imageview12, 
        		tab2_imageview13, tab2_imageview14, tab2_imageview15, tab2_imageview16};
        ImageView[] imageViews3 = {tab2_imageview17, tab2_imageview18, tab2_imageview19, tab2_imageview20, 
        		tab2_imageview21, tab2_imageview22, tab2_imageview23, tab2_imageview24};
        ImageView[] imageViews4 = {tab2_imageview25, tab2_imageview26, tab2_imageview27, tab2_imageview28, 
        		tab2_imageview29, tab2_imageview30, tab2_imageview31, tab2_imageview32};
        ImageView[] imageViews5 = {tab2_imageview33, tab2_imageview34, tab2_imageview35, tab2_imageview36, 
        		tab2_imageview37, tab2_imageview38, tab2_imageview39, tab2_imageview40};
        //缓存图片名字
//        String[] imageName = {"recommend1.jpg", "recommend2.jpg", "recommend3.jpg",  "recommend4.jpg",
//        		"recommend5.jpg", "recommend6.jpg", "recommend7.jpg", "recommend8.jpg"};
        if (count == 1) {
	        //先 读取本地缓存的图片拿来显示
	        for(int i =0; i < imageViews1.length; i++){
	        	File cacheDir = new File(activity.getFilesDir(), "cache");
				File f = new File(cacheDir, imageName[i]);
				Bitmap map = BitmapFactory.decodeFile(f.getPath());
				if(map != null){
					imageViews1[i].setImageBitmap(map);
				}
	 		}
	        //连接网络刷新图片信息
	        for(int i = 0; i < imageViews1.length; i++){
				CallbackImpl callbackImpl = new CallbackImpl(imageViews1[i]);
		    	Bitmap cacheImage = loader.loadBitmap(urlArray[i], callbackImpl, true, imageName[i]);
				if (cacheImage != null) {
					imageViews1[i].setImageBitmap(cacheImage);
				}
				//其余ImageView设置为不可见
				imageViews2[i].setVisibility(View.GONE);
				imageViews3[i].setVisibility(View.GONE);
				imageViews4[i].setVisibility(View.GONE);
				imageViews5[i].setVisibility(View.GONE);
				
				m2[i].setVisibility(View.GONE);
				m3[i].setVisibility(View.GONE);
				m4[i].setVisibility(View.GONE);
				m5[i].setVisibility(View.GONE);
			}
	        
        }else if (count == 2) {
        	//连接网络刷新图片信息
	        for(int i = 0; i < imageViews2.length; i++){
				CallbackImpl callbackImpl = new CallbackImpl(imageViews2[i]);
		    	Bitmap cacheImage = loader.loadBitmap(urlArray[i], callbackImpl, false, "no");
				if (cacheImage != null) {
					imageViews2[i].setImageBitmap(cacheImage);
				}
				imageViews2[i].setVisibility(View.VISIBLE);
				m2[i].setVisibility(View.VISIBLE);
			}
		}else if (count == 3) {
			//连接网络刷新图片信息
	        for(int i = 0; i < imageViews2.length; i++){
				CallbackImpl callbackImpl = new CallbackImpl(imageViews3[i]);
		    	Bitmap cacheImage = loader.loadBitmap(urlArray[i], callbackImpl, false, "no");
				if (cacheImage != null) {
					imageViews3[i].setImageBitmap(cacheImage);
				}
				imageViews3[i].setVisibility(View.VISIBLE);
				m3[i].setVisibility(View.VISIBLE);
			}
		}else if (count == 4) {
			//连接网络刷新图片信息
	        for(int i = 0; i < imageViews2.length; i++){
				CallbackImpl callbackImpl = new CallbackImpl(imageViews4[i]);
		    	Bitmap cacheImage = loader.loadBitmap(urlArray[i], callbackImpl, false, "no");
				if (cacheImage != null) {
					imageViews4[i].setImageBitmap(cacheImage);
				}
				imageViews4[i].setVisibility(View.VISIBLE);
				m4[i].setVisibility(View.VISIBLE);
			}
		}else if (count == 5) {
			//连接网络刷新图片信息
	        for(int i = 0; i < imageViews2.length; i++){
				CallbackImpl callbackImpl = new CallbackImpl(imageViews5[i]);
		    	Bitmap cacheImage = loader.loadBitmap(urlArray[i], callbackImpl, false, "no");
				if (cacheImage != null) {
					imageViews5[i].setImageBitmap(cacheImage);
				}
				imageViews5[i].setVisibility(View.VISIBLE);
				m5[i].setVisibility(View.VISIBLE);
			}
		}
        
        return vi;
    }
}