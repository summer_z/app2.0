package com.qiangpro.main;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.qiangpro.tab4.AboutActivity;
import com.qiangpro.tab4.HelpActivity;
import com.qiangpro.tab4.ShareMaActivity;
import com.qiangpro.tab4.SuggestionActivity;
import com.qiangpro.util.Constants.ConstantValues;
import com.qiangpro.util.DataCleanManager;
import com.qiangpro.util.DialogManager;
import com.qiangpro.util.NetworkConnection;
import com.qiangpro.util.UpdateManager;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.sso.UMSsoHandler;

public class TabActivity4 extends Activity {
	private TextView  namedByTab,tab4_textview2, tab4_textview3, tab4_textview5, 
				tab4_textview6, tab4_textview7, tab4_textview8, tab4_textview9;
	final UMSocialService mController = UMServiceFactory.getUMSocialService("com.umeng.share");
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.tab4_activity);
		
		
		this.tab4_textview2 = (TextView)super.findViewById(R.id.tab4_textview2);
		this.tab4_textview3 = (TextView)super.findViewById(R.id.tab4_textview3);
		this.tab4_textview5 = (TextView)super.findViewById(R.id.tab4_textview5);
		this.tab4_textview6 = (TextView)super.findViewById(R.id.tab4_textview6);
		this.tab4_textview7 = (TextView)super.findViewById(R.id.tab4_textview7);
		this.tab4_textview8 = (TextView)super.findViewById(R.id.tab4_textview8);
		this.tab4_textview9 = (TextView)super.findViewById(R.id.tab4_textview9);
		this.namedByTab=(TextView)super.findViewById(R.id.namedByTab);
		// 设置分享内容
		mController.setShareContent("ToFree土匪，一款免费的抢购软件，http://115.29.45.115/goqiang/resources/xml/QiangPro.apk");
		// 设置分享图片
		mController.setShareMedia(new UMImage(TabActivity4.this, R.drawable.ic_launcher));
//		mController.getConfig().removePlatform( SHARE_MEDIA.RENREN, SHARE_MEDIA.DOUBAN);
//		mController.getConfig().addFollow(SHARE_MEDIA.SINA, "summer_z1993");
//		mController.getConfig().addFollow(SHARE_MEDIA.TENCENT, "ZY1314520AXQ");
		namedByTab.setText(getString(R.string.tab4_text));
		
		tab4_textview2.setOnClickListener(new TextViewOnClickListener(2));
		tab4_textview3.setOnClickListener(new TextViewOnClickListener(3));
		tab4_textview5.setOnClickListener(new TextViewOnClickListener(5));
		tab4_textview6.setOnClickListener(new TextViewOnClickListener(6));
		tab4_textview7.setOnClickListener(new TextViewOnClickListener(7));
		tab4_textview8.setOnClickListener(new TextViewOnClickListener(8));
		tab4_textview9.setOnClickListener(new TextViewOnClickListener(9));
		
	}
	
	
	private class TextViewOnClickListener implements OnClickListener{
		private int index = 0;
		public TextViewOnClickListener(int index){
			this.index = index;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch(index){
				case 2:{
					mController.openShare(TabActivity4.this, false);
					break;
				}
				case 3:{
					DataCleanManager.cleanCache(TabActivity4.this);
					Toast.makeText(TabActivity4.this, R.string.tab4_clean_cache, Toast.LENGTH_SHORT).show() ;
					break;
				}
				case 5:{
					Intent it = new  Intent(TabActivity4.this, ShareMaActivity.class);
					TabActivity4.this.startActivity(it);
					break;
				}
				case 6:{
					Intent it = new  Intent(TabActivity4.this, HelpActivity.class);
					TabActivity4.this.startActivity(it);
					break;
				}
				case 7:{
					Intent it = new Intent(TabActivity4.this, SuggestionActivity.class);
					TabActivity4.this.startActivity(it);
					break;
				}
				case 8:{
					boolean isConnected = NetworkConnection.isNetworkConnected(TabActivity4.this);
					if(isConnected){
						UpdateManager up = new UpdateManager(TabActivity4.this);
						SharedPreferences sp = TabActivity4.this.getSharedPreferences(ConstantValues.VERSION_FILENAME, 
								Activity.MODE_PRIVATE);
						boolean target = sp.getBoolean("version", false) ;
						up.checkUpdate(target) ; //检查更新
					}else{
						Toast.makeText(TabActivity4.this, R.string.unconnected, Toast.LENGTH_LONG).show() ;
					}
					break;
				}
				case 9:{
					Intent it = new Intent(TabActivity4.this, AboutActivity.class);
					TabActivity4.this.startActivity(it);
					break;
				}
			}
		}
		
	}
	//设置返回按键的监听事件
  	public boolean onKeyDown(int keyCode, KeyEvent event) {
  		if (keyCode == KeyEvent.KEYCODE_BACK) {	// 返回键
  			DialogManager.doExit(TabActivity4.this);
  		}
  		return false ;
  	}
  	
  	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
	
	@Override 
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    /**使用SSO授权必须添加如下代码 */
	    UMSsoHandler ssoHandler = mController.getConfig().getSsoHandler(requestCode) ;
	    if(ssoHandler != null){
	       ssoHandler.authorizeCallBack(requestCode, resultCode, data);
	    }
	}
}
