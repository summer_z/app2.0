package com.qiangpro.main;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qiangpro.tab1.FreeAreaActivity;
import com.qiangpro.tab1.GradeAreaActivity;
import com.qiangpro.tab3.LoginActivity;
import com.qiangpro.tab3.RegisterActivity;
import com.qiangpro.tab3.UserInfoActivity;
import com.qiangpro.util.Constants;
import com.qiangpro.util.DialogManager;
import com.qiangpro.util.NetworkConnection;
import com.umeng.analytics.MobclickAgent;


public class TabActivity3 extends Activity {
	
	private TextView namedByTab;
	private LinearLayout  linearlayout4_tab3_activity, linearlayout5_tab3_activity,
	linearlayout6_tab3_activity, linearlayout7_tab3_activity, linearlayout8_tab3_activity;
	private SharedPreferences sp;
	private boolean IsSuccess;
	private ImageView login_now;
	private TextView userName;
	
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.setContentView(R.layout.tab3_activity);
		sp = TabActivity3.this.getSharedPreferences(Constants.SAVE_NAME_PWD_FILENAME, Activity.MODE_PRIVATE);
		
		init();
		getUserInfo();
		
		linearlayout4_tab3_activity.setOnClickListener(new LinearLayoutOnClickListener(4));
		linearlayout5_tab3_activity.setOnClickListener(new LinearLayoutOnClickListener(5));
		linearlayout6_tab3_activity.setOnClickListener(new LinearLayoutOnClickListener(6));
		linearlayout7_tab3_activity.setOnClickListener(new LinearLayoutOnClickListener(7));
		linearlayout8_tab3_activity.setOnClickListener(new LinearLayoutOnClickListener(8));
	}
	
	public void init(){
		namedByTab=(TextView)this.findViewById(R.id.namedByTab);
		namedByTab.setText(getString(R.string.tab3_text));
		login_now=(ImageView)findViewById(R.id.login_now);
		userName=(TextView)findViewById(R.id.userName);
		this.linearlayout4_tab3_activity = (LinearLayout)this.findViewById(R.id.linearlayout4_tab3_activity);
		this.linearlayout5_tab3_activity = (LinearLayout)this.findViewById(R.id.linearlayout5_tab3_activity);
		this.linearlayout6_tab3_activity = (LinearLayout)this.findViewById(R.id.linearlayout6_tab3_activity);
		this.linearlayout7_tab3_activity = (LinearLayout)this.findViewById(R.id.linearlayout7_tab3_activity);
		this.linearlayout8_tab3_activity = (LinearLayout)this.findViewById(R.id.linearlayout8_tab3_activity);
	}

	public void goLogin(View v){
		Intent intent =new Intent(TabActivity3.this,LoginActivity.class);
		TabActivity3.this.startActivityForResult(intent, 1);	
	}
	
	private void getUserInfo() {
		IsSuccess = sp.getBoolean("login_success", false);
		if(IsSuccess){
			login_now.setVisibility(View.GONE);
			userName.setVisibility(View.VISIBLE);
			userName.setText(sp.getString("username", "0") + "土匪，你好！");
		}else{
			login_now.setVisibility(View.VISIBLE);
			userName.setVisibility(View.GONE);
			
		}
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		getUserInfo();
	}

	private class LinearLayoutOnClickListener implements OnClickListener{
		private int index = 0;
		public LinearLayoutOnClickListener(int index){
			this.index = index;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch(index){
				case 4:{
					Intent it = new Intent(TabActivity3.this, RegisterActivity.class);
					TabActivity3.this.startActivity(it);
					break;
				}
				case 5:{
					boolean login_success = sp.getBoolean("login_success", false);
					if(!login_success){
						Toast.makeText(TabActivity3.this, "亲，请先登录哦……", Toast.LENGTH_SHORT).show();
					}else{
						Intent it = new Intent(TabActivity3.this, UserInfoActivity.class);
						TabActivity3.this.startActivity(it);
					}
					
					break;
				}
				case 6:{
					Intent it = new Intent(TabActivity3.this, FreeAreaActivity.class);
					TabActivity3.this.startActivity(it);
					MobclickAgent.onEvent(TabActivity3.this, "tab3_recommend");
					break;
				}
				case 7:{
					Intent it = new Intent(TabActivity3.this, GradeAreaActivity.class);
					TabActivity3.this.startActivity(it);
					MobclickAgent.onEvent(TabActivity3.this, "tab3_prize");
					break;
				}
				case 8:{
					boolean login_success = sp.getBoolean("login_success", false);
					if (NetworkConnection.isNetworkConnected(TabActivity3.this)) {
						if(login_success){
							SharedPreferences.Editor edit = sp.edit();
							edit.putBoolean("login_success", false);
							edit.commit();
							login_now.setVisibility(View.VISIBLE);
							userName.setVisibility(View.GONE);
							Toast.makeText(TabActivity3.this, R.string.tab3_activity_logout_ok, Toast.LENGTH_SHORT).show();
						}else{
							Toast.makeText(TabActivity3.this, R.string.tab3_activity_login_first, Toast.LENGTH_SHORT).show();
						}
					}else {
						Toast.makeText(TabActivity3.this, R.string.unconnected, Toast.LENGTH_SHORT).show();
					}
					break;
				}
			}
		}
		
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {			// 判断操作类型
		case RESULT_OK:			// 成功操作
			if("1".equals(data.getStringExtra("retmsg")))
			login_now.setVisibility(View.GONE);
			userName.setVisibility(View.VISIBLE);
			userName.setText(sp.getString("username", "0") + "土匪，你好！");
			break;
		case RESULT_CANCELED:			// 取消操作
			break ;
		default:
			break;
		}
	}

	
	//设置返回按键的监听事件
  	public boolean onKeyDown(int keyCode, KeyEvent event) {
  		if (keyCode == KeyEvent.KEYCODE_BACK) {	// 返回键
  			DialogManager.doExit(TabActivity3.this);
  		}
  		return false ;
  	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}


}
