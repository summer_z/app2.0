package com.qiangpro.main;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qiangpro.common.GoodsInformationHomeViewpagerActivity;
import com.qiangpro.tab1.BuyRecordActivity;
import com.qiangpro.tab1.FreeAreaActivity;
import com.qiangpro.tab1.GradeAreaActivity;
import com.qiangpro.tab1.NoticeActivity;
import com.qiangpro.tab1.NumActivity;
import com.qiangpro.tab1.WinRecordActivity;
import com.qiangpro.tab3.LoginActivity;
import com.qiangpro.util.AsyncImageLoader;
import com.qiangpro.util.CallbackImpl;
import com.qiangpro.util.Constants;
import com.qiangpro.util.Constants.ConstantValues;
import com.qiangpro.util.DialogManager;
import com.qiangpro.util.HttpUpload;
import com.qiangpro.util.NetworkConnection;
import com.umeng.analytics.MobclickAgent;

public class TabActivity1 extends Activity {
	private ViewPager pager;  // 左右滑动组件
	private LinearLayout points;  // 用于存放焦点图标的布局
	private List<View> list;  // 用于保存5张图片布局数据源 
    private ImageView[] views;  // 用于存放导航小圆点
    // 5个布局文件,动态加载
    private int[] layouts = {R.layout.tab1_viewpager1, R.layout.tab1_viewpager2,
    		R.layout.tab1_viewpager3, R.layout.tab1_viewpager4, R.layout.tab1_viewpager5};
    private Timer timer;//用于viepager循环滑动的实现
    private TimerTask myTimerTask;
    private Handler mHandler;//定时改变viewpager(滑动)
    private TextView namedByTab;
    private boolean isContinue = true;
    private static final int initPositon = 50;
    private static int currentPosition = 0;
    private static boolean isSleep = true;
    private boolean tag = true;
    private AsyncImageLoader loader = new AsyncImageLoader(TabActivity1.this);
    private String[] urlString = ConstantValues.tab1activityImageUrlArray;
    private String[] imageName = {"home1.jpg", "home2.jpg", "home3.jpg",  "home4.jpg", "home5.jpg"};
    private TextView tab1_textView1, tab1_textView2;
    private ImageView tab1_imageview1, tab1_imageview2;
    //用户信息获取
    private List<NameValuePair> param = new ArrayList<NameValuePair>();
	private ProgressDialog pDialog;
	private Handler userInfoHandler;
	private String urlLogin = "http://" + ConstantValues.IP + "/goqiang/android/getUserValue";
	private SharedPreferences sp;
	private SharedPreferences.Editor edit2;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.setContentView(R.layout.tab1_activity);
		
		pDialog = ProgressDialog.show(TabActivity1.this, "ToFree", "请稍等……");
		pDialog.setCancelable(true);
		init();
		this.userInfoHandler = new UserInfoHandler();
		new UserInfoThread().start();
        
        namedByTab.setText(getString(R.string.tab1_text));
		setPoints();
		if (!NetworkConnection.isNetworkConnected(TabActivity1.this)) {
			loadCacheImage();//没有网络时，加载本地缓存
		}else {
			loadTab1ViewpagerImage();//连接网络读取图片
		}
		timer.schedule(myTimerTask, 4000);
		
		pager.setAdapter(new MyPagerAdapter());
		pager.setOnPageChangeListener(new MyPageChangeListener());
		pager.setCurrentItem(initPositon);
		pager.setOnTouchListener(new MyTouchListener());

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
    	if(!tag){
    		tag = true;
        	timer = new Timer();
            myTimerTask = new MyTimerTask();
            timer.schedule(myTimerTask, 4000);
    	}
		super.onStart();
	}
	
	private void init() {
		pager = (ViewPager)this.findViewById(R.id.viewpager_tab1_activity);
		points = (LinearLayout)this.findViewById(R.id.pointGroup2);
		list = new ArrayList<View>();// 把5张图片对应的布局文件加载回来,放到List中
		namedByTab = (TextView)this.findViewById(R.id.namedByTab);
		tab1_textView1 = (TextView)this.findViewById(R.id.tab1_textview1);
		tab1_textView2 = (TextView)this.findViewById(R.id.tab1_textview2);
//		tab1_imageview1 = (ImageView)this.findViewById(R.id.tab1_imageview1);
//		tab1_imageview2 = (ImageView)this.findViewById(R.id.tab1_imageview2);
		timer = new Timer();
        myTimerTask = new MyTimerTask();
        mHandler = new MHandler();
        sp = this.getSharedPreferences(Constants.SAVE_NAME_PWD_FILENAME, Activity.MODE_PRIVATE);
		edit2 = sp.edit();
	}
	
	private void setPoints() {
		// 根据list的长度决定做多大的导航图片数组
		views = new ImageView[layouts.length];
		for(int i=0; i<layouts.length; i++){
			ImageView iv = new ImageView(this);
			iv.setLayoutParams(new LayoutParams(20,20));
			iv.setPadding(20, 0, 20, 0);
			views[i] = iv;
			if(i==0){
				iv.setBackgroundResource(R.drawable.page_indicator_focused);
			}else{
				iv.setBackgroundResource(R.drawable.page_indicator);
			}
			points.addView(views[i]);	// 很重要
		}
	}
	
	private void loadCacheImage(){
        //先 读取本地缓存的图片拿来显示
        for(int i =0; i < layouts.length; i++){
        	FrameLayout layout = (FrameLayout)getLayoutInflater().inflate(layouts[i], null);
			ImageView image = (ImageView)layout.findViewById(R.id.imageView1);
        	File cacheDir = new File(TabActivity1.this.getFilesDir(), "cache");
			File f = new File(cacheDir, imageName[i]);
			Bitmap map = BitmapFactory.decodeFile(f.getPath());
			if(map != null){
				image.setImageBitmap(map);
			}
			list.add(layout);
 		}
	}
	
	private void loadTab1ViewpagerImage() {
		for(int i = 0; i < layouts.length; i++){
			FrameLayout layout = (FrameLayout)getLayoutInflater().inflate(layouts[i], null);
			ImageView image = (ImageView)layout.findViewById(R.id.imageView1);
			CallbackImpl callbackImpl = new CallbackImpl(image);
	    	Bitmap cacheImage = loader.loadBitmap(urlString[i], callbackImpl, true, imageName[i]);
			if (cacheImage != null) {
				image.setImageBitmap(cacheImage);
			}
			list.add(layout);
		}
	}
	
	private class MyTimerTask extends TimerTask{
		public void run(){
			while (tag) {
	            if (isContinue){
	                currentPosition ++;
	                mHandler.sendEmptyMessage(0);
	//                    System.out.println(">>>>>> currentPosition " + currentPosition);
	                sleep(3000);
	            }
			}
        }
	}

	private void sleep(long time){
        try{
           Thread.sleep(time); 
        } catch (Exception e){
            e.printStackTrace();
        }
	}
	
	private class MHandler extends Handler{
		@Override
		public void handleMessage(Message msg){
//			System.out.println("****** currentPosition ****" + currentPosition);
            pager.setCurrentItem(currentPosition);
        }
	}
	
	private class UserInfoHandler extends Handler{
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			Bundle bundle = new Bundle();
			bundle = msg.getData();
			String msgStr = bundle.getString("msg");
			switch(msg.what){
				case Constants.GOODSDETAILS_SUCCESS:{
					pDialog.dismiss();
					if(msgStr.equals("服务器错误，请稍后重试") || msgStr.equals("用户名或密码错误")){
						Toast.makeText(TabActivity1.this, msgStr, Toast.LENGTH_SHORT).show();
					}else{
						String[] userInfo = msgStr.split("\\|");
						if(userInfo.length == 2){
							float grade = (float)Integer.parseInt(userInfo[0]) /100;
							tab1_textView1.setText(grade + "元");
							float todayValue = (float)Integer.parseInt(userInfo[1]) /100;
							tab1_textView2.setText(todayValue + "元");
						}
						
					}
					break;
				}
			}
		}
	}
	
	private class UserInfoThread extends Thread{
		@Override
		public void run(){
			if (sp.getBoolean("login_success", false)) {
				param.clear();
				param.add(new BasicNameValuePair("username", sp.getString("username", "0")));
				param.add(new BasicNameValuePair("password", sp.getString("password", "0")));
				String infoLogin = HttpUpload.post(urlLogin, param);
				Bundle b = new Bundle();
				b.putString("msg", infoLogin);
				Message msg = new Message();
				msg.setData(b);
				msg.what = Constants.GOODSDETAILS_SUCCESS;
				userInfoHandler.sendMessage(msg);
			}else {
				pDialog.dismiss();
			}
			
		}
	}
	
	private class MyPagerAdapter extends PagerAdapter{
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
//				return list.size();
			return Integer.MAX_VALUE;//用于无线循环滑动
		}
		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == arg1;
		}
		@Override
		public Object instantiateItem(View container, int position) {
			position = position % list.size();  //循环的关键！求余数！(本来不用求余数)
			View view = list.get(position);
			view.setOnClickListener(new OnViewClickListener(position));
			( (ViewPager)container ).addView(list.get(position));
			return list.get(position);
		}
		
		@Override
		public void destroyItem(View container, int position, Object object) {
			( (ViewPager)container ).removeView(list.get(position % list.size()));
		}
	}
	//viepager点击事件
	private class OnViewClickListener implements OnClickListener {
		int position;
		public OnViewClickListener(int p){
			position = p;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent it = new Intent(TabActivity1.this, GoodsInformationHomeViewpagerActivity.class);
			it.putExtra("category", "homeviewpager");
			it.putExtra("flag", "h" + (position + 1));
			it.putExtra("index", "homeviewpager");
			TabActivity1.this.startActivity(it);
		}
	}
	
	private class MyPageChangeListener implements OnPageChangeListener{
		@Override
		public void onPageSelected(int arg0) {
			currentPosition = arg0;//必须放在最前面
			arg0 = arg0 % list.size();
			for(int i=0;i<views.length;i++){
				if(i==arg0){
					views[i].setBackgroundResource(R.drawable.page_indicator_focused);
				}else{
					views[i].setBackgroundResource(R.drawable.page_indicator);
				}
			}
		}
		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub
			
		}
	}
	
	private class MyTouchListener implements OnTouchListener{
        @Override
        public boolean onTouch(View v, MotionEvent event){
            switch (event.getAction()){
	            case MotionEvent.ACTION_DOWN:
	            case MotionEvent.ACTION_MOVE:
	                isContinue = false;                
	                break;
	            case MotionEvent.ACTION_UP:
	            default:
	                isContinue = true;
	                isSleep = true;
                break;
            }
            return false;
        }
	}
	
	
	public void showTab1_imageview_haoma(View view){
		Intent it = new Intent(TabActivity1.this, NumActivity.class);
		startActivity(it);
	}
	
	public void showTab1_imageview_zj(View view){
		if (sp.getBoolean("login_success", false)) {
			Intent it = new Intent(TabActivity1.this, WinRecordActivity.class);
			startActivity(it);
		}else {
			Toast.makeText(TabActivity1.this, "请先登录", Toast.LENGTH_LONG).show();
			Intent itIntent = new Intent(TabActivity1.this, LoginActivity.class);
			startActivity(itIntent);
		}
	}
	
	public void showTab1_imageview_gm(View view){
		if (sp.getBoolean("login_success", false)) {
			Intent it = new Intent(TabActivity1.this, BuyRecordActivity.class);
			startActivity(it);
		}else {
			Toast.makeText(TabActivity1.this, "请先登录", Toast.LENGTH_LONG).show();
			Intent itIntent = new Intent(TabActivity1.this, LoginActivity.class);
			startActivity(itIntent);
		}
	}
	
	public void showTab1_imageview_gonggao(View view){
		Intent it = new Intent(TabActivity1.this, NoticeActivity.class);
		startActivity(it);
	}

	public void showImageviewFree(View view){
		Intent it = new Intent(TabActivity1.this, FreeAreaActivity.class);
		startActivity(it);
	}

	public void showImageviewGrade(View view){
		Intent it = new Intent(TabActivity1.this, GradeAreaActivity.class);
		startActivity(it);
	}
    //设置返回按键的监听事件
  	public boolean onKeyDown(int keyCode, KeyEvent event) {
  		if (keyCode == KeyEvent.KEYCODE_BACK) {	// 返回键
  			DialogManager.doExit(TabActivity1.this);
  		}
  		return false ;
  	}
  	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
		new UserInfoThread().start();
		if (!sp.getBoolean("login_success", false)) {
			tab1_textView1.setText("0.00元");
			tab1_textView2.setText("0.00元");
		}
	}
	@Override
	public void onStop() {
		tag = false;
		timer.cancel();
		myTimerTask.cancel();
		super.onStop();
	}
}
