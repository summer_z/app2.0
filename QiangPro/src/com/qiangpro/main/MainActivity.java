package com.qiangpro.main;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;
import com.qiangpro.parse.RemoteImageLoader;
import com.qiangpro.util.Constants;
import com.qiangpro.util.Constants.ConstantValues;
import com.qiangpro.util.DownloadManager;
import com.qiangpro.util.NetworkConnection;
import com.qiangpro.util.UpdateManager;
import com.qiangpro.util.Constants.ConstantValues;
/**
 * @author summer
 *	功能描述：自定义TabHost
 *	继承 TabActivity
 */
public class MainActivity extends TabActivity {
	private TabHost tabHost;
	private LayoutInflater layoutInflater;
	private boolean login_success;
	private SharedPreferences sp2;
	private Handler handler;
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main_activity);
		this.handler = new Handler(){
			@Override
			public void handleMessage(Message msg){
				super.handleMessage(msg);
				switch(msg.what){
					case Constants.GOODSDETAILS_SUCCESS:{
						SharedPreferences sp = MainActivity.this.getSharedPreferences(ConstantValues.VERSION_FILENAME, 
								Activity.MODE_PRIVATE);
						boolean target = sp.getBoolean("version", false) ;
						if (target) { 
				            // 显示下载提示对话框 
				        	DownloadManager downloadManager = new DownloadManager(MainActivity.this);
				        	downloadManager.showNoticeDialog();
				        }
						break;
					}
				}
			}
		};
		sp2 = super.getSharedPreferences(Constants.SAVE_NAME_PWD_FILENAME, Activity.MODE_PRIVATE);
		login_success = sp2.getBoolean("login_success", false);
		boolean isCon = NetworkConnection.isNetworkConnected(MainActivity.this) ;
		if(isCon){
			new Thread(
					new Runnable(){
						@Override
						public void run() {
							// TODO Auto-generated method stub
							UpdateManager update = new UpdateManager(MainActivity.this);
							update.saveRemoteVersionMessage();
							update.saveRemoteGoodsInfo();
						
							RemoteImageLoader remoteImageLoader = new RemoteImageLoader(MainActivity.this);
							remoteImageLoader.saveStartActivityImage();
//							remoteImageLoader.saveHomeBlockImage();
//							remoteImageLoader.saveCategoryBlockImage();
//							for(int i=0;i< ConstantValues.tab2activityImageUrlArray.length;i++){
//								remoteImageLoader.saveGoodsInfoViewpagerImage( ConstantValues.tab2activityImageUrlArray[i]);
//							}
							Message message = new Message();
							message.what = Constants.GOODSDETAILS_SUCCESS;
							handler.sendMessage(message);
						}
					}
				).start();
			
		}else{
			Toast.makeText(MainActivity.this, R.string.unconnected, Toast.LENGTH_LONG).show() ;
		}
		
		initView();
		
	}
	private void initView() {
		tabHost = getTabHost();
		layoutInflater = LayoutInflater.from(this);
		int activityCount = ConstantValues.tabClassArray.length;
		for(int i = 0; i < activityCount; i++){	
			//为每一个Tab按钮设置图标、文字和内容
			TabSpec tabSpec = tabHost.newTabSpec(ConstantValues.tabTextViewArray[i])
					.setIndicator(getTabItemView(i)).setContent(getTabItemIntent(i));
			//将Tab按钮添加进Tab选项卡中
			tabHost.addTab(tabSpec);
			//设置Tab按钮的背景
//			tabHost.getTabWidget().getChildAt(i)
//					.setBackgroundResource(R.drawable.selector_tab_background);
//			tabHost.getTabWidget().getChildAt(i).setBackgroundColor(0x333333);
		}
	}
	//给每个Tab设置图标和文字
	private View getTabItemView(int index){
		View view = layoutInflater.inflate(R.layout.tab_item_view, null);
		ImageView imageView = (ImageView) view.findViewById(R.id.imageview);
		if (imageView != null){
			imageView.setImageResource(ConstantValues.tabImageViewArray[index]);
		}		
//		TextView textView = (TextView) view.findViewById(R.id.textview);
//		textView.setText(ConstantValues.tabTextViewArray[index]);
//		textView.setTextColor(getResources().getColor(R.color.textviewcolor));
		return view;
	}
	//给每个Tab设置跳转的Activity
	private Intent getTabItemIntent(int index){
		Intent intent = new Intent(this, ConstantValues.tabClassArray[index]);
		return intent;
	}
	
}
