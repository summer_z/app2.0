package com.qiangpro.main;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.AvoidXfermode.Mode;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.qiangpro.adapter.ListViewAdapter;
import com.qiangpro.common.GoodsInformationActivity;
import com.qiangpro.parse.RemoteImageLoader;
import com.qiangpro.tab2.RecommendGoodsActivity;
import com.qiangpro.util.Constants;
import com.qiangpro.util.Constants.ConstantValues;
import com.qiangpro.util.DialogManager;
import com.qiangpro.util.HttpUpload;
import com.qiangpro.util.NetworkConnection;
import com.umeng.analytics.MobclickAgent;
 
public class TabActivity2 extends Activity {
	private TextView namedByTab;
	private PullToRefreshListView pullToRefreshListView;
	private ListView tab2ActivityList;
	private ListViewAdapter adapter;
	private String refreshHeadContent;
	private static LayoutInflater inflater=null;
	private boolean flag=true;
	private File cacheDir;
	private RemoteImageLoader remoteImageLoader ;
	private int count = 1;
	private String recommendNumURL = "http://115.29.45.115/goqiang/android/getRecommendNum";
	private List<NameValuePair> params = new ArrayList<NameValuePair>();
	private Handler recommendNumHandler;
	//缓存图片的名字
	private String[] imageName = {"recommend1.jpg", "recommend2.jpg", "recommend3.jpg",  "recommend4.jpg",
    		"recommend5.jpg", "recommend6.jpg", "recommend7.jpg", "recommend8.jpg"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.tab2_activity);
		initViewId();
		recommendNumHandler = new RecommendNumHandle();
		namedByTab.setText(getString(R.string.tab2_text));
		setListener();
		tab2ActivityList = pullToRefreshListView.getRefreshableView();
//		tab2ActivityList.addFooterView(listFooter,false, false);
		adapter=new ListViewAdapter(this, ConstantValues.tab2activityImageUrlArray, imageName);
		tab2ActivityList.setAdapter(adapter);
		registerForContextMenu(tab2ActivityList);
	}
	
	private void initViewId(){
		namedByTab = (TextView)this.findViewById(R.id.namedByTab);
		pullToRefreshListView=(PullToRefreshListView)this.findViewById(R.id.tab2activity_list);
		remoteImageLoader =new RemoteImageLoader(TabActivity2.this.getApplicationContext());
	}
	
	private void setListener() {
		pullToRefreshListView.setMode(com.handmark.pulltorefresh.library.PullToRefreshBase.Mode.BOTH);
		pullToRefreshListView.setOnRefreshListener(new PullStateListener());
		
		pullToRefreshListView.getLoadingLayoutProxy(false, true).setPullLabel("上拉刷新");  
		pullToRefreshListView.getLoadingLayoutProxy(false, true).setRefreshingLabel("正在载入……");  
		pullToRefreshListView.getLoadingLayoutProxy(false, true).setReleaseLabel("放开以刷新");  
	}

    private class PullStateListener implements OnRefreshListener2<ListView>{
    	//下拉刷新
		@Override
		public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			if(NetworkConnection.isNetworkConnected(TabActivity2.this)){
				refreshHeadContent = DateUtils.formatDateTime(TabActivity2.this,
						System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME| DateUtils.FORMAT_SHOW_DATE| DateUtils.FORMAT_ABBREV_ALL);
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(refreshHeadContent);
				new PullDownRefreshDataTask().execute();
			}else{
	        	Toast.makeText( TabActivity2.this, getString(R.string.unconnected), Toast.LENGTH_SHORT).show();
	        	pullToRefreshListView.onRefreshComplete();
			}
		}
		//上拉刷新
		@Override
		public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			if(NetworkConnection.isNetworkConnected(TabActivity2.this)){
				count ++;//用于adapter呈现多少组图片
				new RecommendNumThread().start();
			}else{
	        	Toast.makeText( TabActivity2.this, getString(R.string.unconnected), Toast.LENGTH_SHORT).show();
	        	pullToRefreshListView.onRefreshComplete();
			}
			
		}
    	
    }
    
	private class PullDownRefreshDataTask extends AsyncTask<Void, Void, String[]> {
		@Override
		protected String[] doInBackground(Void... params) {
			
			remoteImageLoader.saveRecommendImage();
			return null;
		}

		@Override
		protected void onPostExecute(String[] result) {
		    adapter=new ListViewAdapter(TabActivity2.this, ConstantValues.tab2activityImageUrlArray, imageName);
		    tab2ActivityList.setAdapter(adapter);
//			adapter.listViewImageLoader.clearCache();
//		    adapter.notifyDataSetChanged();
		    Toast.makeText(TabActivity2.this, getString(R.string.updateSuccess), Toast.LENGTH_SHORT).show() ;
            pullToRefreshListView.onRefreshComplete();
            count = 1;
            super.onPostExecute(result);
		}
	}
	
	private class RecommendNumHandle extends Handler{
		@Override
		public void handleMessage(Message msg){
			Bundle bundle = new Bundle();
			bundle = msg.getData();
			String info = bundle.getString("info");
			switch (msg.what) {
			case Constants.GOODSDETAILS_SUCCESS:
				if (info.equals("请检查您的网络设置")) {
					Toast.makeText( TabActivity2.this, info, Toast.LENGTH_SHORT).show();
				}else{
					int num = Integer.parseInt(info);
					pullToRefreshListView.onRefreshComplete();
					if (num == count*8) {
						adapter.setCount(count);
						if (count == 2) {
							adapter.setUrlArray(ConstantValues.recommendUrl2);
						}else if(count == 3){
							adapter.setUrlArray(ConstantValues.recommendUrl3);
						}else if (count == 4) {
							adapter.setUrlArray(ConstantValues.recommendUrl4);
						}else if (count == 5) {
							adapter.setUrlArray(ConstantValues.recommendUrl5);
						}
				
						adapter.notifyDataSetChanged();
						Toast.makeText( TabActivity2.this, "刷新成功", Toast.LENGTH_SHORT).show();
					}else {
						Toast.makeText( TabActivity2.this, "没有更多了", Toast.LENGTH_SHORT).show();
					}
				}
				break;
			default:
				break;
			}
		}
	}
	
	private class RecommendNumThread extends Thread{
		@Override
		public void run(){
			String info = HttpUpload.post(recommendNumURL, params);
			Bundle bundle = new Bundle();
			Message message = new Message();
			bundle.putString("info", info);
			message.setData(bundle);
			message.what = Constants.GOODSDETAILS_SUCCESS;
			recommendNumHandler.sendMessage(message);
		}
	}
	
	public void showImageview1Detail(View v) {
		showImageviewDetail("recommend", "h1");
	}
	public void showImageview2Detail(View v) {
		showImageviewDetail("recommend", "h2");
	}
	public void showImageview3Detail(View v) {
		showImageviewDetail("recommend", "h3");
	}
	public void showImageview4Detail(View v) {
		showImageviewDetail("recommend", "h4");
	}
	public void showImageview5Detail(View v) {
		showImageviewDetail("recommend", "h5");
	}
	public void showImageview6Detail(View v) {
		showImageviewDetail("recommend", "h6");
	}
	public void showImageview7Detail(View v) {
		showImageviewDetail("recommend", "h7");
	}
	public void showImageview8Detail(View v) {
		showImageviewDetail("recommend", "h8");
	}
	public void showImageview9Detail(View v) {
		showImageviewDetail("recommend", "h9");
	}
	public void showImageview10Detail(View v) {
		showImageviewDetail("recommend", "h10");
	}
	public void showImageview11Detail(View v) {
		showImageviewDetail("recommend", "h11");
	}
	public void showImageview12Detail(View v) {
		showImageviewDetail("recommend", "h12");
	}
	public void showImageview13Detail(View v) {
		showImageviewDetail("recommend", "h13");
	}
	public void showImageview14Detail(View v) {
		showImageviewDetail("recommend", "h14");
	}
	public void showImageview15Detail(View v) {
		showImageviewDetail("recommend", "h15");
	}
	public void showImageview16Detail(View v) {
		showImageviewDetail("recommend", "h16");
	}
	public void showImageview17Detail(View v) {
		showImageviewDetail("recommend", "h17");
	}
	public void showImageview18Detail(View v) {
		showImageviewDetail("recommend", "h18");
	}
	public void showImageview19Detail(View v) {
		showImageviewDetail("recommend", "h19");
	}
	public void showImageview20Detail(View v) {
		showImageviewDetail("recommend", "h20");
	}
	public void showImageview21Detail(View v) {
		showImageviewDetail("recommend", "h21");
	}
	public void showImageview22Detail(View v) {
		showImageviewDetail("recommend", "h22");
	}
	public void showImageview23Detail(View v) {
		showImageviewDetail("recommend", "h23");
	}
	public void showImageview24Detail(View v) {
		showImageviewDetail("recommend", "h24");
	}
	public void showImageview25Detail(View v) {
		showImageviewDetail("recommend", "h25");
	}
	public void showImageview26Detail(View v) {
		showImageviewDetail("recommend", "h26");
	}
	public void showImageview27Detail(View v) {
		showImageviewDetail("recommend", "h27");
	}
	public void showImageview28Detail(View v) {
		showImageviewDetail("recommend", "h28");
	}
	public void showImageview29Detail(View v) {
		showImageviewDetail("recommend", "h29");
	}
	public void showImageview30Detail(View v) {
		showImageviewDetail("recommend", "h30");
	}
	public void showImageview31Detail(View v) {
		showImageviewDetail("recommend", "h31");
	}
	public void showImageview32Detail(View v) {
		showImageviewDetail("recommend", "h32");
	}
	public void showImageview33Detail(View v) {
		showImageviewDetail("recommend", "h33");
	}
	public void showImageview34Detail(View v) {
		showImageviewDetail("recommend", "h34");
	}
	public void showImageview35Detail(View v) {
		showImageviewDetail("recommend", "h35");
	}
	public void showImageview36Detail(View v) {
		showImageviewDetail("recommend", "h36");
	}
	public void showImageview37Detail(View v) {
		showImageviewDetail("recommend", "h37");
	}
	public void showImageview38Detail(View v) {
		showImageviewDetail("recommend", "h38");
	}
	public void showImageview39Detail(View v) {
		showImageviewDetail("recommend", "h39");
	}
	public void showImageview40Detail(View v) {
		showImageviewDetail("recommend", "h40");
	}
	
	
	public void showImageviewDetail(String category, String flag){
		Intent intent= new Intent(TabActivity2.this,RecommendGoodsActivity.class);
		intent.putExtra("category", category);
		intent.putExtra("flag", flag);
		TabActivity2.this.startActivity(intent);
		
	}
	
	
	//设置返回按键的监听事件
  	public boolean onKeyDown(int keyCode, KeyEvent event) {
  		if (keyCode == KeyEvent.KEYCODE_BACK) {	// 返回键
  			DialogManager.doExit(TabActivity2.this);
  		}
  		return false ;
  	}
  	
  	
  	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
	
	@Override
    public void onDestroy()
    {
		tab2ActivityList.setAdapter(null);
        super.onDestroy();
    }
}
