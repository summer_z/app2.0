package com.qiangpro.main;

import java.io.File;

import roboguice.activity.RoboActivity;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.inject.Inject;
import com.qiangpro.service.CacheService;
import com.qiangpro.util.Constants.ConstantValues;
import com.umeng.analytics.MobclickAgent;

public class StartActivity extends RoboActivity {
	private SharedPreferences sp ;
    private SharedPreferences.Editor edit ;
    @Inject
	private CacheService cacheService;
    
    private ImageView start_image;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.start_activity) ;
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN) ;

		start_image = (ImageView)findViewById(R.id.start_image);
		File cacheDir = new File(StartActivity.this.getFilesDir(), "cache");
		File f = new File(cacheDir, "start.jpg");
		Bitmap map = BitmapFactory.decodeFile(f.getPath());
		if(map != null){
			start_image.setImageBitmap(map);
		}
		sp = getSharedPreferences(ConstantValues.CHECK_FIRST_BUILD_FILENAME, Activity.MODE_PRIVATE) ; //检查是否是第一次安装
		edit = sp.edit() ;
		boolean firstLoad = sp.getBoolean("firstLoad", true);
		if(firstLoad){
			cacheService.setNoticeCacheQuantity(0);
			edit.putBoolean("firstLoad", false) ;
			edit.commit() ;
			Intent it = new Intent(StartActivity.this, GuideActivity.class);
			startActivity(it);
			finish();
		}else{
			new Handler().postDelayed(new Runnable(){
				@Override
				public void run() {
					// TODO Auto-generated method stub
					Intent it = new Intent(StartActivity.this, MainActivity.class) ;
					StartActivity.this.startActivity(it) ;
					StartActivity.this.finish() ;
				}
			}, 2000) ;
		}
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
	
}
